package business;

import java.io.IOException;

import persistance.FileUtility;

public class UILayer {

	private Model model;
	private String content;
	private FileUtility fileUtility = new FileUtility();

	public UILayer(Model model) {
		this.model = model;
	}

	public String createUILayer(String generationDirectory) throws Exception {
		String consoleStream = "";
		consoleStream += this.createIndex(generationDirectory);
		consoleStream += this.createIndexAjax(generationDirectory);
		consoleStream += this.createError(generationDirectory);
		consoleStream += this.createHome(generationDirectory);
		consoleStream += this.createHeader(generationDirectory);
		consoleStream += this.createSession(generationDirectory);
		consoleStream += this.createMenu(generationDirectory);
		consoleStream += this.createInsert(generationDirectory);
		consoleStream += this.createUpdate(generationDirectory);
		consoleStream += this.createUpdateProfile(generationDirectory);
		consoleStream += this.createUpdatePassword(generationDirectory);
		consoleStream += this.createUpdateImage(generationDirectory);
		consoleStream += this.createUpdateFile(generationDirectory);
		consoleStream += this.createUpdateProfilePicture(generationDirectory);
		consoleStream += this.createRecoverPassword(generationDirectory);
		consoleStream += this.createSelectAll(generationDirectory);
		consoleStream += this.createSelectAllByRelation(generationDirectory);
		consoleStream += this.createSearch(generationDirectory);
		consoleStream += this.createSearchLog(generationDirectory);			
		return consoleStream;
	}

	private String createIndex(String generationDirectory) throws IOException {
		this.content = "";
		this.content += "<?php \n";
		this.content += "session_start();\n";
		for(Entity entity : this.model.getEntities()){			
			this.content += "require(\"" + PHPGenerator.BUSINESS + "/" + entity.getName() + ".php\");\n";
		}
		this.content += "ini_set(\"display_errors\",\"1\");\n";
		this.content += "date_default_timezone_set(\"America/Bogota\");\n";
		this.content += "$webPagesNoAuthentication = array(\n";
		this.content += "\t'" + PHPGenerator.UI + "/recoverPassword.php',\n";				
		this.content += ");\n";
		this.content += "$webPages = array(\n";
		for(Entity entity : this.model.getEntities()){
			if(!entity.isLog()){
				if(entity.isActor() || entity.isAdmin()){
					this.content += "\t'" + PHPGenerator.UI + "/session" + entity.getName() + ".php',\n";				
				}
				this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/insert" + entity.getName() + ".php',\n";
				this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + entity.getName() + ".php',\n";
				this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php',\n";
				this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + ".php',\n";
				if(entity.isActor()) {
					this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/updateProfile" + entity.getName() + ".php',\n";
					this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/updatePassword" + entity.getName() + ".php',\n";
					this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/updateProfilePicture" + entity.getName() + ".php',\n";
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("*")){
						Entity objEntity = this.model.findEntity(relation.getEntity());
						if(!objEntity.isLog()) {
							this.content += "\t'" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/selectAll" + relation.getEntity() + "By" + entity.getName() + ".php',\n";																					
						}
					}
				}
				for(Attribute attribute : entity.getAttributes()) {
					if(attribute.isImage() || attribute.isFile()) {
						this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php',\n";						
					}
				}
			}else{
				this.content += "\t'" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + ".php',\n";
			}
		}
		this.content += ");\n";
		this.content += "if(isset($_GET['logOut'])){\n";
		this.content += "\t$_SESSION['id']=\"\";\n";
		this.content += "}\n";
		this.content += "?>\n";
		this.content += "<!DOCTYPE html>\n";
		this.content += "<html lang=\"en\">\n";
		this.content += "\t<head>\n";
		this.content += "\t\t<title>" + model.getAcronym() + "</title>\n";
		this.content += "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
		this.content += "\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n";
		this.content += "\t\t<link rel=\"icon\" type=\"image/png\" href=\"img/logo.png\" />\n";			
		if (this.model.getTemplate()==null) {
			this.content += "\t\t<link href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" rel=\"stylesheet\" />\n";			
		}else {
			this.content += "\t\t<link href=\"https://bootswatch.com/4/" + this.model.getTemplate() + "/bootstrap.css\" rel=\"stylesheet\" />\n";
		}
		this.content += "\t\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css\" rel=\"stylesheet\">\n";	    
		this.content += "\t\t<link href=\"https://use.fontawesome.com/releases/v5.11.1/css/all.css\" rel=\"stylesheet\" />\n";	    
		this.content += "\t\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css\" rel=\"stylesheet\" />\n";	    
		this.content += "\t\t<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>\n";
		this.content += "\t\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script>\n";
		this.content += "\t\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\"></script>\n";
		this.content += "\t\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js\"></script>\n";
		this.content += "\t\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js\"></script>\n";
		this.content += "\t\t<script charset=\"utf-8\">\n";
		this.content += "\t\t\t$(function () { \n";
		this.content += "\t\t\t\t$(\"[data-toggle='tooltip']\").tooltip(); \n";
		this.content += "\t\t\t});\n";
		this.content += "\t\t</script>\n";
		this.content += "\t</head>\n";
		this.content += "\t<body>\n";
		this.content += "\t\t<?php\n";
		this.content += "\t\tif(empty($_GET['pid'])){\n";
		this.content += "\t\t\tinclude('" + PHPGenerator.UI + "/home.php' );\n";
		this.content += "\t\t}else{\n";
		this.content += "\t\t\t$pid=base64_decode($_GET['pid']);\n";
		this.content += "\t\t\tif(in_array($pid, $webPagesNoAuthentication)){\n";
		this.content += "\t\t\t\tinclude($pid);\n";
		this.content += "\t\t\t}else{\n";
		this.content += "\t\t\t\tif($_SESSION['id']==\"\"){\n";
		this.content += "\t\t\t\t\theader(\"Location: ?\");\n";
		this.content += "\t\t\t\t\tdie();\n";
		this.content += "\t\t\t\t}\n";
		for(Entity entity : this.model.getEntities()){
			if(entity.isAdmin() || entity.isActor()){
				this.content += "\t\t\t\tif($_SESSION['entity']==\"" + entity.getName() + "\"){\n";
				this.content += "\t\t\t\t\tinclude('" + PHPGenerator.UI + "/menu" + entity.getName() + ".php');\n";
				this.content += "\t\t\t\t}\n";
			}
		}
		this.content += "\t\t\t\tif (in_array($pid, $webPages)){\n";
		this.content += "\t\t\t\t\tinclude($pid);\n";
		this.content += "\t\t\t\t}else{\n";
		this.content += "\t\t\t\t\tinclude('" + PHPGenerator.UI + "/error.php');\n";
		this.content += "\t\t\t\t}\n";
		this.content += "\t\t\t}\n";
		this.content += "\t\t}\n";
		this.content += "\t\t?>\n";
		this.content += "\t\t<div class=\"text-center text-muted\">ITI &copy; <?php echo date(\"Y\")?></div>\n";
		this.content += "\t</body>\n";
		this.content += "</html>\n";			
		return this.fileUtility.writePHP(content, generationDirectory + "/index.php");
	}

	private String createIndexAjax(String generationDirectory) throws IOException {
		this.content = "";
		this.content += "<?php \n";
		for(Entity entity : this.model.getEntities()){			
			this.content += "require(\"" + PHPGenerator.BUSINESS + "/" + entity.getName() + ".php\");\n";
		}
		this.content += "$pid=base64_decode($_GET['pid']);\n";
		this.content += "date_default_timezone_set('America/Bogota');\n";
		this.content += "include($pid);\n";
		this.content += "?>\n";
		return this.fileUtility.writePHP(content, generationDirectory + "/indexAjax.php");
	}

	private String createError(String generationDirectory) throws IOException {
		this.content = "";
		this.content += "<div class=\"container\">\n";
		this.content += "\t\t<div class=\"card\">\n";
		this.content += "\t\t\t<div class=\"card-header\">\n";
		this.content += "\t\t\t\t<h4 class=\"card-title\">Error</h4>\n";
		this.content += "\t\t\t</div>\n";
		this.content += "\t\t\t<div class=\"card-body\">\n";
		this.content += "\t\t\t<p>Web page not found</p>\n";
		this.content += "\t\t\t</div>\n";
		this.content += "\t\t</div>\n";
		this.content += "</div>\n";			
		return this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/error.php");
	}

	private String createHome(String generationDirectory) throws IOException {
		this.content = "";
		this.content += "<?php\n";
		this.content += "$logInError=false;\n";
		this.content += "$enabledError=false;\n";
		this.content += "if(isset($_POST['logIn'])){\n";
		this.content += "\tif(isset($_POST['email']) && isset($_POST['password'])){\n";
		this.content += "\t\t$user_ip = getenv('REMOTE_ADDR');\n";
		this.content += "\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
		this.content += "\t\t$browser = \"-\";\n";
		this.content += "\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
		this.content += "\t\t\t$browser = \"Internet Explorer\";\n";
		this.content += "\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
		this.content += "\t\t\t$browser = \"Chrome\";\n";
		this.content += "\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
		this.content += "\t\t\t$browser = \"Edge\";\n";
		this.content += "\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
		this.content += "\t\t\t$browser = \"Firefox\";\n";
		this.content += "\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
		this.content += "\t\t\t$browser = \"Opera\";\n";
		this.content += "\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
		this.content += "\t\t\t$browser = \"Safari\";\n";
		this.content += "\t\t}\n";
		this.content += "\t\t$email=$_POST['email'];\n";
		this.content += "\t\t$password=$_POST['password'];\n";
		for(Entity entity : this.model.getEntities()){
			if(entity.isActor() || entity.isAdmin()){
				this.content += "\t\t$" + entity.getName().toLowerCase() + " = new "+entity.getName() + "();\n";
				this.content += "\t\tif($" + entity.getName().toLowerCase() + " -> logIn($email, $password)){\n";
				this.content += "\t\t\tif($" + entity.getName().toLowerCase() + " -> get" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_State") + "()==1){\n";
				this.content += "\t\t\t\t$_SESSION['id']=$" + entity.getName().toLowerCase() + " -> getId" + entity.getName() + "();\n";
				this.content += "\t\t\t\t$_SESSION['entity']=\"" + entity.getName() + "\";\n";					
				this.content += "\t\t\t\t$log" + entity.getName() + " = new Log" + entity.getName() + "(\"\", \"Log In\", \"\", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $" + entity.getName().toLowerCase() + " -> getId" + entity.getName() + "());\n";
				this.content += "\t\t\t\t$log" + entity.getName() + " -> insert();\n";
				this.content += "\t\t\t\techo \"<script>location.href = '?pid=\" . base64_encode(\"" + PHPGenerator.UI+"/session" + entity.getName() + ".php\") . \"'</script>\"; \n";
				this.content += "\t\t\t} else { \n";
				this.content += "\t\t\t\t$enabledError=true; \n";
				this.content += "\t\t\t}\n";
				this.content += "\t\t}\n";
			}
		}
		this.content += "\t\t$logInError=true;\n";				
		this.content += "\t}\n";
		this.content += "}\n";
		this.content += "?>\n";
		this.content += "<div align=\"center\">\n";
		this.content += "\t<?php include(\"" + PHPGenerator.UI + "/header.php\"); ?>\n";
		this.content += "</div>\n";
		this.content += "<div class=\"container\">\n";
		this.content += "\t<div class=\"row\">\n";
		this.content += "\t\t<div class=\"col-md-9\">\n";
		this.content += "\t\t\t<div class=\"card\">\n";
		this.content += "\t\t\t\t<div class=\"card-header\">\n";
		this.content += "\t\t\t\t\t<h4><strong>" + model.getAcronym() + "</strong></h4>\n";
		this.content += "\t\t\t\t</div>\n";
		this.content += "\t\t\t\t<div class=\"card-body\">\n";
		this.content += "\t\t\t\t\t<p>" + model.getDescription() + "</p>\n";
		this.content += "\t\t\t\t</div>\n";
		this.content += "\t\t\t</div>\n";
		this.content += "\t\t</div>\n";
		this.content += "\t\t<div class=\"col-md-3\">\n";
		this.content += "\t\t\t<div class=\"card\">\n";
		this.content += "\t\t\t\t<div class=\"card-header\">\n";
		this.content += "\t\t\t\t\t<h4><strong>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_LogIn") + "</strong></h4>\n";
		this.content += "\t\t\t\t</div>\n";
		this.content += "\t\t\t\t<div class=\"card-body\">\n";
		this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?\" class=\"bootstrap-form needs-validation\"  >\n";
		this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
		this.content += "\t\t\t\t\t\t\t<div class=\"input-group\" >\n";
		this.content += "\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "\" autocomplete=\"off\" required />\n";
		this.content += "\t\t\t\t\t\t\t</div>\n";
		this.content += "\t\t\t\t\t\t</div>\n";
		this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
		this.content += "\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "\" required />\n";
		this.content += "\t\t\t\t\t\t</div>\n";
		this.content += "\t\t\t\t\t\t<?php if($enabledError){\n";
		this.content += "\t\t\t\t\t\t\techo \"<div class='alert alert-danger' >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert2") + "</div>\";\n";
		this.content += "\t\t\t\t\t\t} else if ($logInError){\n";
		this.content += "\t\t\t\t\t\t\techo \"<div class='alert alert-danger' >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert1") + "</div>\";\n";
		this.content += "\t\t\t\t\t\t} ?>\n";
		this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
		this.content += "\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"logIn\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_LogIn") + "</button>\n";
		this.content += "\t\t\t\t\t\t</div>\n";
		this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
		this.content += "\t\t\t\t\t\t\t<a href=\"?pid=<?php echo base64_encode(\"ui/recoverPassword.php\") ?>\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_RecoverPassword") + "</a>\n";
		this.content += "\t\t\t\t\t\t</div>\n";
		this.content += "\t\t\t\t\t</form>\n";
		this.content += "\t\t\t\t</div>\n";
		this.content += "\t\t\t</div>\n";
		this.content += "\t\t</div>\n";
		this.content += "\t</div>\n";
		this.content += "</div>\n";
		return this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/home.php");
	}

	private String createHeader(String generationDirectory) throws IOException {
		this.content = "";		
		this.content += "<div class=\"container\">\n";
		this.content += "\t<div class=\"row\"><div class=\"col-md-12 text-center\">&nbsp;</div></div>\n";
		this.content += "\t<div class=\"row\">\n";
		this.content += "\t\t<div class=\"col-md-3 text-center\">\n";
		this.content += "\t\t\t<a href=\"\"><img src=\"img/logo.png\" width=\"100px\" /></a>\n";
		this.content += "\t\t</div>\n";
		this.content += "\t\t<div class=\"col-md-6 text-center\">\n";
		this.content += "\t\t\t<h2>" + this.model.getName() + "</h2>\n";
		this.content += "\t\t</div>\n";
		this.content += "\t\t<div class=\"col-md-3 text-center\">\n";
		this.content += "\t\t\t<a href=\"http://itiud.org\" target=\"_blank\"><img src=\"img/iti.png\" width=\"100px\" /></a>\n";
		this.content += "\t\t</div>\n";
		this.content += "\t</div>\n";
		this.content += "\t\t<div class=\"row\"></div>\n";
		this.content += "\t<div class=\"row\"><div class=\"col-md-12 text-center\">&nbsp;</div></div>\n";
		this.content += "</div>\n";
		return this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/header.php");
	}	

	private String createSession(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			if(entity.isActor()){
				this.content = "";
				this.content += "<?php\n";
				this.content += "$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "($_SESSION['id']);\n";
				this.content += "$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> select();\n";
				this.content += "?>\n";
				this.content += "<div class=\"container\">\n";
				this.content += "\t<div>\n";
				this.content += "\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t<h3>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Profile") + "</h3>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t<div class=\"row\">\n";
				this.content += "\t\t\t\t<div class=\"col-md-3\">\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey()&& !attribute.getType().equals(Attribute.PASSWORD)){
						if(attribute.isImage()){
							this.content += "\t\t\t\t\t<img src=\"<?php echo ($" + entity.getName().toLowerCase() + " -> get" + attribute.getName().substring(0,1).toUpperCase()+attribute.getName().substring(1) +"()!=\"\")?$" + entity.getName().toLowerCase() + " -> get" + attribute.getName().substring(0,1).toUpperCase()+attribute.getName().substring(1) +"():\"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user2-2-icon.png\"; ?>\" width=\"100%\" class=\"rounded\">\n";
						}
					}
				}
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t\t<div class=\"col-md-9\">\n";
				this.content += "\t\t\t\t\t<div class=\"table-responsive-sm\">\n";
				this.content += "\t\t\t\t\t\t<table class=\"table table-striped table-hover\">\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.isImage()){
						this.content += "\t\t\t\t\t\t\t<tr>\n";
						this.content += "\t\t\t\t\t\t\t\t<th>"+Utilities.replaceCapitalLetter(attribute.getName().substring(0, 1).toUpperCase()+attribute.getName().substring(1))+"</th>\n";
						if(attribute.isUrl()) {
							this.content += "\t\t\t\t\t\t\t\t<?php if($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() != \"\"){ ?>\n";
							this.content += "\t\t\t\t\t\t\t\t\t<td><a href=\"<?php echo $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" target=\"_blank\"><span class='fas fa-external-link-alt' data-placement='left' data-toggle='tooltip' data-original-title='<?php echo $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() ?>' ></span></a></td>\n";
							this.content += "\t\t\t\t\t\t\t\t<?php }else{ ?>\n";
							this.content += "\t\t\t\t\t\t\t\t\t<td></td>\n";
							this.content += "\t\t\t\t\t\t\t\t<?php } ?>\n";
						} else if (attribute.getType().equals(Attribute.STATE)) {
							this.content += "\t\t\t\t\t\t\t\t<td><?php echo ($" + entity.getName().toLowerCase() + " -> get" + attribute.getName().substring(0,1).toUpperCase()+attribute.getName().substring(1) +"()==1)?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "\"; ?></td>\n";							
						}else {
							this.content += "\t\t\t\t\t\t\t\t<td><?php echo $" + entity.getName().toLowerCase() + " -> get" + attribute.getName().substring(0,1).toUpperCase()+attribute.getName().substring(1) +"() ?></td>\n";							
						}
						this.content += "\t\t\t\t\t\t\t</tr>\n";
					}
				}
				this.content += "\t\t\t\t\t\t</table>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t\t<div class=\"card-footer\">\n";
				this.content += "\t\t<p><?php echo \"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag1") + ": " + entity.getName() + "\"; ?></p>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";			
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/session" + entity.getName() + ".php");			}
		}
		return consoleStream;
	}

	private String createMenu(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entityForMenu : this.model.getEntities()) {
			if(entityForMenu.isActor()) {
				this.content = "";
				this.content += "<?php\n";
				this.content += "$" + entityForMenu.getName().substring(0, 1).toLowerCase() + entityForMenu.getName().substring(1) + " = new " + entityForMenu.getName() + "($_SESSION['id']);\n";
				this.content += "$" + entityForMenu.getName().substring(0, 1).toLowerCase() + entityForMenu.getName().substring(1) + " -> select();\n";
				this.content += "?>\n";
				this.content += "<nav class=\"navbar sticky-top navbar-expand-lg navbar-dark bg-dark mb-3\" >\n";
				this.content += "\t<a class=\"navbar-brand\" href=\"?pid=<?php echo base64_encode(\"ui/session" + entityForMenu.getName() + ".php\") ?>\"><span class=\"fas fa-home\" aria-hidden=\"true\"></span></a>\n";
				this.content += "\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\"> <span class=\"navbar-toggler-icon\"></span></button>\n";
				this.content += "\t<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n";
				this.content += "\t\t<ul class=\"navbar-nav mr-auto\">\n";
				if(entityForMenu.isAdmin() || entityForMenu.hasCreateService()) {
					this.content += "\t\t\t<li class=\"nav-item dropdown\">\n";
					this.content += "\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + "</a>\n";
					this.content += "\t\t\t\t<div class=\"dropdown-menu\">\n";				
					for(Entity entity : this.model.getEntities()){
						if(!entity.isLog() && entity.isMenuDeploy()){
							if(entityForMenu.isAdmin() || entityForMenu.hasService(entity.getName(), Model.CREATE)) {
								this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/insert" + entity.getName() + ".php\") ?>\">" + Utilities.replaceCapitalLetter(entity.getName()) + "</a>\n";										
							}
						}
					}
					this.content += "\t\t\t\t</div>\n";
					this.content += "\t\t\t</li>\n";					
				}
				if(entityForMenu.isAdmin() || entityForMenu.hasGetService()) {
					this.content += "\t\t\t<li class=\"nav-item dropdown\">\n";
					this.content += "\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_GetAll") + "</a>\n";
					this.content += "\t\t\t\t<div class=\"dropdown-menu\">\n";				
					for(Entity entity : this.model.getEntities()){
						if(!entity.isLog() && entity.isMenuDeploy()){
							if(entityForMenu.isAdmin() || entityForMenu.hasService(entity.getName(), Model.GET)) {
								this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php\") ?>\">" + Utilities.replaceCapitalLetter(entity.getName()) + "</a>\n";										
							}
						}
					}
					this.content += "\t\t\t\t</div>\n";
					this.content += "\t\t\t</li>\n";
					this.content += "\t\t\t<li class=\"nav-item dropdown\">\n";
					this.content += "\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Search") + "</a>\n";
					this.content += "\t\t\t\t<div class=\"dropdown-menu\">\n";				
					for(Entity entity : this.model.getEntities()){
						if(!entity.isLog() && entity.isMenuDeploy()){
							if(entityForMenu.isAdmin() || entityForMenu.hasService(entity.getName(), Model.GET)) {
								this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + ".php\") ?>\">" + Utilities.replaceCapitalLetter(entity.getName()) + "</a>\n";										
							}
						}
					}
					this.content += "\t\t\t\t</div>\n";
					this.content += "\t\t\t</li>\n";
				}
				if(entityForMenu.isAdmin()) {
					this.content += "\t\t\t<li class=\"nav-item dropdown\">\n";
					this.content += "\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\" data-toggle=\"dropdown\">Log</a>\n";
					this.content += "\t\t\t\t<div class=\"dropdown-menu\">\n";				
					for(Entity entity : this.model.getEntities()){
						if(entity.isLog()){
							this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + ".php\") ?>\">" + Utilities.replaceCapitalLetter(entity.getName()) + "</a>\n";										
						}
					}
					this.content += "\t\t\t\t</div>\n";
					this.content += "\t\t\t</li>\n";					
				}
				this.content += "\t\t</ul>\n";
				this.content += "\t\t<ul class=\"navbar-nav\">\n";
				this.content += "\t\t\t<li class=\"nav-item dropdown\">\n";
				String stringToDeploy="";
				for(Attribute attribute : entityForMenu.getAttributes()) {
					if(attribute.isDeploy()) {
						stringToDeploy += "$" + entityForMenu.getName().substring(0, 1).toLowerCase() + entityForMenu.getName().substring(1) + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() . \" \" . ";
					}
				}
				stringToDeploy = stringToDeploy.substring(0, stringToDeploy.length()-9);				
				this.content += "\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\"  data-toggle=\"dropdown\">" + Utilities.replaceCapitalLetter(entityForMenu.getName()) + ": <?php echo " + stringToDeploy + " ?><span class=\"caret\"></span></a>\n";
				this.content += "\t\t\t\t<div class=\"dropdown-menu\" >\n";
				this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entityForMenu.getName().substring(0, 1).toLowerCase() + entityForMenu.getName().substring(1) + "/updateProfile" + entityForMenu.getName() + ".php\") ?>\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Profile") + "</a>\n";
				this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entityForMenu.getName().substring(0, 1).toLowerCase() + entityForMenu.getName().substring(1) + "/updatePassword" + entityForMenu.getName() + ".php\") ?>\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "</a>\n";
				this.content += "\t\t\t\t\t<a class=\"dropdown-item\" href=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entityForMenu.getName().substring(0, 1).toLowerCase() + entityForMenu.getName().substring(1) + "/updateProfilePicture" + entityForMenu.getName() + ".php\") ?>\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + "</a>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</li>\n";
				this.content += "\t\t\t<li class=\"nav-item\">\n";
				this.content += "\t\t\t\t<a class=\"nav-link\" href=\"?logOut=1\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_LogOut") + "</a>\n";		
				this.content += "\t\t\t</li>\n";
				this.content += "\t\t</ul>\n";
				this.content += "\t</div>\n";
				this.content += "</nav>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/menu" + entityForMenu.getName() + ".php");			
			}
		}
		return consoleStream;
	}

	private String createInsert(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			this.content = "";
			if(!entity.isLog()){
				this.content += "<?php\n";
				this.content += "$processed=false;\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.isImage()){
						if(attribute.getType().equals(Attribute.DATE)){
							this.content += "$" + attribute.getName() + "=date(\"d/m/Y\");\n";
						}else{
							this.content += "$" + attribute.getName() + "=\"\";\n";	
						}
						this.content += "if(isset($_POST['" + attribute.getName() + "'])){\n";
						this.content += "\t$" + attribute.getName() + "=$_POST['" + attribute.getName() + "'];\n";
						this.content += "}\n";											
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=\"\";\n";
						this.content += "if(isset($_POST['" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "'])){\n";
						this.content += "\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=$_POST['" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "'];\n";
						this.content += "}\n";
						this.content += "if(isset($_GET['id" + relation.getEntity() + "'])){\n";
						this.content += "\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=$_GET['id" + relation.getEntity() + "'];\n";
						this.content += "}\n";						
					}
				}
				this.content += "if(isset($_POST['insert'])){\n";			
				this.content += "\t$new" + entity.getName() + " = new " + entity.getName() + "(";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.isImage()){
						this.content += "$"+attribute.getName() + ", ";
					}else{
						this.content += "\"\", ";
					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ", ";						
					}
				}	
				this.content=this.content.substring(0, this.content.length()-2);
				this.content += ");\n";
				this.content += "\t$new" + entity.getName() + " -> insert();\n";
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t$obj" + relation.getEntity() + " = new " + relation.getEntity() + "($" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ");\n";
						this.content += "\t$obj" + relation.getEntity() + " -> select();\n";
						this.content += "\t$name" + relation.getEntity() + " = ";					
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								this.content += "$obj" + relation.getEntity() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "() . \" \" . ";
							}
						}
						this.content = this.content.substring(0, this.content.length()-8);
						this.content += ";\n";
					}
				}
				this.content += "\t$user_ip = getenv('REMOTE_ADDR');\n";
				this.content += "\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
				this.content += "\t$browser = \"-\";\n";
				this.content += "\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Internet Explorer\";\n";
				this.content += "\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Chrome\";\n";
				this.content += "\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Edge\";\n";
				this.content += "\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Firefox\";\n";
				this.content += "\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Opera\";\n";
				this.content += "\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Safari\";\n";
				this.content += "\t}\n";
				for(Entity entityForLog : model.getEntities()){
					if(entityForLog.isActor()){
						if(entityForLog.isAdmin()) {
							this.content += "\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
						}else {
							this.content += "\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
						}					
						this.content += "\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
						for(Attribute attribute : entity.getAttributes()){
							if(!attribute.isPrimaryKey() && !attribute.isImage()){
								this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $" + attribute.getName() + " . \"; ";
							}
						}
						for(Relation relation:entity.getRelations()){
							if(relation.getCardinality().equals("1")){
								this.content += Utilities.replaceCapitalLetter(relation.getEntity()) + ": \" . $name" + relation.getEntity() + " . \"; ";						
							}
						}
						this.content = this.content.substring(0, this.content.length()-6);
						this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
						this.content += "\t\t$log" + entityForLog.getName() + " -> insert();\n";
						this.content += "\t}\n";
					}
				}
				this.content += "\t$processed=true;\n";
				this.content += "}\n";
				this.content += "?>\n";	
				this.content += "<div class=\"container\">\n";
				this.content += "\t<div class=\"row\">\n";
				this.content += "\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t<div class=\"card\">\n";
				this.content += "\t\t\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert3") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } ?>\n";
				this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/insert" + entity.getName() + ".php\") ?>\" class=\"bootstrap-form needs-validation\"   >\n";
				for(Attribute attribute:entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.isImage() && !attribute.isFile()){
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1));						
						if(attribute.isMandatory()){
							this.content += "*";					
						}
						if(attribute.getInfo() != null) {
							this.content += " <span class='fas fa-info-circle' data-toggle='tooltip' data-placement='right' data-original-title='" + attribute.getInfo() + "' ></span>";
						}
						this.content += "</label>\n";
						if(attribute.getType().equals(Attribute.TEXT) || attribute.getType().equals(Attribute.LONG_TEXT)){
							this.content += "\t\t\t\t\t\t\t<textarea id=\"" + attribute.getName() + "\" name=\""+attribute.getName() + "\" ><?php echo $" + attribute.getName() + " ?></textarea>\n";
							this.content += "\t\t\t\t\t\t\t<script>\n";
							this.content += "\t\t\t\t\t\t\t\t$('#" + attribute.getName() + "').summernote({\n";
							this.content += "\t\t\t\t\t\t\t\t\ttabsize: 2,\n";
							this.content += "\t\t\t\t\t\t\t\t\theight: 100\n";
							this.content += "\t\t\t\t\t\t\t\t});\n";
							this.content += "\t\t\t\t\t\t\t</script>\n";
						}else if(attribute.getType().equals(Attribute.DATE)){							
							this.content += "\t\t\t\t\t\t\t<input type=\"date\" class=\"form-control\" name=\"" + attribute.getName() + "\" id=\""+attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\" autocomplete=\"off\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";
						}else if(attribute.getType().equals(Attribute.TIME)){							
							this.content += "\t\t\t\t\t\t\t<input type=\"time\" class=\"form-control\" name=\"" + attribute.getName() + "\" id=\""+attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\" autocomplete=\"off\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";
						}else if(attribute.getType().equals(Attribute.INT)){
							this.content += "\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";
						}else if(attribute.getType().equals(Attribute.EMAIL)){
							this.content += "\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\"  required />\n";
						}else if(attribute.getType().equals(Attribute.PASSWORD)){
							this.content += "\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\" required />\n";
						}else if(attribute.getType().equals(Attribute.STATE)){
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"1\" checked />\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check form-check-inline\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"0\" />\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
						}else if(attribute.getType().equals(Attribute.BOOLEAN)){
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"1\" checked />\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check form-check-inline\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"0\" />\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
						}else if(attribute.isUrl()){
							this.content += "\t\t\t\t\t\t\t<input type=\"url\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";
						}else{						
							this.content += "\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";						
						}
						this.content += "\t\t\t\t\t\t</div>\n";					
					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "*</label>\n";					
						this.content += "\t\t\t\t\t\t\t<select class=\"form-control\" name=\"" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "\" id=\"" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "\" data-placeholder=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Select") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "\" required >\n";
						this.content += "\t\t\t\t\t\t\t\t<option></option>\n";
						this.content += "\t\t\t\t\t\t\t\t<?php\n";
						this.content += "\t\t\t\t\t\t\t\t$obj" + relation.getEntity() + " = new "+relation.getEntity() + "();\n";
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						String attributeToOrder="";
						String dir="";
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy() && attributeToOrder.equals("")) {
								attributeToOrder = attribute.getName();
								dir=attribute.getOrder();
							}
						}
						this.content += "\t\t\t\t\t\t\t\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "s = $obj" + relation.getEntity() + " -> selectAllOrder(\"" + attributeToOrder + "\", \"" + ((dir==null)?"asc":dir) + "\");\n";
						this.content += "\t\t\t\t\t\t\t\tforeach($" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "s as $current" + relation.getEntity() + "){\n";
						this.content += "\t\t\t\t\t\t\t\t\techo \"<option value='\" . $current" + relation.getEntity() + " -> getId" + relation.getEntity() + "() . \"'\";\n";
						this.content += "\t\t\t\t\t\t\t\t\tif($current" + relation.getEntity() + " -> getId"+relation.getEntity() + "() == $"+relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "){\n";
						this.content += "\t\t\t\t\t\t\t\t\t\techo \" selected\";\n";
						this.content += "\t\t\t\t\t\t\t\t\t}\n";
						this.content += "\t\t\t\t\t\t\t\t\techo \">\" . ";
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								this.content += "$current" + relation.getEntity() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() . \" \" . ";
							}
						}
						this.content = this.content.substring(0, this.content.length()-8);
						this.content += ". \"";
						this.content += "</option>\";\n";
						this.content += "\t\t\t\t\t\t\t\t}\n";						
						this.content += "\t\t\t\t\t\t\t\t?>\n";
						this.content += "\t\t\t\t\t\t\t</select>\n";
						this.content += "\t\t\t\t\t\t</div>\n";	
					}
				}
				this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"insert\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + "</button>\n";
				this.content += "\t\t\t\t\t</form>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				if(entity.getRelations().size() > 0) {
					this.content += "<script>\n";
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$('#" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "').select2({});\n";
					}
				}				
				if(entity.getRelations().size() > 0) {
					this.content += "</script>\n";
				}
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/insert" + entity.getName() + ".php");
			}
		}
		return consoleStream;
	}

	private String createUpdate(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			this.content = "";
			if(!entity.isLog()){
				this.content += "<?php\n";
				this.content += "$processed=false;\n";
				this.content += "$id" + entity.getName() + " = $_GET['id" + entity.getName() + "'];\n";			
				this.content += "$update" + entity.getName() + " = new " + entity.getName() + "($id" + entity.getName() + ");\n";
				this.content += "$update" + entity.getName() + " -> select();\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.isImage()){
						if(attribute.getType().equals(Attribute.DATE)){
							this.content += "$" + attribute.getName() + "=date(\"d/m/Y\");\n";
						}else{
							this.content += "$" + attribute.getName() + "=\"\";\n";	
						}
						this.content += "if(isset($_POST['" + attribute.getName() + "'])){\n";
						this.content += "\t$" + attribute.getName() + "=$_POST['" + attribute.getName() + "'];\n";
						this.content += "}\n";											
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=\"\";\n";
						this.content += "if(isset($_POST['" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "'])){\n";
						this.content += "\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=$_POST['" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "'];\n";
						this.content += "}\n";						
					}
				}
				this.content += "if(isset($_POST['update'])){\n";			
				this.content += "\t$update" + entity.getName() + " = new " + entity.getName() + "($id" + entity.getName() + ", ";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey()){
						if(!attribute.getType().equals(Attribute.PASSWORD) && !attribute.isImage()) {
							this.content += "$" + attribute.getName() + ", ";	
						}else {
							this.content += "\"\", ";
						}
					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ", ";						
					}
				}	
				this.content=this.content.substring(0, this.content.length()-2);
				this.content += ");\n";
				this.content += "\t$update" + entity.getName() + " -> update();\n";
				this.content += "\t$update" + entity.getName() + " -> select();\n";
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t$obj" + relation.getEntity() + " = new " + relation.getEntity() + "($" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ");\n";
						this.content += "\t$obj" + relation.getEntity() + " -> select();\n";
						this.content += "\t$name" + relation.getEntity() + " = ";					
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								this.content += "$obj" + relation.getEntity() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "() . \" \" . ";
							}
						}
						this.content = this.content.substring(0, this.content.length()-8);
						this.content += ";\n";
					}
				}
				this.content += "\t$user_ip = getenv('REMOTE_ADDR');\n";
				this.content += "\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
				this.content += "\t$browser = \"-\";\n";
				this.content += "\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Internet Explorer\";\n";
				this.content += "\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Chrome\";\n";
				this.content += "\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Edge\";\n";
				this.content += "\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Firefox\";\n";
				this.content += "\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Opera\";\n";
				this.content += "\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Safari\";\n";
				this.content += "\t}\n";
				for(Entity entityForLog : model.getEntities()){
					if(entityForLog.isActor()){
						if(entityForLog.isAdmin()) {
							this.content += "\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
						}else {
							this.content += "\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
						}					
						this.content += "\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
						for(Attribute attribute : entity.getAttributes()){
							if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.isImage()){
								this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $" + attribute.getName() + " . \"; ";
							}
						}
						for(Relation relation:entity.getRelations()){
							if(relation.getCardinality().equals("1")){
								this.content += Utilities.replaceCapitalLetter(relation.getEntity()) + ": \" . $name" + relation.getEntity() + " . \";; ";						
							}
						}
						this.content = this.content.substring(0, this.content.length()-6);
						this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
						this.content += "\t\t$log" + entityForLog.getName() + " -> insert();\n";
						this.content += "\t}\n";
					}
				}
				this.content += "\t$processed=true;\n";
				this.content += "}\n";
				this.content += "?>\n";	
				this.content += "<div class=\"container\">\n";
				this.content += "\t<div class=\"row\">\n";
				this.content += "\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t<div class=\"card\">\n";
				this.content += "\t\t\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert4") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } ?>\n";
				this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $id" + entity.getName() + " ?>\" class=\"bootstrap-form needs-validation\"   >\n";
				for(Attribute attribute:entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.isImage() && !attribute.isFile()){
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1));						
						if(attribute.isMandatory()){
							this.content += "*";					
						}
						if(attribute.getInfo() != null) {
							this.content += " <span class='fas fa-info-circle' data-toggle='tooltip' data-placement='right' data-original-title='" + attribute.getInfo() + "' ></span>";
						}
						this.content += "</label>\n";
						if(attribute.getType().equals(Attribute.TEXT) || attribute.getType().equals(Attribute.LONG_TEXT)){
							this.content += "\t\t\t\t\t\t\t<textarea id=\"" + attribute.getName() + "\" name=\""+attribute.getName() + "\" ><?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?></textarea>\n";
							this.content += "\t\t\t\t\t\t\t<script>\n";
							this.content += "\t\t\t\t\t\t\t\t$('#" + attribute.getName() + "').summernote({\n";
							this.content += "\t\t\t\t\t\t\t\t\ttabsize: 2,\n";
							this.content += "\t\t\t\t\t\t\t\t\theight: 100\n";
							this.content += "\t\t\t\t\t\t\t\t});\n";
							this.content += "\t\t\t\t\t\t\t</script>\n";
						}else if(attribute.getType().equals(Attribute.DATE)){							
							this.content += "\t\t\t\t\t\t\t<input type=\"date\" class=\"form-control\" name=\"" + attribute.getName() + "\" id=\""+attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" autocomplete=\"off\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";		
						}else if(attribute.getType().equals(Attribute.TIME)){							
							this.content += "\t\t\t\t\t\t\t<input type=\"time\" class=\"form-control\" name=\"" + attribute.getName() + "\" id=\""+attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" autocomplete=\"off\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";
						}else if(attribute.getType().equals(Attribute.INT)){							
							this.content += "\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" name=\"" + attribute.getName() + "\" id=\""+attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";									
						}else if(attribute.getType().equals(Attribute.EMAIL)){
							this.content += "\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\"  required />\n";
						}else if(attribute.getType().equals(Attribute.STATE)){
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"1\" <?php echo ($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "()==1)?\"checked\":\"\" ?>/>\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check form-check-inline\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"0\" <?php echo ($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "()==0)?\"checked\":\"\" ?>/>\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
						}else if(attribute.getType().equals(Attribute.BOOLEAN)){
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"1\" <?php echo ($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "()==1)?\"checked\":\"\" ?>/>\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
							this.content += "\t\t\t\t\t\t\t<div class=\"form-check form-check-inline\">\n";
							this.content += "\t\t\t\t\t\t\t\t<input type=\"radio\" class=\"form-check-input\" name=\"" + attribute.getName() + "\" value=\"0\" <?php echo ($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "()==0)?\"checked\":\"\" ?>/>\n";
							this.content += "\t\t\t\t\t\t\t\t<label class=\"form-check-label\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "</label>\n";
							this.content += "\t\t\t\t\t\t\t</div>\n";
						}else if(attribute.isUrl()){
							this.content += "\t\t\t\t\t\t\t<input type=\"url\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $" + attribute.getName() + " ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";
						}else{						
							this.content += "\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";						
						}
						this.content += "\t\t\t\t\t\t</div>\n";					
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "*</label>\n";					
						this.content += "\t\t\t\t\t\t\t<select class=\"form-control\" name=\"" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "\" id=\"" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "\" data-placeholder=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Select") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "\" required >\n";
						this.content += "\t\t\t\t\t\t\t\t<option></option>\n";
						this.content += "\t\t\t\t\t\t\t\t<?php\n";
						this.content += "\t\t\t\t\t\t\t\t$obj" + relation.getEntity() + " = new "+relation.getEntity() + "();\n";
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						String attributeToOrder="";
						String dir="";
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy() && attributeToOrder.equals("")) {
								attributeToOrder = attribute.getName();
								dir=attribute.getOrder();
							}
						}
						this.content += "\t\t\t\t\t\t\t\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "s = $obj" + relation.getEntity() + " -> selectAllOrder(\"" + attributeToOrder + "\", \"" + ((dir==null)?"asc":dir) + "\");\n";
						this.content += "\t\t\t\t\t\t\t\tforeach($" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "s as $current" + relation.getEntity() + "){\n";
						this.content += "\t\t\t\t\t\t\t\t\techo \"<option value='\" . $current" + relation.getEntity() + " -> getId" + relation.getEntity() + "() . \"'\";\n";
						this.content += "\t\t\t\t\t\t\t\t\tif($current" + relation.getEntity() + " -> getId" + relation.getEntity() + "() == $update" + entity.getName() + " -> get" + relation.getEntity() + "() -> getId" + relation.getEntity() + "()){\n";
						this.content += "\t\t\t\t\t\t\t\t\t\techo \" selected\";\n";
						this.content += "\t\t\t\t\t\t\t\t\t}\n";
						this.content += "\t\t\t\t\t\t\t\t\techo \">\" . ";
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								this.content += "$current" + relation.getEntity() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() . \" \" . ";
							}
						}
						this.content = this.content.substring(0, this.content.length()-8);
						this.content += ". \"";
						this.content += "</option>\";\n";
						this.content += "\t\t\t\t\t\t\t\t}\n";						
						this.content += "\t\t\t\t\t\t\t\t?>\n";
						this.content += "\t\t\t\t\t\t\t</select>\n";
						this.content += "\t\t\t\t\t\t</div>\n";	
					}
				}
				this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"update\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + "</button>\n";
				this.content += "\t\t\t\t\t</form>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				if(entity.getRelations().size() > 0) {
					this.content += "<script>\n";
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$('#" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "').select2({});\n";
					}
				}				
				if(entity.getRelations().size() > 0) {
					this.content += "</script>\n";
				}				
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/update" + entity.getName() + ".php");
			}
		}
		return consoleStream;
	}

	private String createUpdateProfile(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			this.content = "";
			if(entity.isActor()){
				this.content += "<?php\n";
				this.content += "$processed=false;\n";
				this.content += "$update" + entity.getName() + " = new " + entity.getName() + "($_SESSION['id']);\n";
				this.content += "$update" + entity.getName() + " -> select();\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.getType().equals(Attribute.STATE) && !attribute.isImage()){
						if(attribute.getType().equals(Attribute.DATE)){
							this.content += "$" + attribute.getName() + "=date(\"Y-m-d\");\n";
						}else{
							this.content += "$" + attribute.getName() + "=\"\";\n";	
						}
						this.content += "if(isset($_POST['" + attribute.getName() + "'])){\n";
						this.content += "\t$" + attribute.getName() + "=$_POST['" + attribute.getName() + "'];\n";
						this.content += "}\n";											
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=\"\";\n";
						this.content += "if(isset($_POST['" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "'])){\n";
						this.content += "\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "=$_POST['" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "'];\n";
						this.content += "}\n";						
					}
				}
				this.content += "if(isset($_POST['update'])){\n";			
				this.content += "\t$update" + entity.getName() + " = new " + entity.getName() + "($_SESSION['id'], ";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey()){
						if(attribute.getType().equals(Attribute.PASSWORD) || attribute.isImage()) {
							this.content += "\"\", ";
						}else if (attribute.getType().equals(Attribute.STATE) ) {
							this.content += "\"1\", ";
						}else {
							this.content += "$" + attribute.getName() + ", ";
						}
					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ", ";						
					}
				}	
				this.content=this.content.substring(0, this.content.length()-2);
				this.content += ");\n";
				this.content += "\t$update" + entity.getName() + " -> update();\n";
				this.content += "\t$update" + entity.getName() + " -> select();\n";
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t$obj" + relation.getEntity() + " = new " + relation.getEntity() + "($" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ");\n";
						this.content += "\t$obj" + relation.getEntity() + " -> select();\n";
						this.content += "\t$name" + relation.getEntity() + " = ";					
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								this.content += "$obj" + relation.getEntity() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "() . \" \" . ";
							}
						}
						this.content = this.content.substring(0, this.content.length()-8);
						this.content += ";\n";
					}
				}
				this.content += "\t$user_ip = getenv('REMOTE_ADDR');\n";
				this.content += "\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
				this.content += "\t$browser = \"-\";\n";
				this.content += "\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Internet Explorer\";\n";
				this.content += "\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Chrome\";\n";
				this.content += "\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Edge\";\n";
				this.content += "\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Firefox\";\n";
				this.content += "\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Opera\";\n";
				this.content += "\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t$browser = \"Safari\";\n";
				this.content += "\t}\n";
				this.content += "\t$log" + entity.getName() + " = new Log" + entity.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " Profile " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.getType().equals(Attribute.STATE) && !attribute.isImage()){
						this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $" + attribute.getName() + " . \"; ";
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += Utilities.replaceCapitalLetter(relation.getEntity()) + ": \" . $name" + relation.getEntity() + " . \";; ";						
					}
				}
				this.content = this.content.substring(0, this.content.length()-6);
				this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
				this.content += "\t$log" + entity.getName() + " -> insert();\n";
				this.content += "\t$processed=true;\n";
				this.content += "}\n";
				this.content += "?>\n";	
				this.content += "<div class=\"container\">\n";
				this.content += "\t<div class=\"row\">\n";
				this.content += "\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t<div class=\"card\">\n";
				this.content += "\t\t\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " Profile " + entity.getName() + "</h4>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert4") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } ?>\n";
				this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/updateProfile" + entity.getName() + ".php\") ?>\" class=\"bootstrap-form needs-validation\"   >\n";
				for(Attribute attribute:entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && !attribute.getType().equals(Attribute.STATE) && !attribute.isImage()){
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1));						
						if(attribute.isMandatory()){
							this.content += "*";					
						}
						this.content += "</label>\n";
						if(attribute.getType().equals(Attribute.TEXT)){
							this.content += "\t\t\t\t\t\t\t<textarea id=\"" + attribute.getName() + "\" name=\""+attribute.getName() + "\" ><?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?></textarea>\n";
							this.content += "\t\t\t\t\t\t\t<script>\n";
							this.content += "\t\t\t\t\t\t\t\t$('#" + attribute.getName() + "').summernote({\n";
							this.content += "\t\t\t\t\t\t\t\t\ttabsize: 2,\n";
							this.content += "\t\t\t\t\t\t\t\t\theight: 100\n";
							this.content += "\t\t\t\t\t\t\t\t});\n";
							this.content += "\t\t\t\t\t\t\t</script>\n";
						}else if(attribute.getType().equals(Attribute.DATE)){							
							this.content += "\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"" + attribute.getName() + "\" id=\""+attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" autocomplete=\"off\" />\n";
							this.content += "\t\t\t\t\t\t\t<script>\n";
							this.content += "\t\t\t\t\t\t\t\t$( \"#"+attribute.getName() + "\" ).datepicker({\n";
							this.content += "\t\t\t\t\t\t\t\t\tuiLibrary: 'bootstrap4',\n";
							this.content += "\t\t\t\t\t\t\t\t\tformat: 'yyyy-mm-dd'\n";
							this.content += "\t\t\t\t\t\t\t\t});\n";
							this.content += "\t\t\t\t\t\t\t</script>\n";
						}else if(attribute.getType().equals(Attribute.EMAIL)){
							this.content += "\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\"  required />\n";
						}else if(attribute.getType().equals(Attribute.PASSWORD)){
							this.content += "\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" required />\n";
						}else{						
							this.content += "\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"" + attribute.getName() + "\" value=\"<?php echo $update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() ?>\"";
							if(attribute.isMandatory()){
								this.content += " required ";
							}
							this.content += "/>\n";						
						}
						this.content += "\t\t\t\t\t\t</div>\n";					
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t<label>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "*</label>\n";					
						this.content += "\t\t\t\t\t\t<select class=\"form-control\" name=\"" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "\">\n";
						this.content += "\t\t\t\t\t\t\t<?php\n";
						this.content += "\t\t\t\t\t\t\t$obj" + relation.getEntity() + " = new "+relation.getEntity() + "();\n";
						this.content += "\t\t\t\t\t\t\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "s = $obj" + relation.getEntity() + " -> selectAll();\n";
						this.content += "\t\t\t\t\t\t\tforeach($" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "s as $current" + relation.getEntity() + "){\n";
						this.content += "\t\t\t\t\t\t\t\techo \"<option value='\" . $current" + relation.getEntity() + " -> getId" + relation.getEntity() + "() . \"'\";\n";
						this.content += "\t\t\t\t\t\t\t\tif($current" + relation.getEntity() + " -> getId" + relation.getEntity() + "() == $update" + entity.getName() + " -> get" + relation.getEntity() + "() -> getId" + relation.getEntity() + "()){\n";
						this.content += "\t\t\t\t\t\t\t\t\techo \" selected\";\n";
						this.content += "\t\t\t\t\t\t\t\t}\n";
						this.content += "\t\t\t\t\t\t\t\techo \">\" . ";
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								this.content += "$current" + relation.getEntity() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() . \" \" . ";
							}
						}
						this.content = this.content.substring(0, this.content.length()-8);
						this.content += ". \"";
						this.content += "</option>\";\n";
						this.content += "\t\t\t\t\t\t\t}\n";						
						this.content += "\t\t\t\t\t\t\t?>\n";
						this.content += "\t\t\t\t\t\t</select>\n";
						this.content += "\t\t\t\t\t</div>\n";	
					}
				}
				this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"update\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + "</button>\n";
				this.content += "\t\t\t\t\t</form>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/updateProfile" + entity.getName() + ".php");
			}
		}
		return consoleStream;
	}

	private String createUpdatePassword(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			this.content = "";
			if(entity.isActor()){
				this.content += "<?php\n";
				this.content += "$processed = false;\n";
				this.content += "$error = 0;\n";
				this.content += "if(isset($_POST['update'])){\n";			
				this.content += "\tif($_POST['newPassword'] == $_POST['newPasswordConfirm']){\n";			
				this.content += "\t\t$update" + entity.getName() + " = new " + entity.getName() + "($_SESSION['id']);\n";
				this.content += "\t\t$update" + entity.getName() + " -> select();\n";
				this.content += "\t\tif($update" + entity.getName() + " -> get" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "() == md5($_POST['currentPassword'])){\n";
				this.content += "\t\t\t$update" + entity.getName() + " -> update" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "($_POST['newPassword']);\n";
				this.content += "\t\t\t$user_ip = getenv('REMOTE_ADDR');\n";
				this.content += "\t\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
				this.content += "\t\t\t$browser = \"-\";\n";
				this.content += "\t\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
				this.content += "\t\t\t\t$browser = \"Internet Explorer\";\n";
				this.content += "\t\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t\t$browser = \"Chrome\";\n";
				this.content += "\t\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
				this.content += "\t\t\t\t$browser = \"Edge\";\n";
				this.content += "\t\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t\t$browser = \"Firefox\";\n";
				this.content += "\t\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t\t$browser = \"Opera\";\n";
				this.content += "\t\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t\t$browser = \"Safari\";\n";
				this.content += "\t\t\t}\n";
				this.content += "\t\t\t$log" + entity.getName() + " = new Log" + entity.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_EditPassword") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"\"";
				this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
				this.content += "\t\t\t$log" + entity.getName() + " -> insert();\n";
				this.content += "\t\t\t$processed = true;\n";
				this.content += "\t\t} else {\n";
				this.content += "\t\t\t$error = 2;\n";
				this.content += "\t\t}\n";
				this.content += "\t} else {\n";
				this.content += "\t\t$error = 1;\n";
				this.content += "\t}\n";
				this.content += "}\n";
				this.content += "?>\n";	
				this.content += "<div class=\"container\">\n";
				this.content += "\t<div class=\"row\">\n";
				this.content += "\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t<div class=\"card\">\n";
				this.content += "\t\t\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_EditPassword") + " " + entity.getName() + "</h4>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert6") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } else if($error == 1) { ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-danger\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert7") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } else if($error == 2) { ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-danger\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert8") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } ?>\n";
				this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/updatePassword" + entity.getName() + ".php\") ?>\" class=\"bootstrap-form needs-validation\"   >\n";
				this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
				this.content += "\t\t\t\t\t\t\t<label>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_CurrentPassword") + "*</label>\n";												
				this.content += "\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"currentPassword\" required />\n";
				this.content += "\t\t\t\t\t\t</div>\n";					
				this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
				this.content += "\t\t\t\t\t\t\t<label>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_NewPassword") + "*</label>\n";						
				this.content += "\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"newPassword\" required />\n";
				this.content += "\t\t\t\t\t\t</div>\n";					
				this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
				this.content += "\t\t\t\t\t\t\t<label>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_ConfirmPassword") + "*</label>\n";						
				this.content += "\t\t\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"newPasswordConfirm\" required />\n";
				this.content += "\t\t\t\t\t\t</div>\n";					
				this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"update\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + "</button>\n";
				this.content += "\t\t\t\t\t</form>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/updatePassword" + entity.getName() + ".php");
			}
		}
		return consoleStream;
	}
	
	private String createUpdateProfilePicture(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			this.content = "";
			if(entity.isActor()){
				this.content += "<?php\n";
				this.content += "$processed=false;\n";
				this.content += "$update" + entity.getName() + " = new " + entity.getName() + "($_SESSION['id']);\n";
				this.content += "$update" + entity.getName() + " -> select();\n";
				this.content += "$error = 0;\n";
				this.content += "if(isset($_POST['update'])){\n";
				this.content += "\t$localPath=$_FILES['image']['tmp_name'];\n";			
				this.content += "\t$type=$_FILES['image']['type'];\n";			
				this.content += "\tif($type!=\"" + PHPGenerator.IMAGE + "/png\" && $type!=\"" + PHPGenerator.IMAGE + "/jpg\" && $type!=\"" + PHPGenerator.IMAGE + "/jpeg\"){\n";						
				this.content += "\t\t$error=1;\n";			
				this.content += "\t} else {\n";			
				this.content += "\t\tif (file_exists($update" + entity.getName() + " -> get" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + "())) {\n";
				this.content += "\t\t\tunlink($update" + entity.getName() + " -> get" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + "());\n";
				this.content += "\t\t}\n";
				this.content += "\t\t$serverPath = \"" + PHPGenerator.IMAGE + "/\" . time() . \".png\";\n";			
				this.content += "\t\tcopy($localPath,$serverPath);\n";
				this.content += "\t\t$update" + entity.getName() + " -> updateImage" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + "($serverPath);\n";
				this.content += "\t\t$user_ip = getenv('REMOTE_ADDR');\n";
				this.content += "\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
				this.content += "\t\t$browser = \"-\";\n";
				this.content += "\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
				this.content += "\t\t\t$browser = \"Internet Explorer\";\n";
				this.content += "\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t$browser = \"Chrome\";\n";
				this.content += "\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
				this.content += "\t\t\t$browser = \"Edge\";\n";
				this.content += "\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t$browser = \"Firefox\";\n";
				this.content += "\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t$browser = \"Opera\";\n";
				this.content += "\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
				this.content += "\t\t\t$browser = \"Safari\";\n";
				this.content += "\t\t}\n";
				for(Entity entityForLog : model.getEntities()){
					if(entityForLog.isActor()){
						if(entityForLog.isAdmin()) {
							this.content += "\t\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
						}else {
							this.content += "\t\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
						}					
						this.content += "\t\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " Profile " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + " in " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
						this.content += Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + ": \" . $serverPath";
						this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
						this.content += "\t\t\t$log" + entityForLog.getName() + " -> insert();\n";
						this.content += "\t\t}\n";
					}
				}
				this.content += "\t\t$processed = true;\n";
				this.content += "\t}\n";			
				this.content += "}\n";			
				this.content += "?>\n";	
				this.content += "<div class=\"container\">\n";
				this.content += "\t<div class=\"row\">\n";
				this.content += "\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t<div class=\"card\">\n";
				this.content += "\t\t\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " <i>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture").toLowerCase() + "</i> " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_of") + " " + entity.getName() + "</h4>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert5") + "\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } else if($error == 1) { ?>\n";
				this.content += "\t\t\t\t\t<div class=\"alert alert-danger\" >Error. The image must be png\n";
				this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
				this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
				this.content += "\t\t\t\t\t\t</button>\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t\t<?php } ?>\n";
				this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/updateProfilePicture" + entity.getName() + ".php\") ?>\" class=\"bootstrap-form needs-validation\" enctype=\"multipart/form-data\"   >\n";
				this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
				this.content += "\t\t\t\t\t\t\t<label>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Picture") + "*</label>\n";						
				this.content += "\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" name=\"image\" required />\n";
				this.content += "\t\t\t\t\t\t</div>\n";					
				this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"update\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + "</button>\n";
				this.content += "\t\t\t\t\t</form>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/updateProfilePicture" + entity.getName() + ".php");											
			}
		}		
		return consoleStream;
	}


	private String createUpdateImage(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			if(!entity.isLog()){
				for(Attribute attribute : entity.getAttributes()) {
					if(attribute.isImage()) {
						this.content = "";			
						this.content += "<?php\n";
						this.content += "$processed = false;\n";
						this.content += "$attribute = $_GET['attribute'];\n";
						this.content += "$id" + entity.getName() + "= $_GET['id" + entity.getName() + "'];\n";
						this.content += "$error = 0;\n";
						this.content += "if(isset($_POST['update'])){\n";
						this.content += "\t$localPath=$_FILES['image']['tmp_name'];\n";			
						this.content += "\t$type=$_FILES['image']['type'];\n";			
						this.content += "\tif($type!=\"" + PHPGenerator.IMAGE + "/png\" && $type!=\"" + PHPGenerator.IMAGE + "/jpg\" && $type!=\"" + PHPGenerator.IMAGE + "/jpeg\"){\n";			
						this.content += "\t\t$error=1;\n";			
						this.content += "\t} else {\n";			
						this.content += "\t\t$update" + entity.getName() + " = new " + entity.getName() + "($id" + entity.getName() + ");\n";
						this.content += "\t\t$update" + entity.getName() + " -> select();\n";
						this.content += "\t\tif (file_exists($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "())) {\n";
						this.content += "\t\t\tunlink($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "());\n";
						this.content += "\t\t}\n";
						this.content += "\t\t$serverPath = \"" + PHPGenerator.IMAGE + "/\" . time() . \".\" . substr($type, strpos($type, \"/\")+1);\n";			
						this.content += "\t\tcopy($localPath,$serverPath);\n";
						this.content += "\t\t$update" + entity.getName() + " -> updateImage" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($serverPath);\n";
						this.content += "\t\t$user_ip = getenv('REMOTE_ADDR');\n";
						this.content += "\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
						this.content += "\t\t$browser = \"-\";\n";
						this.content += "\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Internet Explorer\";\n";
						this.content += "\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Chrome\";\n";
						this.content += "\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Edge\";\n";
						this.content += "\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Firefox\";\n";
						this.content += "\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Opera\";\n";
						this.content += "\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Safari\";\n";
						this.content += "\t\t}\n";
						for(Entity entityForLog : model.getEntities()){
							if(entityForLog.isActor()){
								if(entityForLog.isAdmin()) {
									this.content += "\t\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
								}else {
									this.content += "\t\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
								}					
								this.content += "\t\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + attribute.getName() + " in " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
								this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $serverPath";
								this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
								this.content += "\t\t\t$log" + entityForLog.getName() + " -> insert();\n";
								this.content += "\t\t}\n";
							}
						}
						this.content += "\t\t$processed = true;\n";
						this.content += "\t}\n";			
						this.content += "}\n";			
						this.content += "?>\n";	
						this.content += "<div class=\"container\">\n";
						this.content += "\t<div class=\"row\">\n";
						this.content += "\t\t<div class=\"col-md-2\"></div>\n";
						this.content += "\t\t<div class=\"col-md-8\">\n";
						this.content += "\t\t\t<div class=\"card\">\n";
						this.content += "\t\t\t\t<div class=\"card-header\">\n";
						this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " <i>" + Utilities.replaceCapitalLetter(attribute.getName()).toLowerCase() + "</i> " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_of") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
						this.content += "\t\t\t\t</div>\n";
						this.content += "\t\t\t\t<div class=\"card-body\">\n";
						this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
						this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert5") + "\n";
						this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
						this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
						this.content += "\t\t\t\t\t\t</button>\n";
						this.content += "\t\t\t\t\t</div>\n";
						this.content += "\t\t\t\t\t<?php } else if($error == 1) { ?>\n";
						this.content += "\t\t\t\t\t<div class=\"alert alert-danger\" >Error. " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert10") + "\n";
						this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
						this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
						this.content += "\t\t\t\t\t\t</button>\n";
						this.content += "\t\t\t\t\t</div>\n";
						this.content += "\t\t\t\t\t<?php } ?>\n";
						this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") ?>&id" + entity.getName() + "=<?php echo $id" + entity.getName() + " ?>&attribute=<?php echo $attribute ?>\" class=\"bootstrap-form needs-validation\" enctype=\"multipart/form-data\"   >\n";
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "*</label>\n";						
						this.content += "\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" name=\"image\" required />\n";
						this.content += "\t\t\t\t\t\t</div>\n";					
						this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"update\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + "</button>\n";
						this.content += "\t\t\t\t\t</form>\n";
						this.content += "\t\t\t\t</div>\n";
						this.content += "\t\t\t</div>\n";
						this.content += "\t\t</div>\n";
						this.content += "\t</div>\n";
						this.content += "</div>\n";
						consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php");											
					}
				}
			}
		}
		return consoleStream;
	}

	private String createUpdateFile(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			if(!entity.isLog()){
				for(Attribute attribute : entity.getAttributes()) {
					if(attribute.isFile()) {
						this.content = "";			
						this.content += "<?php\n";
						this.content += "$processed = false;\n";
						this.content += "$attribute = $_GET['attribute'];\n";
						this.content += "$id" + entity.getName() + "= $_GET['id" + entity.getName() + "'];\n";
						this.content += "$error = 0;\n";
						this.content += "if(isset($_POST['update'])){\n";
						this.content += "\t$localPath=$_FILES['image']['tmp_name'];\n";			
						this.content += "\t$type=$_FILES['image']['type'];\n";			
						this.content += "\tif($type!=\"" + PHPGenerator.APPLICATION + "/pdf\"){\n";			
						this.content += "\t\t$error=1;\n";			
						this.content += "\t} else {\n";			
						this.content += "\t\t$update" + entity.getName() + " = new " + entity.getName() + "($id" + entity.getName() + ");\n";
						this.content += "\t\t$update" + entity.getName() + " -> select();\n";
						this.content += "\t\tif (file_exists($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "())) {\n";
						this.content += "\t\t\tunlink($update" + entity.getName() + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "());\n";
						this.content += "\t\t}\n";
						this.content += "\t\t$serverPath = \"" + PHPGenerator.FILE + "/\" . time() . \".\" . substr($type, strpos($type, \"/\")+1);\n";			
						this.content += "\t\tcopy($localPath,$serverPath);\n";
						this.content += "\t\t$update" + entity.getName() + " -> updateImage" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($serverPath);\n";
						this.content += "\t\t$user_ip = getenv('REMOTE_ADDR');\n";
						this.content += "\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
						this.content += "\t\t$browser = \"-\";\n";
						this.content += "\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Internet Explorer\";\n";
						this.content += "\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Chrome\";\n";
						this.content += "\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Edge\";\n";
						this.content += "\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Firefox\";\n";
						this.content += "\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Opera\";\n";
						this.content += "\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
						this.content += "\t\t\t$browser = \"Safari\";\n";
						this.content += "\t\t}\n";
						for(Entity entityForLog : model.getEntities()){
							if(entityForLog.isActor()){
								if(entityForLog.isAdmin()) {
									this.content += "\t\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
								}else {
									this.content += "\t\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
								}					
								this.content += "\t\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + attribute.getName() + " in " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
								this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $serverPath";
								this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
								this.content += "\t\t\t$log" + entityForLog.getName() + " -> insert();\n";
								this.content += "\t\t}\n";
							}
						}
						this.content += "\t\t$processed = true;\n";
						this.content += "\t}\n";			
						this.content += "}\n";			
						this.content += "?>\n";	
						this.content += "<div class=\"container\">\n";
						this.content += "\t<div class=\"row\">\n";
						this.content += "\t\t<div class=\"col-md-2\"></div>\n";
						this.content += "\t\t<div class=\"col-md-8\">\n";
						this.content += "\t\t\t<div class=\"card\">\n";
						this.content += "\t\t\t\t<div class=\"card-header\">\n";
						this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " <i>" + Utilities.replaceCapitalLetter(attribute.getName()).toLowerCase() + "</i> " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_of") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
						this.content += "\t\t\t\t</div>\n";
						this.content += "\t\t\t\t<div class=\"card-body\">\n";
						this.content += "\t\t\t\t\t<?php if($processed){ ?>\n";
						this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert5") + "\n";
						this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
						this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
						this.content += "\t\t\t\t\t\t</button>\n";
						this.content += "\t\t\t\t\t</div>\n";
						this.content += "\t\t\t\t\t<?php } else if($error == 1) { ?>\n";
						this.content += "\t\t\t\t\t<div class=\"alert alert-danger\" >Error. " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Alert11") + "\n";
						this.content += "\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
						this.content += "\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
						this.content += "\t\t\t\t\t\t</button>\n";
						this.content += "\t\t\t\t\t</div>\n";
						this.content += "\t\t\t\t\t<?php } ?>\n";
						this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") ?>&id" + entity.getName() + "=<?php echo $id" + entity.getName() + " ?>&attribute=<?php echo $attribute ?>\" class=\"bootstrap-form needs-validation\" enctype=\"multipart/form-data\"   >\n";
						this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
						this.content += "\t\t\t\t\t\t\t<label>" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "*</label>\n";						
						this.content += "\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" name=\"image\" required />\n";
						this.content += "\t\t\t\t\t\t</div>\n";					
						this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"update\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + "</button>\n";
						this.content += "\t\t\t\t\t</form>\n";
						this.content += "\t\t\t\t</div>\n";
						this.content += "\t\t\t</div>\n";
						this.content += "\t\t</div>\n";
						this.content += "\t</div>\n";
						this.content += "</div>\n";
						consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php");											
					}
				}
			}
		}
		return consoleStream;
	}

	private String createRecoverPassword(String generationDirectory) throws IOException {
		this.content = "";
		this.content += "<?php\n";
		this.content += "if(isset($_POST['recover'])){\n";
		this.content += "\t$found" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + " = false;\n";
		this.content += "\t$generatedPassword = \"\";\n";
		for(Entity entity : this.model.getEntities()) {
			if(entity.isActor()) {
				this.content += "\tif(!$found" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "){\n";
				this.content += "\t\t$recover" + entity.getName() + " = new " + entity.getName() + "();\n";
				this.content += "\t\tif($recover" + entity.getName() + " -> exist" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "($_POST['" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "'])) {;\n";
				this.content += "\t\t\t$generatedPassword = rand(100000,999999);\n";
				this.content += "\t\t\t$recover" + entity.getName() + " -> recoverPassword($_POST['" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "'], $generatedPassword);\n";
				this.content += "\t\t$found" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + " = true;\n";
				this.content += "\t\t}\n";
				this.content += "\t}\n";
			}
		}
		this.content += "\tif($found" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "){\n";
		this.content += "\t\t$to=$_POST['" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "'];\n";
		this.content += "\t\t$subject=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag5") + " " + this.model.getAcronym() + "\";\n";
		this.content += "\t\t$from=\"FROM: " + this.model.getAcronym() + " <contact@itiud.org>\";\n";
		this.content += "\t\t$message=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag6") + ": \".$generatedPassword;\n";
		this.content += "\t\tmail($to, $subject, $message, $from);\n";
		this.content += "\t}\n";
		this.content += "}\n";
		this.content += "?>\n";	
		this.content += "<div align=\"center\">\n";
		this.content += "\t<?php include(\"" + PHPGenerator.UI + "/header.php\"); ?>\n";
		this.content += "</div>\n";
		this.content += "<div class=\"container\">\n";
		this.content += "\t<div class=\"row\">\n";
		this.content += "\t\t<div class=\"col-md-2\"></div>\n";
		this.content += "\t\t<div class=\"col-md-8\">\n";
		this.content += "\t\t\t<div class=\"card\">\n";
		this.content += "\t\t\t\t<div class=\"card-header\">\n";
		this.content += "\t\t\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_RecoverPassword") + "</h4>\n";
		this.content += "\t\t\t\t</div>\n";
		this.content += "\t\t\t\t<div class=\"card-body\">\n";
		this.content += "\t\t\t\t\t<?php if(isset($_POST['recover'])) { ?>\n";
		this.content += "\t\t\t\t\t<div class=\"alert alert-success\" >" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag3") + ": <em><?php echo $_POST['" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "'] ?></em> " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag4") + "</div>\n";
		this.content += "\t\t\t\t\t<?php } else { ?>\n";
		this.content += "\t\t\t\t\t<form id=\"form\" method=\"post\" action=\"?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/recoverPassword.php\") ?>\" class=\"bootstrap-form needs-validation\"   >\n";
		this.content += "\t\t\t\t\t\t<div class=\"form-group\">\n";
		this.content += "\t\t\t\t\t\t\t<label>" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "*</label>\n";						
		this.content += "\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "\" required />\n";
		this.content += "\t\t\t\t\t\t</div>\n";					
		this.content += "\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-info\" name=\"recover\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_RecoverPassword") + "</button>\n";
		this.content += "\t\t\t\t\t</form>\n";
		this.content += "\t\t\t\t\t<?php } ?>\n";
		this.content += "\t\t\t\t</div>\n";
		this.content += "\t\t\t</div>\n";
		this.content += "\t\t</div>\n";
		this.content += "\t</div>\n";
		this.content += "</div>\n";
		return this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/recoverPassword.php");
	}

	private String createSelectAll(String generationDirectory) throws Exception {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			boolean useModal = false;
			if(!entity.isLog() && entity.isMenuDeploy()){
				this.content = "";
				String order = "";
				String dir = "";
				for(Attribute attribute : entity.getAttributes()){
					if(attribute.getOrder() != null) {
						order = attribute.getName();
						dir = attribute.getOrder();
					}
				}
				this.content += "<?php\n";
				this.content += "$order = \"" + order + "\";\n";
				this.content += "if(isset($_GET['order'])){\n";
				this.content += "\t$order = $_GET['order'];\n";
				this.content += "}\n";				
				this.content += "$dir = \"" + dir + "\";\n";
				this.content += "if(isset($_GET['dir'])){\n";
				this.content += "\t$dir = $_GET['dir'];\n";
				this.content += "}\n";				
				if(entity.isDelete()) {
					this.content += "$error = 0;\n";
					this.content += "if(isset($_GET['action']) && $_GET['action']==\"delete\"){\n";
					this.content += "\t$delete" + entity.getName() + " = new " + entity.getName() + "($_GET['id" + entity.getName() + "']);\n";
					this.content += "\t$delete" + entity.getName() + " -> select();\n";
					this.content += "\tif($delete" + entity.getName() + " -> delete()){\n";				
					for(Relation relation : entity.getRelations()){
						if(relation.getCardinality().equals("1")){
							this.content += "\t\t$name" + relation.getEntity() + " = ";					
							Entity entityRelation = this.model.findEntity(relation.getEntity());
							for(Attribute attribute : entityRelation.getAttributes()) {
								if(attribute.isDeploy()) {
									this.content += "$delete" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "() . \" \" . ";
								}
							}
							this.content = this.content.substring(0, this.content.length()-9);
							this.content += ";\n";
						}
					}
					this.content += "\t\t$user_ip = getenv('REMOTE_ADDR');\n";
					this.content += "\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
					this.content += "\t\t$browser = \"-\";\n";
					this.content += "\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
					this.content += "\t\t\t$browser = \"Internet Explorer\";\n";
					this.content += "\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
					this.content += "\t\t\t$browser = \"Chrome\";\n";
					this.content += "\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
					this.content += "\t\t\t$browser = \"Edge\";\n";
					this.content += "\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
					this.content += "\t\t\t$browser = \"Firefox\";\n";
					this.content += "\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
					this.content += "\t\t\t$browser = \"Opera\";\n";
					this.content += "\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
					this.content += "\t\t\t$browser = \"Safari\";\n";
					this.content += "\t\t}\n";
					for(Entity entityForLog : model.getEntities()){
						if(entityForLog.isActor()){
							if(entityForLog.isAdmin()) {
								this.content += "\t\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
							}else {
								this.content += "\t\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
							}					
							this.content += "\t\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"Delete " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
							for(Attribute attribute : entity.getAttributes()){
								if(!attribute.isPrimaryKey() && !attribute.isImage()){
									this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $delete" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \";; ";
								}
							}
							for(Relation relation:entity.getRelations()){
								if(relation.getCardinality().equals("1")){
									this.content += Utilities.replaceCapitalLetter(relation.getEntity()) + ": \" . $name" + relation.getEntity() + " . \";; ";						
								}
							}
							this.content = this.content.substring(0, this.content.length()-7);
							this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
							this.content += "\t\t\t$log" + entityForLog.getName() + " -> insert();\n";
							this.content += "\t\t}\n";
						}
					}
					this.content += "\t}else{\n";
					this.content += "\t\t$error = 1;\n";
					this.content += "\t}\n";
					this.content += "}\n";
				}
				this.content += "?>\n";
				this.content += "<div class=\"container-fluid\">\n";
				this.content += "\t<div class=\"card\">\n";
				this.content += "\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_GetAll") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t\t<div class=\"card-body\">\n";
				if(entity.isDelete()) {
					this.content += "\t\t<?php if(isset($_GET['action']) && $_GET['action']==\"delete\"){ ?>\n";
					this.content += "\t\t\t<?php if($error == 0){ ?>\n";
					this.content += "\t\t\t\t<div class=\"alert alert-success\" >The registry was succesfully deleted.\n";
					this.content += "\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
					this.content += "\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
					this.content += "\t\t\t\t\t</button>\n";
					this.content += "\t\t\t\t</div>\n";
					this.content += "\t\t\t\t<?php } else { ?>\n";
					this.content += "\t\t\t\t<div class=\"alert alert-danger\" >The registry was not deleted. Check it does not have related information\n";
					this.content += "\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
					this.content += "\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
					this.content += "\t\t\t\t\t</button>\n";
					this.content += "\t\t\t\t</div>\n";
					this.content += "\t\t\t\t<?php }\n";
					this.content += "\t\t\t} ?>\n";
				}
				this.content += "\t\t<div class=\"table-responsive\">\n";
				this.content += "\t\t\t<table class=\"table table-striped table-hover\">\n";
				this.content += "\t\t\t\t<thead>\n";
				this.content += "\t\t\t\t\t<tr><th></th>\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
						if(attribute.isUrl()) {
							this.content += "\t\t\t\t\t\t<th>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th> \n";							
						}else {
							this.content += "\t\t\t\t\t\t<th nowrap>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + " \n";
							this.content += "\t\t\t\t\t\t<?php if($order==\""+attribute.getName() + "\" && $dir==\"asc\") { ?>\n";
							this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-up'></span>\n";												
							this.content += "\t\t\t\t\t\t<?php } else { ?>\n";						
							this.content += "\t\t\t\t\t\t\t<a href='index.php?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php\") ?>&order=" + attribute.getName() + "&dir=asc'>\n";
							this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-amount-up' data-toggle='tooltip' data-placement='right' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag7") + "' ></span></a>\n";						
							this.content += "\t\t\t\t\t\t<?php } ?>\n";
							this.content += "\t\t\t\t\t\t<?php if($order==\""+attribute.getName() + "\" && $dir==\"desc\") { ?>\n";
							this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-down'></span>\n";												
							this.content += "\t\t\t\t\t\t<?php } else { ?>\n";						
							this.content += "\t\t\t\t\t\t\t<a href='?pid=<?php echo base64_encode(\""+PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php\") ?>&order=" + attribute.getName() + "&dir=desc'>\n";
							this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-amount-down' data-toggle='tooltip' data-placement='right' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag8") + "' ></span></a>\n";						
							this.content += "\t\t\t\t\t\t<?php } ?>\n";
							if(attribute.getInfo() != null) {
								this.content += " <span class='fas fa-info-circle' data-toggle='tooltip' data-placement='right' data-original-title='" + attribute.getInfo() + "' ></span>";
							}
							this.content += "\t\t\t\t\t\t</th>\n";							
						}
					}else if(!attribute.isPrimaryKey() && !attribute.isVisible()){
						useModal = true;
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t\t\t\t<th>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "</th>\n";		
					}
				}			
				this.content += "\t\t\t\t\t\t<th nowrap></th>\n";
				this.content += "\t\t\t\t\t</tr>\n";
				this.content += "\t\t\t\t</thead>\n";
				this.content += "\t\t\t\t</tbody>\n";
				this.content += "\t\t\t\t\t<?php\n";
				this.content += "\t\t\t\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "();\n";
				this.content += "\t\t\t\t\tif($order != \"\" && $dir != \"\") {\n";
				this.content += "\t\t\t\t\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> selectAllOrder($order, $dir);\n";
				this.content += "\t\t\t\t\t} else {\n";
				this.content += "\t\t\t\t\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> selectAll();\n";
				this.content += "\t\t\t\t\t}\n";
				this.content += "\t\t\t\t\t$counter = 1;\n";				
				this.content += "\t\t\t\t\tforeach ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s as $current" + entity.getName() + ") {\n";
				this.content += "\t\t\t\t\t\techo \"<tr><td>\" . $counter . \"</td>\";\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
						if(attribute.isUrl()) {
							this.content += "\t\t\t\t\t\tif($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() != \"\") {\n";							
							this.content += "\t\t\t\t\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + "><a href='\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"' target='_blank'><span class='fas fa-external-link-alt' data-toggle='tooltip' data-placement='left' data-original-title='\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"' ></span></a></td>\";\n";
							this.content += "\t\t\t\t\t\t} else {\n";
							this.content += "\t\t\t\t\t\t\techo \"<td></td>\";\n";
							this.content += "\t\t\t\t\t\t}\n";
						} else if (attribute.getType().equals(Attribute.STATE)) {
							this.content += "\t\t\t\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "\") . \"</td>\";\n";						
						} else if (attribute.getType().equals(Attribute.BOOLEAN)) {
							this.content += "\t\t\t\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "\") . \"</td>\";\n";						
						} else {
							this.content += "\t\t\t\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + ">\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"</td>\";\n";
						}

					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t\t\t\techo \"<td><a href='modal" + relation.getEntity() + ".php?id" + relation.getEntity() + "=\" . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> getId" + relation.getEntity() + "() . \"' data-toggle='modal' data-target='#modal" + entity.getName() + "' >\"";
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						int numAttributes=0;
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								if(numAttributes==0) {
									this.content += " . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase()+attribute.getName().substring(1, attribute.getName().length()) + "()";
									numAttributes++;
								}else {
									this.content += " . \" \" . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "()";
								}

							}
						}
						this.content += " . \"</a></td>\";\n";
					}
				}			
				this.content += "\t\t\t\t\t\techo \"<td class='text-right' nowrap>\";\n";
				if(useModal){
					this.content += "\t\t\t\t\t\techo \"<a href='modal" + entity.getName() + ".php?id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'  data-toggle='modal' data-target='#modal" + entity.getName() + "' >";
					this.content += "<span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag2") + "' ></span>";
					this.content += "</a> \";\n";					
				}
				String conditionals[] = {"", "", ""};
				for(Entity entity4Services : this.model.getEntities()) {
					if(entity4Services.isActor()) {
						if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.CREATE)) {
							conditionals[0] += "$_SESSION['entity'] == '" + entity4Services.getName() + "' || ";
						}
						if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.EDIT)) {
							conditionals[1] += "$_SESSION['entity'] == '" + entity4Services.getName() + "' || ";
						}
						if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.DELETE)) {
							conditionals[2] += "$_SESSION['entity'] == '" + entity4Services.getName() + "' || ";
						}
					}
				}
				for(int i=0; i<conditionals.length; i++) {
					conditionals[i] = conditionals[i].substring(0, conditionals[i].length()-4);
				}				
				this.content += "\t\t\t\t\t\tif(" + conditionals[1] + ") {\n";
				this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
				this.content += "<span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "' ></span>";
				this.content += "</a> \";\n";
				for(Attribute attribute : entity.getAttributes()) {
					if(attribute.isImage()) {
						this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&attribute=" + attribute.getName() + "'>";
						this.content += "<span class='fas fa-camera' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(attribute.getName()) + "'></span>";
						this.content += "</a> \";\n";						
					}else if(attribute.isFile()) {
						this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&attribute=" + attribute.getName() + "'>";
						this.content += "<span class='fas fa-file-alt' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(attribute.getName()) + "'></span>";
						this.content += "</a> \";\n";						
					}
				}
				this.content += "\t\t\t\t\t\t}\n";
				if(entity.isDelete()) {
					String deployAttributes="";
					for(Attribute attribute : entity.getAttributes()) {
						if(attribute.isDeploy()) {
							deployAttributes += "$current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)+"() . \" \" . ";
						}					
					}
					try {
						deployAttributes = deployAttributes.substring(0, deployAttributes.length()-9);
					} catch (Exception e) {
						throw new Exception("Attribute deploy is required in at least one attibute of the entity: " + entity.getName());
					}
					this.content += "\t\t\t\t\t\tif(" + conditionals[2] + ") {\n";					
					this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&action=delete' onclick='return confirm(\\\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag9") + " " + Utilities.replaceCapitalLetter(entity.getName()) + ": \" . " + deployAttributes +  " . \"\\\")'>";
					this.content += "<span class='fas fa-backspace' data-toggle='tooltip' data-placement='left' data-original-title='Delete " + Utilities.replaceCapitalLetter(entity.getName()) + "' ></span>";
					this.content += "</a> \";\n";											
					this.content += "\t\t\t\t\t\t}\n";
				}				
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("*")){
						if(!relation.getEntity().startsWith("Log")){							
							this.content+= "\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/selectAll" + relation.getEntity() + "By" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
							this.content+= "<span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_GetAll") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "' ></span>";
							this.content+= "</a> \";\n";	
							this.content += "\t\t\t\t\t\tif(" + conditionals[0] + ") {\n";
							this.content+= "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/insert" + relation.getEntity() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
							this.content+= "<span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "' ></span>";
							this.content+= "</a> \";\n";												
							this.content += "\t\t\t\t\t\t}\n";
						}
					}
				}
				this.content += "\t\t\t\t\t\techo \"</td>\";\n";
				this.content += "\t\t\t\t\t\techo \"</tr>\";\n";
				this.content += "\t\t\t\t\t\t$counter++;\n";
				this.content += "\t\t\t\t\t}\n";
				this.content += "\t\t\t\t\t?>\n";
				this.content += "\t\t\t\t</tbody>\n";
				this.content += "\t\t\t</table>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				this.content += "<div class=\"modal fade\" id=\"modal" + entity.getName() + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n";
				this.content += "\t<div class=\"modal-dialog modal-lg\" >\n";
				this.content += "\t\t<div class=\"modal-content\" id=\"modalContent\">\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";									
				this.content += "<script>\n";
				this.content += "\t$('body').on('show.bs.modal', '.modal', function (e) {\n";
				this.content += "\t\tvar link = $(e.relatedTarget);\n";
				this.content += "\t\t$(this).find(\".modal-content\").load(link.attr(\"href\"));\n";
				this.content += "\t});\n";
				this.content += "</script>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php");
				consoleStream += this.createModal(generationDirectory, entity);
			}
		}		
		return consoleStream;
	}

	private String createModal(String generationDirectory, Entity entity) throws IOException {
		this.content="";
		this.content += "<?php\n";
		for(Entity entityModal : this.model.getEntities()){			
			this.content += "require(\"" + PHPGenerator.BUSINESS + "/" + entityModal.getName() + ".php\");\n";
		}
		this.content += "require_once(\"" + PHPGenerator.PERSISTANCE + "/Connection.php\");\n";
		this.content += "$id" + entity.getName() + " = $_GET ['id" + entity.getName() + "'];\n";
		this.content += "$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "($id" + entity.getName() + ");\n";
		this.content += "$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> select();\n";
		this.content += "?>\n";
		this.content += "<script charset=\"utf-8\">\n";
		this.content += "\t$(function () { \n";
		this.content += "\t\t$(\"[data-toggle='tooltip']\").tooltip(); \n";
		this.content += "\t}); \n";
		this.content += "</script>\n";
		this.content += "<div class=\"modal-header\">\n";
		this.content += "\t<h4 class=\"modal-title\">" + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
		this.content += "\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n";
		this.content += "</div>\n";
		this.content += "<div class=\"modal-body\">\n";
		this.content += "\t<table class=\"table table-striped table-hover\">\n";
		for(Attribute attribute : entity.getAttributes()){
			if(!attribute.getType().equals(Attribute.PASSWORD) && !attribute.isPrimaryKey()){
				this.content += "\t\t<tr>\n";
				this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th>\n";
				if(attribute.isUrl() || attribute.isFile()) {
					this.content += "\t\t\t<?php if($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() != \"\"){ ?>\n";
					this.content += "\t\t\t\t<td><a href=\"<?php echo $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" target=\"_blank\"><span class='fas fa-external-link-alt' data-toggle='tooltip' data-placement='left' data-original-title='<?php echo $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() ?>' ></span></a></td>\n";
					this.content += "\t\t\t<?php }else{ ?>\n";
					this.content += "\t\t\t\t<td></td>\n";
					this.content += "\t\t\t<?php } ?>\n";
				} else if(attribute.isImage()) {
					this.content += "\t\t\t\t<td><img class=\"rounded\" src=\"<?php echo $" + entity.getName().toLowerCase() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() ?>\" height=\"300px\" /></td>\n";
				} else if (attribute.getType().equals(Attribute.STATE)) {
					this.content += "\t\t\t<td><?php echo ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "\") ?> </td>\n";							
				} else if (attribute.getType().equals(Attribute.BOOLEAN)) {
					this.content += "\t\t\t<td><?php echo ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "\") ?> </td>\n";							
				} else {
					this.content += "\t\t\t<td><?php echo $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase()+attribute.getName().substring(1) + "() ?></td>\n";	
				}
				this.content += "\t\t</tr>\n";
			}
		}
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")) {
				this.content += "\t\t<tr>\n";
				this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(relation.getEntity().substring(0,1).toUpperCase() + 
						relation.getEntity().substring(1)) + "</th>\n";
				this.content += "\t\t\t<td><?php echo ";
				String deployAttributes="";
				Entity entityRelation = this.model.findEntity(relation.getEntity());
				for(Attribute attribute : entityRelation.getAttributes()) {
					if(attribute.isDeploy()) {
						deployAttributes += "$" + entity.getName().substring(0, 1).toLowerCase() + 
								entity.getName().substring(1) + " -> get" + 
								relation.getEntity() + "() -> get" + 
								attribute.getName().substring(0, 1).toUpperCase() + 
								attribute.getName().substring(1, attribute.getName().length()) + "() . \" \" . ";
					}
				}
				deployAttributes = deployAttributes.substring(0, deployAttributes.length()-9);
				this.content += deployAttributes + " ?></td>\n";
				this.content += "\t\t</tr>\n";
			}
		}
		this.content += "\t</table>\n";
		this.content += "</div>\n";
		return this.fileUtility.writePHP(content, generationDirectory + "/modal" + entity.getName() + ".php");		
	}

	private String createSelectAllByRelation(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			boolean useModal = false;
			if(!entity.isLog()){
				for(Relation relationByEntity : entity.getRelations()) {
					if(relationByEntity.getCardinality().equals("1")) {
						this.content = "";
						this.content += "<?php\n";
						String order = "";
						String dir = "";
						for(Attribute attribute : entity.getAttributes()){
							if(attribute.getOrder() != null) {
								order = attribute.getName();
								dir = attribute.getOrder();
							}
						}
						this.content += "$order = \"" + order + "\";\n";
						this.content += "if(isset($_GET['order'])){\n";
						this.content += "\t$order = $_GET['order'];\n";
						this.content += "}\n";				
						this.content += "$dir = \"" + dir + "\";\n";
						this.content += "if(isset($_GET['dir'])){\n";
						this.content += "\t$dir = $_GET['dir'];\n";
						this.content += "}\n";				
						this.content += "$" + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + " = new " + relationByEntity.getEntity() + "($_GET['id" + relationByEntity.getEntity() + "']); \n";
						this.content += "$" + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + " -> select();\n";
						if(entity.isDelete()) {
							this.content += "$error = 0;\n";
							this.content += "if(!empty($_GET['action']) && $_GET['action']==\"delete\"){\n";
							this.content += "\t$delete" + entity.getName() + " = new " + entity.getName() + "($_GET['id" + entity.getName() + "']);\n";
							this.content += "\t$delete" + entity.getName() + " -> select();\n";
							this.content += "\tif($delete" + entity.getName() + " -> delete()){\n";				
							for(Relation relation : entity.getRelations()){
								if(relation.getCardinality().equals("1")){
									this.content += "\t\t$name" + relation.getEntity() + " = ";					
									Entity entityRelation = this.model.findEntity(relation.getEntity());
									for(Attribute attribute : entityRelation.getAttributes()) {
										if(attribute.isDeploy()) {
											this.content += "$delete" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "() . \" \" . ";
										}
									}
									this.content = this.content.substring(0, this.content.length()-9);
									this.content += ";\n";
								}
							}
							this.content += "\t\t$user_ip = getenv('REMOTE_ADDR');\n";
							this.content += "\t\t$agent = $_SERVER[\"HTTP_USER_AGENT\"];\n";
							this.content += "\t\t$browser = \"-\";\n";
							this.content += "\t\tif( preg_match('/MSIE (\\d+\\.\\d+);/', $agent) ) {\n";
							this.content += "\t\t\t$browser = \"Internet Explorer\";\n";
							this.content += "\t\t} else if (preg_match('/Chrome[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
							this.content += "\t\t\t$browser = \"Chrome\";\n";
							this.content += "\t\t} else if (preg_match('/Edge\\/\\d+/', $agent) ) {\n";
							this.content += "\t\t\t$browser = \"Edge\";\n";
							this.content += "\t\t} else if ( preg_match('/Firefox[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
							this.content += "\t\t\t$browser = \"Firefox\";\n";
							this.content += "\t\t} else if ( preg_match('/OPR[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
							this.content += "\t\t\t$browser = \"Opera\";\n";
							this.content += "\t\t} else if (preg_match('/Safari[\\/\\s](\\d+\\.\\d+)/', $agent) ) {\n";
							this.content += "\t\t\t$browser = \"Safari\";\n";
							this.content += "\t\t}\n";
							for(Entity entityForLog : model.getEntities()){
								if(entityForLog.isActor()){
									if(entityForLog.isAdmin()) {
										this.content += "\t\tif($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";
									}else {
										this.content += "\t\telse if($_SESSION['entity'] == '" + entityForLog.getName() + "'){\n";	
									}					
									this.content += "\t\t\t$log" + entityForLog.getName() + " = new Log" + entityForLog.getName() + "(\"\",\"Delete " + Utilities.replaceCapitalLetter(entity.getName()) + "\", \"";
									for(Attribute attribute : entity.getAttributes()){
										if(!attribute.isPrimaryKey() && !attribute.isImage()){
											this.content += Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + ": \" . $delete" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \";; ";
										}
									}
									for(Relation relation:entity.getRelations()){
										if(relation.getCardinality().equals("1")){
											this.content += Utilities.replaceCapitalLetter(relation.getEntity()) + ": \" . $name" + relation.getEntity() + " . \";; ";						
										}
									}
									this.content = this.content.substring(0, this.content.length()-7);
									this.content += ", date(\"Y-m-d\"), date(\"H:i:s\"), $user_ip, PHP_OS, $browser, $_SESSION['id']);\n";
									this.content += "\t\t\t$log" + entityForLog.getName() + " -> insert();\n";
									this.content += "\t\t}\n";
								}
							}
							this.content += "\t}else{\n";
							this.content += "\t\t$error = 1;\n";
							this.content += "\t}\n";
							this.content += "}\n";
						}
						this.content += "?>\n";						
						this.content += "<div class=\"container-fluid\">\n";
						this.content += "\t<div class=\"card\">\n";
						this.content += "\t\t<div class=\"card-header\">\n";
						Entity entityForName = model.findEntity(relationByEntity.getEntity());
						String stringToDeploy="";
						for(Attribute attribute : entityForName.getAttributes()) {
							if(attribute.isDeploy()) {
								stringToDeploy += "$" + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + " -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "() . \" \" . ";
							}
						}
						stringToDeploy = stringToDeploy.substring(0, stringToDeploy.length()-9);
						this.content += "\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_GetAll") + " " + Utilities.replaceCapitalLetter(entity.getName()) + " " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_of") + " " + Utilities.replaceCapitalLetter(relationByEntity.getEntity()) + ": <em><?php echo " + stringToDeploy + " ?></em></h4>\n";
						this.content += "\t\t</div>\n";
						this.content += "\t\t<div class=\"card-body\">\n";
						if(entity.isDelete()) {
							this.content += "\t\t<?php if(isset($_GET['action']) && $_GET['action']==\"delete\"){ ?>\n";
							this.content += "\t\t\t<?php if($error == 0){ ?>\n";
							this.content += "\t\t\t\t<div class=\"alert alert-success\" >The registry was succesfully deleted.\n";
							this.content += "\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
							this.content += "\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
							this.content += "\t\t\t\t\t</button>\n";
							this.content += "\t\t\t\t</div>\n";
							this.content += "\t\t\t\t<?php } else { ?>\n";
							this.content += "\t\t\t\t<div class=\"alert alert-danger\" >The registry was not deleted. Check it does not have related information\n";
							this.content += "\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n";
							this.content += "\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>\n";
							this.content += "\t\t\t\t\t</button>\n";
							this.content += "\t\t\t\t</div>\n";
							this.content += "\t\t\t\t<?php }\n";
							this.content += "\t\t\t} ?>\n";
						}
						this.content += "\t\t\t<div class=\"table-responsive\">\n";
						this.content += "\t\t\t<table class=\"table table-striped table-hover\">\n";
						this.content += "\t\t\t\t<thead>\n";
						this.content += "\t\t\t\t\t<tr><th></th>\n";
						for(Attribute attribute : entity.getAttributes()){
							if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
								if(attribute.isUrl()) {
									this.content += "\t\t\t\t\t\t<th>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th> \n";							
								}else {
									this.content += "\t\t\t\t\t\t<th nowrap>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + " \n";
									this.content += "\t\t\t\t\t\t<?php if($order==\"" + attribute.getName() + "\" && $dir==\"asc\") { ?>\n";
									this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-up'></span>\n";												
									this.content += "\t\t\t\t\t\t<?php } else { ?>\n";						
									this.content += "\t\t\t\t\t\t\t<a data-toggle='tooltip' data-placement='right' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag7") + "' href='?pid=<?php echo base64_encode(\"" + PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + "By" + relationByEntity.getEntity() + ".php\") ?>&id" + relationByEntity.getEntity() + "=<?php echo $_GET['id" + relationByEntity.getEntity() + "'] ?>&order=" + attribute.getName() + "&dir=asc'>\n";
									this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-amount-up'></span></a>\n";						
									this.content += "\t\t\t\t\t\t<?php } ?>\n";
									this.content += "\t\t\t\t\t\t<?php if($order==\"" + attribute.getName() + "\" && $dir==\"desc\") { ?>\n";
									this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-down'></span>\n";												
									this.content += "\t\t\t\t\t\t<?php } else { ?>\n";						
									this.content += "\t\t\t\t\t\t\t<a data-toggle='tooltip' data-placement='right' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag8") + "' href='?pid=<?php echo base64_encode(\""+PHPGenerator.UI+"/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + "By" + relationByEntity.getEntity() + ".php\") ?>&id" + relationByEntity.getEntity() + "=<?php echo $_GET['id" + relationByEntity.getEntity() + "'] ?>&order=" + attribute.getName() + "&dir=desc'>\n";
									this.content += "\t\t\t\t\t\t\t<span class='fas fa-sort-amount-down'></span></a>\n";						
									this.content += "\t\t\t\t\t\t<?php } ?>\n";
									if(attribute.getInfo() != null) {
										this.content += " <span class='fas fa-info-circle' data-toggle='tooltip' data-placement='right' data-original-title='" + attribute.getInfo() + "' ></span>";
									}
									this.content += "\t\t\t\t\t\t</th>\n";							
								}
							}else if(!attribute.isPrimaryKey() && !attribute.isVisible()){							
								useModal = true;
							}
						}
						for(Relation relation : entity.getRelations()){
							if(relation.getCardinality().equals("1")){
								this.content += "\t\t\t\t\t\t<th>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "</th>\n";		
							}
						}
						this.content += "\t\t\t\t\t\t<th nowrap></th>\n";
						this.content += "\t\t\t\t\t</tr>\n";
						this.content += "\t\t\t\t</thead>\n";
						this.content += "\t\t\t\t</tbody>\n";
						this.content += "\t\t\t\t\t<?php\n";
						String parameters = "";
						for(int i=0; i<entity.getAttributes().size();i++) {
							parameters += "\"\", ";
						}
						for(Relation relation : entity.getRelations()) {
							if(relation.getCardinality().equals("1")) {
								if(relationByEntity == relation) {
									parameters += "$_GET['id" + relationByEntity.getEntity() + "'], "; 
								}else {
									parameters += "\"\", ";
								}								
							}
						}
						parameters = parameters.substring(0, parameters.length()-2);
						this.content += "\t\t\t\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "(" + parameters + ");\n";
						this.content += "\t\t\t\t\tif($order!=\"\" && $dir!=\"\") {\n";
						this.content += "\t\t\t\t\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> selectAllBy" + relationByEntity.getEntity() + "Order($order, $dir);\n";
						this.content += "\t\t\t\t\t} else {\n";
						this.content += "\t\t\t\t\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> selectAllBy" + relationByEntity.getEntity() + "();\n";
						this.content += "\t\t\t\t\t}\n";
						this.content += "\t\t\t\t\t$counter = 1;\n";				
						this.content += "\t\t\t\t\tforeach ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s as $current" + entity.getName() + ") {\n";
						this.content += "\t\t\t\t\t\techo \"<tr><td>\" . $counter . \"</td>\";\n";
						for(Attribute attribute : entity.getAttributes()){
							if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
								if(attribute.isUrl()) {
									this.content += "\t\t\t\t\t\tif($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() != \"\") {\n";							
									this.content += "\t\t\t\t\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + "><a href='\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"' target='_blank'><span class='fas fa-external-link-alt' data-toggle='tooltip' data-placement='left' data-original-title='\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"' ></span></a></td>\";\n";
									this.content += "\t\t\t\t\t\t} else {\n";
									this.content += "\t\t\t\t\t\t\techo \"<td></td>\";\n";
									this.content += "\t\t\t\t\t\t}\n";
								} else if (attribute.getType().equals(Attribute.STATE)) {
									this.content += "\t\t\t\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "\") . \"</td>\";\n";							
								} else if (attribute.getType().equals(Attribute.BOOLEAN)) {
									this.content += "\t\t\t\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "\") . \"</td>\";\n";						
								}else {
									this.content += "\t\t\t\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + ">\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"</td>\";\n";
								}
							}
						}
						for(Relation relation : entity.getRelations()){
							if(relation.getCardinality().equals("1")){
								this.content += "\t\t\t\t\t\techo \"<td><a href='modal" + relation.getEntity() + ".php?id" + relation.getEntity() + "=\" . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> getId" + relation.getEntity() + "() . \"' data-toggle='modal' data-target='#modal" + entity.getName() + "' >\"";
								Entity entityRelation = this.model.findEntity(relation.getEntity());
								int numAttributes=0;
								for(Attribute attribute : entityRelation.getAttributes()) {
									if(attribute.isDeploy()) {
										if(numAttributes==0) {
											this.content += " . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase()+attribute.getName().substring(1, attribute.getName().length()) + "()";
											numAttributes++;
										}else {
											this.content += " . \" \" . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "()";
										}

									}
								}
								this.content += " . \"</a></td>\";\n";
							}
						}			
						this.content += "\t\t\t\t\t\techo \"<td class='text-right' nowrap>\";\n";
						if(useModal){
							this.content += "\t\t\t\t\t\techo \"<a href='modal" + entity.getName() + ".php?id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'  data-toggle='modal' data-target='#modal" + entity.getName() + "' >";
							this.content += "<span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag2") + "' ></span>";
							this.content += "</a> \";\n";					
						}
						String conditionals[] = {"", "", ""};
						for(Entity entity4Services : this.model.getEntities()) {
							if(entity4Services.isActor()) {
								if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.CREATE)) {
									conditionals[0] += "$_SESSION['entity'] == '" + entity4Services.getName() + "' || ";
								}
								if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.EDIT)) {
									conditionals[1] += "$_SESSION['entity'] == '" + entity4Services.getName() + "' || ";
								}
								if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.DELETE)) {
									conditionals[2] += "$_SESSION['entity'] == '" + entity4Services.getName() + "' || ";
								}
							}
						}
						for(int i=0; i<conditionals.length; i++) {
							conditionals[i] = conditionals[i].substring(0, conditionals[i].length()-4);
						}				
						this.content += "\t\t\t\t\t\tif(" + conditionals[1] + ") {\n";
						this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
						this.content += "<span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "' ></span>";
						this.content += "</a> \";\n";
						for(Attribute attribute : entity.getAttributes()) {
							if(attribute.isImage()) {
								this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&attribute=" + attribute.getName() + "'>";
								this.content += "<span class='fas fa-camera' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(attribute.getName()) + "'></span>";
								this.content += "</a> \";\n";
							}else if(attribute.isFile()) {
								this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&attribute=" + attribute.getName() + "'>";
								this.content += "<span class='fas fa-file-alt' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(attribute.getName()) + "'></span>";
								this.content += "</a> \";\n";						
							}
						}
						this.content += "\t\t\t\t\t\t}\n";
						if(entity.isDelete()) {
							String deployAttributes="";
							for(Attribute attribute : entity.getAttributes()) {
								if(attribute.isDeploy()) {
									deployAttributes += "$current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)+"() . \" \" . ";
								}
							}							
							this.content += "\t\t\t\t\t\tif(" + conditionals[2] + ") {\n";					
							if(!deployAttributes.equals("")) {
								deployAttributes = deployAttributes.substring(0, deployAttributes.length()-9);																																	
								this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + "By" + relationByEntity.getEntity() + ".php\") . \"&id" + relationByEntity.getEntity() + "=\" . $_GET['id" + relationByEntity.getEntity() + "'] . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&action=delete' onclick='return confirm(\\\"Confirm to delete " + Utilities.replaceCapitalLetter(entity.getName()) + ": \" . " + deployAttributes +  " . \"\\\")'> ";
							} else {
								this.content += "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + "By" + relationByEntity.getEntity() + ".php\") . \"&id" + relationByEntity.getEntity() + "=\" . $_GET['id" + relationByEntity.getEntity() + "'] . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&action=delete' onclick='return confirm(\\\"Confirm to delete " + Utilities.replaceCapitalLetter(entity.getName()) + "\\\")'> ";
							}
							this.content += "<span class='fas fa-backspace' data-toggle='tooltip' data-placement='left' data-original-title='Delete " + Utilities.replaceCapitalLetter(entity.getName()) + "' ></span>";
							this.content += "</a> \";\n";											
							this.content += "\t\t\t\t\t\t}\n";
						}				
						for(Relation relation : entity.getRelations()){
							if(relation.getCardinality().equals("*")){
								if(!relation.getEntity().startsWith("Log")){							
									this.content+= "\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/selectAll" + relation.getEntity() + "By" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
									this.content+= "<span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_GetAll") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "' ></span>";
									this.content+= "</a> \";\n";	
									this.content += "\t\t\t\t\t\tif(" + conditionals[0] + ") {\n";
									this.content+= "\t\t\t\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/insert" + relation.getEntity() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
									this.content+= "<span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "' ></span>";
									this.content+= "</a> \";\n";												
									this.content += "\t\t\t\t\t\t}\n";
								}
							}
						}						
						this.content += "\t\t\t\t\t\techo \"</td>\";\n";
						this.content += "\t\t\t\t\t\techo \"</tr>\";\n";
						this.content += "\t\t\t\t\t\t$counter++;\n";
						this.content += "\t\t\t\t\t};\n";
						this.content += "\t\t\t\t\t?>\n";
						this.content += "\t\t\t\t</tbody>\n";
						this.content += "\t\t\t</table>\n";
						this.content += "\t\t\t</div>\n";
						this.content += "\t\t</div>\n";
						this.content += "\t</div>\n";
						this.content += "</div>\n";
						this.content += "<div class=\"modal fade\" id=\"modal" + entity.getName() + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n";
						this.content += "\t<div class=\"modal-dialog modal-lg\" >\n";
						this.content += "\t\t<div class=\"modal-content\" id=\"modalContent\">\n";
						this.content += "\t\t</div>\n";
						this.content += "\t</div>\n";
						this.content += "</div>\n";									
						this.content += "<script>\n";
						this.content += "\t$('body').on('show.bs.modal', '.modal', function (e) {\n";
						this.content += "\t\tvar link = $(e.relatedTarget);\n";
						this.content += "\t\t$(this).find(\".modal-content\").load(link.attr(\"href\"));\n";
						this.content += "\t});\n";
						this.content += "</script>\n";
						consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + "By" + relationByEntity.getEntity() + ".php");
					}
				}
			}
		}		
		return consoleStream;
	}

	private String createSearch(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			boolean useModal = false;
			if(!entity.isLog() && entity.isMenuDeploy()){
				this.content = "";
				this.content += "<div class=\"container-fluid\">\n";
				this.content += "\t<div class=\"card\">\n";
				this.content += "\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Search") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t<div class=\"container\">\n";
				this.content += "\t\t\t\t<div class=\"row\">\n";
				this.content += "\t\t\t\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t\t\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"search\" placeholder=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Search") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "\" autocomplete=\"off\" />\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t\t<div id=\"searchResult\"></div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				this.content += "<script>\n";
				this.content += "$(document).ready(function(){\n";
				this.content += "\t$(\"#search\").keyup(function(){\n";
				this.content += "\t\tif($(\"#search\").val().length > 2){\n";
				this.content += "\t\t\tvar search = $(\"#search\").val().replaceAll(\" \", \"%20\");\n";
				this.content += "\t\t\tvar path = \"indexAjax.php?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + "Ajax.php\"); ?>&search=\"+search+\"&entity=<?php echo $_SESSION['entity'] ?>\";\n";
				this.content += "\t\t\t$(\"#searchResult\").load(path);\n";
				this.content += "\t\t}\n";
				this.content += "\t});\n";
				this.content += "});\n";
				this.content += "</script>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + ".php");
				this.content = "";
				this.content += "<script charset=\"utf-8\">\n";
				this.content += "\t$(function () { \n";
				this.content += "\t\t$(\"[data-toggle='tooltip']\").tooltip(); \n";
				this.content += "\t});\n";
				this.content += "</script>\n";
				this.content += "<div class=\"table-responsive\">\n";
				this.content += "<table class=\"table table-striped table-hover\">\n";
				this.content += "\t<thead>\n";
				this.content += "\t\t<tr><th></th>\n";
				for(Attribute attribute:entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
						if(attribute.isUrl()) {
							this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th> \n";													
						}else {
							this.content += "\t\t\t<th nowrap>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th>\n";							
						}
					}else if(!attribute.isPrimaryKey() && !attribute.isVisible()){
						useModal = true;
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "</th>\n";		
					}
				}			
				this.content += "\t\t\t<th nowrap></th>\n";
				this.content += "\t\t</tr>\n";
				this.content += "\t</thead>\n";
				this.content += "\t</tbody>\n";
				this.content += "\t\t<?php\n";
				this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "();\n";
				this.content += "\t\t$searchText = $_GET['search'];\n";
				this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> search($searchText);\n";
				this.content += "\t\t$counter = 1;\n";				
				this.content += "\t\tforeach ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s as $current" + entity.getName() + ") {\n";
				this.content += "\t\t\techo \"<tr><td>\" . $counter . \"</td>\";\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
						if(attribute.isUrl()) {
							this.content += "\t\t\tif($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() != \"\") {\n";							
							this.content += "\t\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + "><a href='\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"' target='_blank'><span class='fas fa-external-link-alt' data-toggle='tooltip' data-placement='left' data-original-title='\" . $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() . \"' ></span></a></td>\";\n";
							this.content += "\t\t\t} else {\n";
							this.content += "\t\t\t\techo \"<td></td>\";\n";
							this.content += "\t\t\t}\n";
						} else if (attribute.getType().equals(Attribute.STATE)) {
							this.content += "\t\t\t\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "\") . \"</td>\";\n";						
						} else if (attribute.getType().equals(Attribute.BOOLEAN)) {
							this.content += "\t\t\t\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "\") . \"</td>\";\n";						
						}else {
							this.content += "\t\t\t$pos = stripos($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), $searchText);\n";
							this.content += "\t\t\tif($pos !== false){\n";
							this.content += "\t\t\t\t$text = substr($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), 0, $pos) . \"<strong>\" . substr($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), $pos, strlen($searchText)) . \"</strong>\" . substr($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), $pos + strlen($searchText));\n";
							this.content += "\t\t\t} else {\n";
							this.content += "\t\t\t\t$text = $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "();\n";							
							this.content += "\t\t\t}\n";
							this.content += "\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + ">\" . $text . \"</td>\";\n";	
						}
					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\techo \"<td>\"";
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						int numAttributes=0;
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								if(numAttributes==0) {
									this.content += " . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase()+attribute.getName().substring(1, attribute.getName().length()) + "()";
									numAttributes++;
								}else {
									this.content += " . \" \" . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "()";
								}

							}
						}
						this.content += " . \"</td>\";\n";
					}
				}			
				this.content += "\t\t\techo \"<td class='text-right' nowrap>\";\n";
				if(useModal){
					this.content += "\t\t\techo \"<a href='modal" + entity.getName() + ".php?id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'  data-toggle='modal' data-target='#modal" + entity.getName() + "' >";
					this.content += "<span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag2") + "' ></span>";
					this.content += "</a> \";\n";					
				}
				String conditionals[] = {"", "", ""};
				for(Entity entity4Services : this.model.getEntities()) {
					if(entity4Services.isActor()) {
						if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.CREATE)) {
							conditionals[0] += "$_GET['entity'] == '" + entity4Services.getName() + "' || ";
						}
						if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.EDIT)) {
							conditionals[1] += "$_GET['entity'] == '" + entity4Services.getName() + "' || ";
						}
						if(entity4Services.isAdmin() || entity4Services.hasService(entity.getName(), Model.DELETE)) {
							conditionals[2] += "$_GET['entity'] == '" + entity4Services.getName() + "' || ";
						}
					}
				}
				for(int i=0; i<conditionals.length; i++) {
					conditionals[i] = conditionals[i].substring(0, conditionals[i].length()-4);
				}				
				this.content += "\t\t\tif(" + conditionals[1] + ") {\n";
				this.content += "\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
				this.content += "<span class='fas fa-edit' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "' ></span>";
				this.content += "</a> \";\n";
				for(Attribute attribute : entity.getAttributes()) {
					if(attribute.isImage()) {
						this.content += "\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&attribute=" + attribute.getName() + "'>";
						this.content += "<span class='fas fa-camera' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(attribute.getName()) + "'></span>";
						this.content += "</a> \";\n";						
					}else if(attribute.isFile()) {
						this.content += "\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/update" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&attribute=" + attribute.getName() + "'>";
						this.content += "<span class='fas fa-file-alt' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Edit") + " " + Utilities.replaceCapitalLetter(attribute.getName()) + "'></span>";
						this.content += "</a> \";\n";						
					}
				}
				this.content += "\t\t\t}\n";
				if(entity.isDelete()) {
					String deployAttributes="";
					for(Attribute attribute : entity.getAttributes()) {
						if(attribute.isDeploy()) {
							deployAttributes += "$current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)+"() . \" \" . ";
						}
					}
					deployAttributes = deployAttributes.substring(0, deployAttributes.length()-9);
					this.content += "\t\t\tif(" + conditionals[2] + ") {\n";					
					this.content += "\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/selectAll" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"&action=delete' onclick='return confirm(\\\"Confirm to delete " + Utilities.replaceCapitalLetter(entity.getName()) + ": \" . " + deployAttributes +  " . \"\\\")'>";
					this.content += "<span class='fas fa-backspace' data-toggle='tooltip' data-placement='left' data-original-title='Delete " + Utilities.replaceCapitalLetter(entity.getName()) + "' ></span>";
					this.content += "</a> \";\n";											
					this.content += "\t\t\t}\n";
				}				
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("*")){
						if(!relation.getEntity().startsWith("Log")){							
							this.content+= "\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/selectAll" + relation.getEntity() + "By" + entity.getName() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
							this.content+= "<span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_GetAll") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "' ></span>";
							this.content+= "</a> \";\n";	
							this.content += "\t\t\tif(" + conditionals[0] + ") {\n";
							this.content+= "\t\t\t\techo \"<a href='?pid=\" . base64_encode(\"" + PHPGenerator.UI + "/" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "/insert" + relation.getEntity() + ".php\") . \"&id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'>";
							this.content+= "<span class='fas fa-pen' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Create") + " " + Utilities.replaceCapitalLetter(relation.getEntity()) + "' ></span>";
							this.content+= "</a> \";\n";												
							this.content += "\t\t\t}\n";
						}
					}
				}
				this.content += "\t\t\techo \"</td>\";\n";
				this.content += "\t\t\techo \"</tr>\";\n";
				this.content += "\t\t\t$counter++;\n";
				this.content += "\t\t}\n";
				this.content += "\t\t?>\n";
				this.content += "\t</tbody>\n";
				this.content += "</table>\n";
				this.content += "</div>\n";
				if(useModal){
					this.content += "<div class=\"modal fade\" id=\"modal" + entity.getName() + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n";
					this.content += "\t<div class=\"modal-dialog modal-lg\" >\n";
					this.content += "\t\t<div class=\"modal-content\" id=\"modalContent\">\n";
					this.content += "\t\t</div>\n";
					this.content += "\t</div>\n";
					this.content += "</div>\n";									
					this.content += "<script>\n";
					this.content += "\t$('body').on('show.bs.modal', '.modal', function (e) {\n";
					this.content += "\t\tvar link = $(e.relatedTarget);\n";
					this.content += "\t\t$(this).find(\".modal-content\").load(link.attr(\"href\"));\n";
					this.content += "\t});\n";
					this.content += "</script>\n";

				}				
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + "Ajax.php");
			}
		}		
		return consoleStream;
	}

	private String createSearchLog(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){
			if(entity.isLog()){
				this.content = "";
				this.content += "<div class=\"container-fluid\">\n";
				this.content += "\t<div class=\"card\">\n";
				this.content += "\t\t<div class=\"card-header\">\n";
				this.content += "\t\t\t<h4 class=\"card-title\">" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Search") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t\t<div class=\"card-body\">\n";
				this.content += "\t\t\t<div class=\"container\">\n";
				this.content += "\t\t\t\t<div class=\"row\">\n";
				this.content += "\t\t\t\t\t<div class=\"col-md-2\"></div>\n";
				this.content += "\t\t\t\t\t<div class=\"col-md-8\">\n";
				this.content += "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"search\" placeholder=\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Search") + " " + Utilities.replaceCapitalLetter(entity.getName()) + "\" autocomplete=\"off\" />\n";
				this.content += "\t\t\t\t\t</div>\n";
				this.content += "\t\t\t\t</div>\n";
				this.content += "\t\t\t</div>\n";
				this.content += "\t\t\t<div id=\"searchResult\"></div>\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";
				this.content += "<script>\n";
				this.content += "$(document).ready(function(){\n";
				this.content += "\t$(\"#search\").keyup(function(){\n";
				this.content += "\t\tif($(\"#search\").val().length > 2){\n";
				this.content += "\t\t\tvar search = $(\"#search\").val().replace(\" \", \"%20\");\n";
				this.content += "\t\t\tvar path = \"indexAjax.php?pid=<?php echo base64_encode(\"" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + "Ajax.php\"); ?>&search=\"+search;\n";
				this.content += "\t\t\t$(\"#searchResult\").load(path);\n";
				this.content += "\t\t}\n";
				this.content += "\t});\n";
				this.content += "});\n";
				this.content += "</script>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + ".php");
				this.content = "";
				this.content += "<script charset=\"utf-8\">\n";
				this.content += "\t$(function () { \n";
				this.content += "\t\t$(\"[data-toggle='tooltip']\").tooltip(); \n";
				this.content += "\t});\n";
				this.content += "</script>\n";
				this.content += "<div class=\"table-responsive\">\n";
				this.content += "<table class=\"table table-striped table-hover\">\n";
				this.content += "\t<thead>\n";
				this.content += "\t\t<tr><th></th>\n";
				for(Attribute attribute:entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
						if(!attribute.isUrl()) {
							this.content += "\t\t\t<th nowrap>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th>\n";							
						}else {
							this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th> \n";							
						}
					}
				}
				for(Relation relation:entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(relation.getEntity()) + "</th>\n";		
					}
				}			
				this.content += "\t\t\t<th nowrap></th>\n";
				this.content += "\t\t</tr>\n";
				this.content += "\t</thead>\n";
				this.content += "\t</tbody>\n";
				this.content += "\t\t<?php\n";
				this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "();\n";
				this.content += "\t\t$searchText = $_GET['search'];\n";
				this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> search($searchText);\n";
				this.content += "\t\t$counter = 1;\n";				
				this.content += "\t\tforeach ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s as $current" + entity.getName() + ") {\n";
				this.content += "\t\t\techo \"<tr><td>\" . $counter . \"</td>\";\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.isPrimaryKey() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()){
						if (attribute.getType().equals(Attribute.STATE)) {
							this.content += "\t\t\t<td><?php echo ($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Enabled") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Disabled") + "\") ?> </td>\n";							
						} else if (attribute.getType().equals(Attribute.BOOLEAN)) {
							this.content += "\t\t\techo \"<td>\" . ($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()==1?\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_True") + "\":\"" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_False") + "\") . \"</td>\";\n";						
						} else {
							this.content += "\t\t\t$pos = stripos($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), $searchText);\n";
							this.content += "\t\t\tif($pos !== false){\n";
							this.content += "\t\t\t\t$text = substr($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), 0, $pos) . \"<strong>\" . substr($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), $pos, strlen($searchText)) . \"</strong>\" . substr($current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "(), $pos + strlen($searchText));\n";
							this.content += "\t\t\t} else {\n";
							this.content += "\t\t\t\t$text = $current" + entity.getName() + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "();\n";							
							this.content += "\t\t\t}\n";
							this.content += "\t\t\techo \"<td" + ((attribute.isNowrap())?" nowrap":"") + ">\" . $text . \"</td>\";\n";	
						}
					}
				}
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += "\t\t\techo \"<td>\"";
						Entity entityRelation = this.model.findEntity(relation.getEntity());
						int numAttributes=0;
						for(Attribute attribute : entityRelation.getAttributes()) {
							if(attribute.isDeploy()) {
								if(numAttributes==0) {
									this.content += " . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase()+attribute.getName().substring(1, attribute.getName().length()) + "()";
									numAttributes++;
								}else {
									this.content += " . \" \" . $current" + entity.getName() + " -> get" + relation.getEntity() + "() -> get" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1, attribute.getName().length()) + "()";
								}
							}
						}
						this.content += " . \"</td>\";\n";
					}
				}			
				this.content += "\t\t\techo \"<td class='text-right' nowrap>\n";
				this.content += "\t\t\t\t<a href='modal" + entity.getName() + ".php?id" + entity.getName() + "=\" . $current" + entity.getName() + " -> getId" + entity.getName() + "() . \"'  data-toggle='modal' data-target='#modal" + entity.getName() + "' >\n";
				this.content += "\t\t\t\t\t<span class='fas fa-eye' data-toggle='tooltip' data-placement='left' data-original-title='" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Tag2") + "' ></span>\n";
				this.content += "\t\t\t\t</a>\n";					
				this.content += "\t\t\t\t</td>\";\n";
				this.content += "\t\t\techo \"</tr>\";\n";
				this.content += "\t\t\t$counter++;\n";
				this.content += "\t\t}\n";
				this.content += "\t\t?>\n";
				this.content += "\t</tbody>\n";
				this.content += "</table>\n";
				this.content += "</div>\n";
				this.content += "<div class=\"modal fade\" id=\"modal" + entity.getName() + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n";
				this.content += "\t<div class=\"modal-dialog modal-lg\" >\n";
				this.content += "\t\t<div class=\"modal-content\" id=\"modalContent\">\n";
				this.content += "\t\t</div>\n";
				this.content += "\t</div>\n";
				this.content += "</div>\n";									
				this.content += "<script>\n";
				this.content += "\t$('body').on('show.bs.modal', '.modal', function (e) {\n";
				this.content += "\t\tvar link = $(e.relatedTarget);\n";
				this.content += "\t\t$(this).find(\".modal-content\").load(link.attr(\"href\"));\n";
				this.content += "\t});\n";
				this.content += "</script>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0,  1).toLowerCase() + entity.getName().substring(1) + "/search" + entity.getName() + "Ajax.php");
				this.content="";
				this.content += "<?php\n";
				for(Entity entityModal : this.model.getEntities()){			
					this.content += "require(\"" + PHPGenerator.BUSINESS + "/" + entityModal.getName() + ".php\");\n";
				}
				this.content += "require_once(\"" + PHPGenerator.PERSISTANCE + "/Connection.php\");\n";
				this.content += "$id" + entity.getName() + " = $_GET ['id" + entity.getName() + "'];\n";
				this.content += "$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " = new " + entity.getName() + "($id" + entity.getName() + ");\n";
				this.content += "$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> select();\n";
				this.content += "?>\n";
				this.content += "<script charset=\"utf-8\">\n";
				this.content += "\t$(function () { \n";
				this.content += "\t\t$(\"[data-toggle='tooltip']\").tooltip(); \n";
				this.content += "\t}); \n";
				this.content += "</script>\n";
				this.content += "<div class=\"modal-header\">\n";
				this.content += "\t<h4 class=\"modal-title\">" + Utilities.replaceCapitalLetter(entity.getName()) + "</h4>\n";
				this.content += "\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n";
				this.content += "</div>\n";
				this.content += "<div class=\"modal-body\">\n";
				this.content += "\t<table class=\"table table-striped table-hover\">\n";
				for(Attribute attribute : entity.getAttributes()){
					if(!attribute.getType().equals(Attribute.PASSWORD) && !attribute.isPrimaryKey()){
						this.content += "\t\t<tr>\n";
						this.content += "\t\t\t<th>" + Utilities.replaceCapitalLetter(attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1)) + "</th>\n";
						this.content += "\t\t\t<td><?php echo str_replace(\";; \", \"<br>\", $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + " -> get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "()) ?></td>\n";	
						this.content += "\t\t</tr>\n";
					}
				}
				this.content += "\t</table>\n";
				this.content += "</div>\n";
				consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/modal" + entity.getName() + ".php");
			}
		}		
		return consoleStream;	
	}
}