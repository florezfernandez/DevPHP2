package business;

public class Utilities {

	public static String replaceCapitalLetter(String source) {
	    String target="";
	    int previousIndex = 0;
		for(int i = 1; i < source.length(); i++) {
	        if(Character.isUpperCase(source.charAt(i))) {
	            target += source.substring(previousIndex, i) + " ";
	            previousIndex = i;
	        }
	    }
	    return (!target.equals(""))?(target+source.substring(previousIndex)):source;
	}
}
