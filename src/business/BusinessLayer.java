package business;

import java.io.IOException;

import persistance.FileUtility;

public class BusinessLayer {

	private Model model;
	private String content;
	private FileUtility fileUtility = new FileUtility();

	public BusinessLayer(Model model) {
		this.model = model;
	}

	/**
	 * Create all classes in the business layer based on the collection of entities
	 * @param generationDirectory: Directory where the PHP project is going to be created
	 * @return Message indicating the path of the created PHP files that contains the classes of the project 
	 * @throws IOException: The path where the PHP files are going to be created does not exist
	 */
	public String createBusinessLayer(String generationDirectory) throws IOException{
		String consoleStream = "";
		for(Entity entity : this.model.getEntities()){			
			this.content = "<?php\n";
			this.content += "require_once (\"persistence/" + entity.getName() + "DAO.php\");\n";
			this.content += "require_once (\"persistence/Connection.php\");\n";
			this.content += "\nclass " + entity.getName() + " {\n";
			this.createAttributes(entity);
			this.createRelations(entity);
			this.content += "\tprivate $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO;\n";
			this.content += "\t" + (entity.isExtensible()?"protected":"private") + " $connection;\n";
			this.createGettersAndSetters(entity);
			this.createConstructor(entity);
			this.createLogIn(entity);
			this.createInsert(entity);
			this.createUpdate(entity);
			this.createUpdatePassword(entity);
			this.createExistEmail(entity);
			this.createRecoverPassword(entity);
			this.createUpdateImage(entity);
			this.createUpdateFile(entity);
			this.createSelect(entity);
			this.createSelectAll(entity);
			this.createSelectAllByRelation(entity);
			this.createSelectAllOrder(entity);
			this.createSelectAllByRelationOrder(entity);
			this.createSearch(entity);
			this.createDelete(entity);
			this.content += "}\n";
			this.content += "?>\n";
			consoleStream += this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.BUSINESS + "/" + entity.getName()+".php");
		}
		return consoleStream;
	}

	private void createAttributes(Entity entity) {
		for(Attribute attribute : entity.getAttributes()){
			this.content += "\t" + (entity.isExtensible()?"protected":"private") + " $" + attribute.getName() + ";\n";					
		}
	}

	private void createRelations(Entity entity) {
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "\t" + (entity.isExtensible()?"protected":"private") + " $" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ";\n";										
			}
		}			
	}
	
	private void createGettersAndSetters(Entity entity) {
		for(Attribute attribute : entity.getAttributes()){
			this.content += "\n\tfunction get" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "() {\n";
			this.content += "\t\treturn $this -> " + attribute.getName() + ";\n";
			this.content += "\t}\n";
			this.content += "\n\tfunction set" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + "($p" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + ") {\n";
			this.content += "\t\t$this -> " + attribute.getName() + " = $p" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + ";\n";
			this.content += "\t}\n";					
		}
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "\n\tfunction get" + relation.getEntity() + "() {\n";
				this.content += "\t\treturn $this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1)  + ";\n";
				this.content += "\t}\n";
				this.content += "\n\tfunction set" + relation.getEntity() + "($p" + relation.getEntity() + ") {\n";
				this.content += "\t\t$this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " = $p" + relation.getEntity() + ";\n";
				this.content += "\t}\n";
			}
		}				
	}
	
	private void createConstructor(Entity entity) {
		this.content += "\n\tfunction __construct(";
		for(Attribute attribute : entity.getAttributes()){
			this.content += "$p" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + " = \"\", ";					
		}
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "$p" + relacion.getEntity()+" = \"\", ";
			}
		}	
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += "){\n";
		for(Attribute attribute : entity.getAttributes()){
			this.content += "\t\t$this -> " + attribute.getName() + " = $p"+attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + ";\n";					
		}	
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "\t\t$this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " = $p"+relation.getEntity() + ";\n";					
			}
		}	
		this.content += "\t\t$this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO = new " + entity.getName() + "DAO(";
		for(Attribute attribute : entity.getAttributes()){
			this.content += "$this -> " + attribute.getName() + ", ";
		}	
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "$this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) +", ";
			}
		}			
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += ");\n";
		this.content += "\t\t$this -> connection = new Connection();\n";
		this.content += "\t}\n";		
	}	
	
	private void createLogIn(Entity entity) {
		if(entity.isActor()){
			this.content += "\n\tfunction logIn($email, $password){\n";
			this.content += "\t\t$this -> connection -> open();\n";
			this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().toLowerCase() + "DAO -> logIn($email, $password));\n";
			this.content += "\t\tif($this -> connection -> numRows()==1){\n";
			this.content += "\t\t\t$result = $this -> connection -> fetchRow();\n";
			int numAttributes=0;
			for(Attribute attribute : entity.getAttributes()){
				this.content += "\t\t\t$this -> " + attribute.getName() + " = $result[" + numAttributes + "];\n";					
				numAttributes++;
			}		
			for(Relation relation :entity.getRelations()){
				if(relation.getCardinality().equals("1")){
					this.content += "\t\t\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " = new " + relation.getEntity() + "($result[" + numAttributes + "]);\n";
					this.content += "\t\t\t$" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " -> select();\n";
					this.content += "\t\t\t$this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " = $" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ";\n";					
					numAttributes++;
				}
			}
			this.content += "\t\t\t$this -> connection -> close();\n";
			this.content += "\t\t\treturn true;\n";
			this.content += "\t\t}else{\n";
			this.content += "\t\t\t$this -> connection -> close();\n";
			this.content += "\t\t\treturn false;\n";
			this.content += "\t\t}\n";
			this.content += "\t}\n";
		}			
	}
	
	private void createInsert(Entity entity) {
		this.content += "\n\tfunction insert(){\n";
		this.content += "\t\t$this -> connection -> open();\n";
		this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> insert());\n";
		this.content += "\t\t$id = $this -> connection -> lastId();;\n";
		this.content += "\t\t$this -> connection -> close();\n";
		this.content += "\t\treturn $id;\n";
		this.content += "\t}\n";	
	}	
		
	private void createUpdate(Entity entity) {
		this.content += "\n\tfunction update(){\n";
		this.content += "\t\t$this -> connection -> open();\n";
		this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> update());\n";
		this.content += "\t\t$this -> connection -> close();\n";
		this.content += "\t}\n";			
	}

	private void createUpdatePassword(Entity entity) {
		if(entity.isActor()) {
			this.content += "\n\tfunction update" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "($password){\n";
			this.content += "\t\t$this -> connection -> open();\n";
			this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> update" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "($password));\n";
			this.content += "\t\t$this -> connection -> close();\n";
			this.content += "\t}\n";			
		}
	}

	private void createExistEmail(Entity entity) {
		if(entity.isActor()){
			this.content += "\n\tfunction exist" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "){\n";
			this.content += "\t\t$this -> connection -> open();\n";
			this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().toLowerCase() + "DAO -> exist" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "));\n";
			this.content += "\t\tif($this -> connection -> numRows()==1){\n";
			this.content += "\t\t\t$this -> connection -> close();\n";
			this.content += "\t\t\treturn true;\n";
			this.content += "\t\t}else{\n";
			this.content += "\t\t\t$this -> connection -> close();\n";
			this.content += "\t\t\treturn false;\n";
			this.content += "\t\t}\n";
			this.content += "\t}\n";
		}	
	}

	private void createRecoverPassword(Entity entity) {
		if(entity.isActor()) {
			this.content += "\n\tfunction recoverPassword($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ", $password){\n";
			this.content += "\t\t$this -> connection -> open();\n";
			this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> recoverPassword($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ", $password));\n";
			this.content += "\t\t$this -> connection -> close();\n";
			this.content += "\t}\n";			
		}
	}
	
	private void createUpdateImage(Entity entity) {
		for(Attribute attribute : entity.getAttributes()) {
			if(attribute.isImage()) {
				this.content += "\n\tfunction updateImage" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($value){\n";
				this.content += "\t\t$this -> connection -> open();\n";
				this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> updateImage" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($value));\n";
				this.content += "\t\t$this -> connection -> close();\n";
				this.content += "\t}\n";
				return;
			}
		}
	}

	private void createUpdateFile(Entity entity) {
		for(Attribute attribute : entity.getAttributes()) {
			if(attribute.isFile()) {
				this.content += "\n\tfunction updateFile" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($value){\n";
				this.content += "\t\t$this -> connection -> open();\n";
				this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> updateFile" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($value));\n";
				this.content += "\t\t$this -> connection -> close();\n";
				this.content += "\t}\n";
				return;
			}
		}
	}
	private void createSelect(Entity entity) {
		this.content += "\n\tfunction select(){\n";
		this.content += "\t\t$this -> connection -> open();\n";
		this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> select());\n";
		this.content += "\t\t$result = $this -> connection -> fetchRow();\n";
		this.content += "\t\t$this -> connection -> close();\n";
		int numAttributes = 0;
		for(Attribute attribute : entity.getAttributes()){
			this.content += "\t\t$this -> " + attribute.getName() + " = $result[" + numAttributes + "];\n";					
			numAttributes++;
		}	
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = new " + relacion.getEntity() + "($result[" + numAttributes + "]);\n";
				this.content += "\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " -> select();\n";
				this.content += "\t\t$this -> " + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = $" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + ";\n";					
				numAttributes++;
			}
		}										
		this.content += "\t}\n";				
	}
	
	private void createSelectAll(Entity entity) {
		this.content += "\n\tfunction selectAll(){\n";
		this.content += "\t\t$this -> connection -> open();\n";
		this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> selectAll());\n";
		this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = array();\n";
		this.content += "\t\twhile ($result = $this -> connection -> fetchRow()){\n";		
		int numAttributes=0;
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = new " + relacion.getEntity() + "($result[" + (entity.getAttributes().size() + numAttributes) + "]);\n";
				this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " -> select();\n";
				numAttributes++;
			}
		}										
		this.content += "\t\t\tarray_push($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s, new " + entity.getName() + "(";
		numAttributes=0;
		for(@SuppressWarnings("unused") Attribute attribute : entity.getAttributes()){
			this.content += "$result[" + numAttributes + "], ";					
			numAttributes++;
		}
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + ", ";					
			}
		}
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += "));\n";
		this.content += "\t\t}\n";
		this.content += "\t\t$this -> connection -> close();\n";
		this.content += "\t\treturn $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s;\n";
		this.content += "\t}\n";		
	}	
	
	private void createSelectAllByRelation(Entity entity) {
		for(Relation relationByEntity : entity.getRelations()) {
			if(relationByEntity.getCardinality().equals("1")) {
				this.content += "\n\tfunction selectAllBy" + relationByEntity.getEntity() + "(){\n";
				this.content += "\t\t$this -> connection -> open();\n";
				this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> selectAllBy" + relationByEntity.getEntity() + "());\n";
				this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = array();\n";
				this.content += "\t\twhile ($result = $this -> connection -> fetchRow()){\n";		
				int numAttributes=0;
				for(Relation relacion : entity.getRelations()){
					if(relacion.getCardinality().equals("1")){
						this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = new " + relacion.getEntity() + "($result[" + (entity.getAttributes().size() + numAttributes) + "]);\n";
						this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " -> select();\n";
						numAttributes++;
					}
				}										
				this.content += "\t\t\tarray_push($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s, new " + entity.getName() + "(";
				numAttributes=0;
				for(@SuppressWarnings("unused") Attribute attribute : entity.getAttributes()){
					this.content += "$result[" + numAttributes + "], ";					
					numAttributes++;
				}
				for(Relation relacion : entity.getRelations()){
					if(relacion.getCardinality().equals("1")){
						this.content += "$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + ", ";					
					}
				}
				this.content = this.content.substring(0, this.content.length()-2);
				this.content += "));\n";
				this.content += "\t\t}\n";
				this.content += "\t\t$this -> connection -> close();\n";
				this.content += "\t\treturn $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s;\n";
				this.content += "\t}\n";					
			}
		}
	}

	private void createSelectAllOrder(Entity entity) {
		this.content += "\n\tfunction selectAllOrder($order, $dir){\n";
		this.content += "\t\t$this -> connection -> open();\n";
		this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> selectAllOrder($order, $dir));\n";
		this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = array();\n";
		this.content += "\t\twhile ($result = $this -> connection -> fetchRow()){\n";		
		int numAttributes=0;
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = new " + relacion.getEntity() + "($result[" + (entity.getAttributes().size() + numAttributes) + "]);\n";
				this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " -> select();\n";
				numAttributes++;
			}
		}										
		this.content += "\t\t\tarray_push($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s, new " + entity.getName() + "(";
		numAttributes=0;
		for(@SuppressWarnings("unused") Attribute attribute : entity.getAttributes()){
			this.content += "$result[" + numAttributes + "], ";					
			numAttributes++;
		}
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + ", ";					
			}
		}
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += "));\n";
		this.content += "\t\t}\n";
		this.content += "\t\t$this -> connection -> close();\n";
		this.content += "\t\treturn $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s;\n";
		this.content += "\t}\n";		
	}

	private void createSelectAllByRelationOrder(Entity entity) {
		for(Relation relationByEntity : entity.getRelations()) {
			if(relationByEntity.getCardinality().equals("1")) {
				this.content += "\n\tfunction selectAllBy" + relationByEntity.getEntity() + "Order($order, $dir){\n";
				this.content += "\t\t$this -> connection -> open();\n";
				this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> selectAllBy" + relationByEntity.getEntity() + "Order($order, $dir));\n";
				this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = array();\n";
				this.content += "\t\twhile ($result = $this -> connection -> fetchRow()){\n";		
				int numAttributes=0;
				for(Relation relacion : entity.getRelations()){
					if(relacion.getCardinality().equals("1")){
						this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = new " + relacion.getEntity() + "($result[" + (entity.getAttributes().size() + numAttributes) + "]);\n";
						this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " -> select();\n";
						numAttributes++;
					}
				}										
				this.content += "\t\t\tarray_push($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s, new " + entity.getName() + "(";
				numAttributes=0;
				for(@SuppressWarnings("unused") Attribute attribute : entity.getAttributes()){
					this.content += "$result[" + numAttributes + "], ";					
					numAttributes++;
				}
				for(Relation relacion : entity.getRelations()){
					if(relacion.getCardinality().equals("1")){
						this.content += "$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + ", ";					
					}
				}
				this.content = this.content.substring(0, this.content.length()-2);
				this.content += "));\n";
				this.content += "\t\t}\n";
				this.content += "\t\t$this -> connection -> close();\n";
				this.content += "\t\treturn $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s;\n";
				this.content += "\t}\n";					
			}
		}
	}

	private void createSearch(Entity entity) {
		this.content += "\n\tfunction search($search){\n";
		this.content += "\t\t$this -> connection -> open();\n";
		this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> search($search));\n";
		this.content += "\t\t$" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s = array();\n";
		this.content += "\t\twhile ($result = $this -> connection -> fetchRow()){\n";		
		int numAttributes=0;
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " = new " + relacion.getEntity() + "($result[" + (entity.getAttributes().size() + numAttributes) + "]);\n";
				this.content += "\t\t\t$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + " -> select();\n";
				numAttributes++;
			}
		}										
		this.content += "\t\t\tarray_push($" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s, new " + entity.getName() + "(";
		numAttributes=0;
		for(@SuppressWarnings("unused") Attribute attribute : entity.getAttributes()){
			this.content += "$result[" + numAttributes + "], ";					
			numAttributes++;
		}
		for(Relation relacion : entity.getRelations()){
			if(relacion.getCardinality().equals("1")){
				this.content += "$" + relacion.getEntity().substring(0, 1).toLowerCase() + relacion.getEntity().substring(1) + ", ";					
			}
		}
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += "));\n";
		this.content += "\t\t}\n";
		this.content += "\t\t$this -> connection -> close();\n";
		this.content += "\t\treturn $" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "s;\n";
		this.content += "\t}\n";			
	}

	private void createDelete(Entity entity) {
		if(entity.isDelete()) {
			this.content += "\n\tfunction delete(){\n";
			this.content += "\t\t$this -> connection -> open();\n";
			this.content += "\t\t$this -> connection -> run($this -> " + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "DAO -> delete());\n";
			this.content += "\t\t$success = $this -> connection -> querySuccess();\n";
			this.content += "\t\t$this -> connection -> close();\n";
			this.content += "\t\treturn $success;\n";
			this.content += "\t}\n";			
		}		
	}
}
