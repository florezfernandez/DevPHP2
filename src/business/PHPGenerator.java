package business;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import persistance.FileUtility;
import persistance.XMLReader;

public class PHPGenerator {

	private Model model;
	public final static String BUSINESS="business";
	public final static String PERSISTANCE="persistence";
	public final static String UI="ui";
	public final static String UML="uml";
	public final static String IMG="img";
	public final static String CSS="css";
	public final static String JS="js";
	public final static String FONTS="fonts";
	public final static String IMAGE="image";
	public final static String FILE="file";
	public final static String APPLICATION="application";
	
		
	/**
	 * Generate directories and files of the PHP project
	 * @param generationDirectory: Directory where the PHP project is going to be created
	 * @param xmlPath: Path of the XML model
	 * @throws Exception 
	 */
	public String generatePHP(String generationDirectory, String xmlPath) throws Exception {
		String consoleStream = "";
		this.loadXMLModel(xmlPath);
		FileUtility fileUtility = new FileUtility();		
		FileWriter f;
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.BUSINESS);		
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.BUSINESS + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.BUSINESS + "/index.php";
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.PERSISTANCE);
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.PERSISTANCE + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.PERSISTANCE + "/index.php";
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.UI);
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.UI + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.UI + "/index.php";
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.UML);
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.UML + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.UML + "/index.php";
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.IMG);
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.IMG + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.IMG + "/index.php";
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.JS);
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.JS + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.JS + "/index.php";
		consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.IMAGE);
		f = new FileWriter(generationDirectory + "/" + PHPGenerator.IMAGE + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
	    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.IMAGE + "/index.php";
	    consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.FILE);
	    f = new FileWriter(generationDirectory + "/" + PHPGenerator.FILE + "/index.php");
	    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
	    f.close();
		consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.FILE + "/index.php";
		consoleStream += this.copyBaseFiles(generationDirectory);
		for(Entity entity : this.model.getEntities()){
			consoleStream += fileUtility.createDirectory(generationDirectory + "/" + PHPGenerator.UI + "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1));
			f = new FileWriter(generationDirectory + "/" + PHPGenerator.UI+ "/" + entity.getName().substring(0, 1).toLowerCase() + entity.getName().substring(1) + "/index.php");
		    f.write("<script>location.replace(\"http://" + this.model.getAcronym().toLowerCase() + ".itiud.org\");</script>");
		    f.close();
		    consoleStream += "Created file: " + generationDirectory + "/" + PHPGenerator.IMAGE + "/index.php";
		}
		BusinessLayer businessLayer = new BusinessLayer(this.model);
		consoleStream += businessLayer.createBusinessLayer(generationDirectory);
		
		PersistenceLayer persistenceLayer = new PersistenceLayer(this.model);
		consoleStream += persistenceLayer.createPersistenceLayer(generationDirectory);
		
		UILayer uiLayer = new UILayer(this.model);
		consoleStream += uiLayer.createUILayer(generationDirectory);
		
		UMLGenerator umlGenerator = new UMLGenerator(model);
		consoleStream += umlGenerator.createUML(generationDirectory);
		
		return consoleStream;
	}
	
	/**
	 * Load the XML model into the attribute called model
	 * @param xmlPath: Path of the XML model
	 * @throws IOException: The file of the xmlPath does not exists 
	 * @throws JDOMException: XML file incorrect 
	 */
	@SuppressWarnings("unchecked")
	public void loadXMLModel(String xmlPath) throws JDOMException, IOException {
		XMLReader xmlReader = new XMLReader();		
		Document document = xmlReader.readXML(xmlPath);
		Element root = document.getRootElement();
		String name = root.getAttributeValue(Model.NAME);
		String acronym = root.getAttributeValue(Model.ACRONYM);
		String description = root.getAttributeValue(Model.DESCRIPTION);
		String language = root.getAttributeValue(Model.LANGUAGE);		
		String template = root.getAttributeValue(Model.TEMPLATE);		
		this.model = new Model(name, acronym, description, language, template);		
		
		this.createEntityAdministrator();
		Attribute attribute;
		List<Element> entitiesXML = root.getChildren(Model.ENTITY);
		for(Element entityXML : entitiesXML){
			Entity entity = new Entity(entityXML.getAttributeValue(Model.NAME));
			if(entityXML.getAttributeValue(Model.ACTOR) != null){
				entity.setActor(Boolean.parseBoolean(entityXML.getAttributeValue(Model.ACTOR)));
				this.createEntityLog(entity.getName());
			}
			if(entityXML.getAttributeValue(Model.MENU_DEPLOY)==null){
				entity.setMenuDeploy(true);
			}else {
				entity.setMenuDeploy(Boolean.parseBoolean(entityXML.getAttributeValue(Model.MENU_DEPLOY)));
			}
			if(entityXML.getAttributeValue(Model.DELETE) != null){
				entity.setDelete(Boolean.parseBoolean(entityXML.getAttributeValue(Model.DELETE)));
			}
			if(entityXML.getAttributeValue(Model.EXTENSIBLE) != null){
				entity.setExtensible(Boolean.parseBoolean(entityXML.getAttributeValue(Model.EXTENSIBLE)));
			}
			List<Element> attributesXML = entityXML.getChildren(Model.ATTRIBUTE);
			attribute = new Attribute("id" + entity.getName());
			attribute.setPrimaryKey(true);
			attribute.setType(Attribute.INT);
			attribute.setMandatory(true);
			entity.addAttribute(attribute);
			if(entityXML.getAttributeValue(Model.ACTOR) != null){
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_name"));
				attribute.setType(Attribute.STRING);
				attribute.setMandatory(true);
				attribute.setVisible(true);
				attribute.setDeploy(true);
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_lastName"));
				attribute.setType(Attribute.STRING);
				attribute.setMandatory(true);
				attribute.setVisible(true);
				attribute.setOrder("asc");
				attribute.setDeploy(true);
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email"));
				attribute.setType(Attribute.EMAIL);
				attribute.setMandatory(true);
				attribute.setVisible(true);
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_password"));
				attribute.setType(Attribute.PASSWORD);
				attribute.setMandatory(true);
				attribute.setVisible(true);
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_picture"));
				attribute.setType(Attribute.STRING);
				attribute.setImage(true);				
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_phone"));
				attribute.setType(Attribute.STRING);
				attribute.setVisible(true);
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_mobile"));
				attribute.setType(Attribute.STRING);
				attribute.setVisible(true);
				entity.addAttribute(attribute);
				attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_state"));
				attribute.setType(Attribute.STATE);
				attribute.setVisible(true);
				attribute.setMandatory(true);
				entity.addAttribute(attribute);
			}
			for(Element attributeXML : attributesXML){
				attribute = new Attribute(attributeXML.getAttributeValue(Model.NAME));
				if(attributeXML.getAttributeValue(Model.MANDOTORY) != null){
					attribute.setMandatory(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.MANDOTORY)));
				}				
				if(attributeXML.getAttributeValue(Model.TYPE) != null){
					attribute.setType(attributeXML.getAttributeValue(Model.TYPE));
				}				
				if(attributeXML.getAttributeValue(Model.NOWRAP) != null){
					attribute.setNowrap(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.NOWRAP)));
				}	
				attribute.setVisible(true);
				if(attributeXML.getAttributeValue(Model.VISIBLE) != null){
					attribute.setVisible(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.VISIBLE)));
				}								
				if(attributeXML.getAttributeValue(Model.ORDER) != null){
					attribute.setOrder(attributeXML.getAttributeValue(Model.ORDER));
				}								
				if(attributeXML.getAttributeValue(Model.DEPLOY) != null){
					attribute.setDeploy(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.DEPLOY)));
				}								
				if(attributeXML.getAttributeValue(Model.LENGTH) != null){
					attribute.setLength(Integer.parseInt(attributeXML.getAttributeValue(Model.LENGTH)));
				}												
				if(attributeXML.getAttributeValue(Model.URL) != null){
					attribute.setUrl(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.URL)));
				}												
				if(attributeXML.getAttributeValue(Model.IMAGE) != null){
					attribute.setImage(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.IMAGE)));
				}				
				if(attributeXML.getAttributeValue(Model.FILE) != null){
					attribute.setFile(Boolean.parseBoolean(attributeXML.getAttributeValue(Model.FILE)));
				}
				if(attributeXML.getAttributeValue(Model.INFO) != null){
					attribute.setInfo(attributeXML.getAttributeValue(Model.INFO));
				}				
				entity.addAttribute(attribute);
			}
			
			List<Element> relationsXML = entityXML.getChildren(Model.RELATION);
			for(Element relationXML : relationsXML){
				Relation relation = new Relation(relationXML.getAttributeValue(Model.CARDINALITY),relationXML.getAttributeValue(Model.ENTITY));
				entity.addRelation(relation);
			}
			if(entity.isActor()){
				Relation relation = new Relation("*","Log" + entity.getName());
				entity.addRelation(relation);	
				List<Element> servicesXML = entityXML.getChildren(Model.SERVICE);
				for(Element serviceXML : servicesXML){
					Service service = new Service(serviceXML.getAttributeValue(Model.ENTITY));
					if(serviceXML.getAttributeValue(Model.CREATE) != null){
						service.setCreate(Boolean.parseBoolean(serviceXML.getAttributeValue(Model.CREATE)));
					}				
					if(serviceXML.getAttributeValue(Model.GET) != null){
						service.setGet(Boolean.parseBoolean(serviceXML.getAttributeValue(Model.GET)));
					}				
					if(serviceXML.getAttributeValue(Model.EDIT) != null){
						service.setEdit(Boolean.parseBoolean(serviceXML.getAttributeValue(Model.EDIT)));
					}				
					if(serviceXML.getAttributeValue(Model.DELETE) != null){
						service.setDelete(Boolean.parseBoolean(serviceXML.getAttributeValue(Model.DELETE)));
					}									
					entity.addService(service);
				}
			}
			model.addEntity(entity);
		}	
	}	
	
	/**
	 * Create by default the entity Administrator with its corresponding log
	 */
	private void createEntityAdministrator() {
		Entity entity = new Entity(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator"));
		entity.setAdmin(true);
		entity.setActor(true);
		entity.setMenuDeploy(true);
		Attribute attribute;
		attribute = new Attribute("id" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator"));
		attribute.setPrimaryKey(true);
		attribute.setType(Attribute.INT);
		attribute.setMandatory(true);
		attribute.setVisible(true);		
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_name"));
		attribute.setType(Attribute.STRING);
		attribute.setMandatory(true);
		attribute.setVisible(true);
		attribute.setDeploy(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_lastName"));
		attribute.setType(Attribute.STRING);
		attribute.setMandatory(true);
		attribute.setVisible(true);
		attribute.setOrder("asc");
		attribute.setDeploy(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email"));
		attribute.setType(Attribute.EMAIL);
		attribute.setMandatory(true);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_password"));
		attribute.setType(Attribute.PASSWORD);
		attribute.setMandatory(true);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_picture"));
		attribute.setType(Attribute.STRING);
		attribute.setImage(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_phone"));
		attribute.setType(Attribute.STRING);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_mobile"));
		attribute.setType(Attribute.STRING);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_state"));
		attribute.setType(Attribute.STATE);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute.setMandatory(true);
		Relation relation;
		relation = new Relation("*", "Log" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator"));
		entity.addRelation(relation);
		model.addEntity(entity);
		this.createEntityLog(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator"));		
	}
	
	/**
	 * Create Log entity for entities whose the value of the attribute Actor is true 
	 * @param entityName: Name of the entity to create the Log
	 */
	private void createEntityLog(String entityName){
		Entity entity = new Entity("Log" + entityName);
		entity.setLog(true);
		entity.setMenuDeploy(true);
		Attribute attribute;
		attribute = new Attribute("idLog" + entityName);
		attribute.setPrimaryKey(true);
		attribute.setType(Attribute.INT);
		attribute.setMandatory(true);
		entity.addAttribute(attribute);		
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_action"));
		attribute.setMandatory(true);	
		attribute.setLength(100);
		attribute.setType(Attribute.STRING);
		attribute.setVisible(true);
		entity.addAttribute(attribute);		
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_information"));
		attribute.setMandatory(true);
		attribute.setType(Attribute.TEXT);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_date"));
		attribute.setMandatory(true);
		attribute.setType(Attribute.DATE);
		attribute.setVisible(true);
		attribute.setNowrap(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_time"));
		attribute.setMandatory(true);
		attribute.setType(Attribute.TIME);
		attribute.setNowrap(true);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_ip"));
		attribute.setMandatory(true);
		attribute.setType(Attribute.STRING);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_os"));
		attribute.setMandatory(true);
		attribute.setType(Attribute.STRING);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		attribute = new Attribute(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_browser"));
		attribute.setMandatory(true);
		attribute.setType(Attribute.STRING);
		attribute.setVisible(true);
		entity.addAttribute(attribute);
		Relation relation;
		relation = new Relation("1", entityName);
		entity.addRelation(relation);
		model.addEntity(entity);
	}
	
	/**
	 * Copy files of Bootstrap, JQuery, and text editor
	 * @param generationDirectory: Directory where the project is going to be generated
	 * @return 
	 * @throws IOException: TBD
	 */
	private String copyBaseFiles(String generationDirectory) throws IOException {
		String consoleStream = "";
		ArrayList<String> dirs= new ArrayList<String>();
		dirs.add("img/");		
		dirs.add("js/");
		FileUtility fileUtility = new FileUtility();
		for(String dir : dirs){
			File folder = new File(dir);
			File[] listOfFiles = folder.listFiles();
			for(File f : listOfFiles) {
				fileUtility.copyFiles(f.getAbsolutePath(), generationDirectory + "/" + f);
				consoleStream += "File created: " + f.getName() + "\n";
			}
		}
		return consoleStream;
	}
}
