package business;

import java.util.ArrayList;

public class Model {
	
	public final static String NAME = "name";
	public final static String ACRONYM = "acronym";
	public final static String DESCRIPTION = "description";
	public final static String LANGUAGE = "language";
	public final static String TEMPLATE = "template";
	public final static String ENTITY = "entity";
	public final static String ATTRIBUTE = "attribute";
	public final static String RELATION = "relation";
	public final static String SERVICE = "service";

	public final static String ACTOR = "actor";
	public final static String PRIMARY_KEY= "primaryKey";
	public final static String MANDOTORY = "mandatory";
	public final static String TYPE = "type";
	public final static String NOWRAP = "nowrap";
	public final static String VISIBLE = "visible";
	public final static String ORDER = "order";
	public final static String ASC = "asc";
	public final static String DESC = "desc";
	public final static String DEPLOY = "deploy";
	public final static String LENGTH = "length";
	public final static String URL ="url";
	public final static String IMAGE ="image";
	public final static String FILE ="file";	
	public final static String INFO ="info";
	public final static String CARDINALITY = "cardinality";
	public final static String MENU_DEPLOY = "menuDeploy";
	public final static String CREATE = "create";
	public final static String GET = "get";
	public final static String EDIT = "edit";
	public final static String DELETE = "delete";
	public final static String EXTENSIBLE = "extensible";
	
	public final static String TRUE = "true";
	public final static String FALSE = "false";
	
	private String name;
	private String acronym;
	private String description;
	private String language;
	private String template;
	private ArrayList<Entity> entities = new ArrayList<Entity>();
		
	public ArrayList<Entity> getEntities() {
		return entities;
	}

	public void setEntities(ArrayList<Entity> entities) {
		this.entities = entities;
	}

	public String getName() {
		return name;
	}

	public String getAcronym() {
		return acronym;
	}

	public String getDescription() {
		return description;
	}

	public String getLanguage() {
		return language;
	}
	
	public String getTemplate() {
		return template;
	}

	public Model(String name, String acronym, String description, String language, String template) {
		this.name = name;
		this.acronym = acronym;
		this.description = description;
		this.language = language;
		this.template = template;
	}

	public void addEntity(Entity entity) {
		this.entities.add(entity);		
	}

	public Entity findEntity(String entityName){
		for(Entity entity : this.entities){
			if(entity.getName().equals(entityName)){
				return entity;
			}
		}
		return null;
	}
}
