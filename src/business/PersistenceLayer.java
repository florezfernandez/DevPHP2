package business;

import java.io.IOException;

import persistance.FileUtility;

public class PersistenceLayer {

	private Model model;
	private String content;
	private FileUtility fileUtility = new FileUtility();

	public PersistenceLayer(Model model) {
		this.model = model;
	}

	public String createPersistenceLayer(String generationDirectory) throws IOException {
		String consoleStream = "";
		consoleStream += this.createDAO(generationDirectory);
		consoleStream += this.createConnection(generationDirectory);		
		consoleStream += this.createSQL(generationDirectory);
		return consoleStream;
	}


	private String createDAO(String generationDirectory) throws IOException {
		String consoleStream = "";
		for(Entity entity:this.model.getEntities()){			
			this.content = "<?php\n";
			this.content += "class " + entity.getName() + "DAO{\n";
			this.createAttributes(entity);
			this.createRelations(entity);
			this.createConstructor(entity);
			this.createLogIn(entity);
			this.createInsert(entity);
			this.createUpdate(entity);
			this.createUpdatePassword(entity);
			this.createExistEmail(entity);
			this.createRecoverPassword(entity);
			this.createUpdateImage(entity);
			this.createUpdateFile(entity);
			this.createSelect(entity);
			this.createSelectAll(entity);
			this.createSelectAllByRelation(entity);
			this.createSelectAllOrder(entity);
			this.createSelectAllByRelationOrder(entity);
			this.createSearch(entity);
			this.createDelete(entity);
			this.content += "}\n";
			this.content += "?>\n";
			consoleStream += this.fileUtility.writePHP(this.content, generationDirectory + "/" + PHPGenerator.PERSISTANCE + "/" + entity.getName() + "DAO.php");
		}
		return consoleStream;
	}

	private void createAttributes(Entity entity) {
		for(Attribute attribute : entity.getAttributes()){
			this.content += "\t" + (entity.isExtensible()?"protected":"private") + " $" + attribute.getName() + ";\n";					
		}
	}

	private void createRelations(Entity entity) {
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "\t" + (entity.isExtensible()?"protected":"private") + " $" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + ";\n";										
			}
		}			
	}

	private void createConstructor(Entity entity) {
		this.content += "\n\tfunction __construct(";
		for(Attribute attribute : entity.getAttributes()){
			this.content += "$p" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + " = \"\", ";					
		}
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "$p" + relation.getEntity()+" = \"\", ";
			}
		}	
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += "){\n";
		for(Attribute attribute : entity.getAttributes()){
			this.content += "\t\t$this -> " + attribute.getName() + " = $p" + attribute.getName().substring(0,1).toUpperCase() + attribute.getName().substring(1) + ";\n";					
		}	
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "\t\t$this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " = $p"+relation.getEntity() + ";\n";					
			}
		}	
		this.content += "\t}\n";		
	}	

	private void createLogIn(Entity entity) {
		if(entity.isActor()){
			this.content += "\n\tfunction logIn($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ", $" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_password") + "){\n";
			this.content += "\t\treturn \"select ";
			for(Attribute attribute:entity.getAttributes()){
				this.content += attribute.getName() + ", ";					
			}
			for(Relation relation:entity.getRelations()){
				if(relation.getCardinality().equals("1")){
					this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";					
				}
			}
			this.content = this.content.substring(0, this.content.length()-2) + "\n";
			this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
			this.content += "\t\t\t\twhere " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + " = '\" . $" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + " . \"' and " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_password") + " = '\" . md5($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_password") + ") . \"'\";\n";
			this.content += "\t}\n";
		}					
	}

	private void createInsert(Entity entity) {
		this.content += "\n\tfunction insert(){\n";
		this.content += "\t\treturn \"insert into " + entity.getName() + "(";
		for(Attribute attribute : entity.getAttributes()){
			if(!attribute.isPrimaryKey()){
				if(attribute.getType().equals(Attribute.DATE) && !attribute.isMandatory()) {
					this.content += "\" . (($this -> " + attribute.getName() + "!=\"\")?\"" + attribute.getName() + ", \":\"\") . \"";
				}else {
					this.content += attribute.getName() + ", ";	
				}															
			}
		}			
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";											
			}
		}
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += ")\n";
		this.content += "\t\t\t\tvalues(";
		for(Attribute attribute : entity.getAttributes()){
			if(!attribute.isPrimaryKey()){
				if(attribute.getType().equals(Attribute.DATE) && !attribute.isMandatory()) {
					this.content += "\" . (($this -> " + attribute.getName() + "!=\"\")?\"'\" . $this -> " + attribute.getName() + " . \"', \":\"\") . \"";
				}else {
					if(!attribute.getType().equals(Attribute.PASSWORD)){
						this.content += "'\" . $this -> " + attribute.getName() + " . \"', ";																	
					}else{
						this.content += "md5('\" . $this -> " + attribute.getName() + " . \"'), ";											
					}					
				}
			}
		}	
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "'\" . $this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " . \"', ";											
			}
		}
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += ")\";\n";
		this.content += "\t}\n";		
	}

	private void createUpdate(Entity entity) {
		this.content += "\n\tfunction update(){\n";
		this.content += "\t\treturn \"update " + entity.getName() + " set \n";
		for(Attribute attribute:entity.getAttributes()){
			if(!attribute.isPrimaryKey()){
				if(!attribute.getType().equals(Attribute.PASSWORD) && !attribute.isImage() && !attribute.isFile()){
					
					if(attribute.getType().equals(Attribute.DATE) && !attribute.isMandatory()) {
						this.content += "\t\t\t\t\" . (($this -> " + attribute.getName() + "!=\"\")?\"" + attribute.getName() + " = '\" . $this -> " + attribute.getName() + " . \"', \":\"\") . \"\n";
					}else {
						this.content += "\t\t\t\t" + attribute.getName() + " = '\" . $this -> " + attribute.getName() + " . \"',\n";	
					}															
				}
			}
		}
		for(Relation relation:entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += "\t\t\t\t" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + " = '\" . $this -> " + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + " . \"',\n";											
			}
		}
		this.content = this.content.substring(0, this.content.length()-2);
		this.content += "\t\n";		
		this.content += "\t\t\t\twhere id" + entity.getName() + " = '\" . $this -> id" + entity.getName() + " . \"'\";\n";		
		this.content += "\t}\n";		
	}

	private void createUpdatePassword(Entity entity) {
		if(entity.isActor()) {
			this.content += "\n\tfunction update" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Password") + "($password){\n";
			this.content += "\t\treturn \"update " + entity.getName() + " set \n";
			for(Attribute attribute:entity.getAttributes()){
				if(attribute.getType().equals(Attribute.PASSWORD)){
					this.content += "\t\t\t\t" + attribute.getName() + " = '\" . md5($password) . \"'\n";																
				}
			}
			this.content += "\t\t\t\twhere id" + entity.getName() + " = '\" . $this -> id" + entity.getName() + " . \"'\";\n";		
			this.content += "\t}\n";							
		}
	}

	private void createExistEmail(Entity entity) {
		if(entity.isActor()){
			this.content += "\n\tfunction exist" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Email") + "($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + "){\n";
			this.content += "\t\treturn \"select ";
			for(Attribute attribute:entity.getAttributes()){
				this.content += attribute.getName() + ", ";					
			}
			for(Relation relation:entity.getRelations()){
				if(relation.getCardinality().equals("1")){
					this.content += relation.getEntity().toLowerCase() + "_id" + relation.getEntity() + ", ";					
				}
			}
			this.content = this.content.substring(0, this.content.length()-2) + "\n";
			this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
			this.content += "\t\t\t\twhere " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + " = '\" . $" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ". \"'\";\n";
			this.content += "\t}\n";
		}							
	}

	private void createRecoverPassword(Entity entity) {
		if(entity.isActor()) {
			this.content += "\n\tfunction recoverPassword($" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ", $password){\n";
			this.content += "\t\treturn \"update " + entity.getName() + " set \n";
			for(Attribute attribute:entity.getAttributes()){
				if(attribute.getType().equals(Attribute.PASSWORD)){
					this.content += "\t\t\t\t" + attribute.getName() + " = '\" . md5($password) . \"'\n";																
				}
			}
			this.content += "\t\t\t\twhere ";
			for(Attribute attribute:entity.getAttributes()){
				if(attribute.getType().equals(Attribute.EMAIL)){
					this.content += attribute.getName();																
				}
			}			
			this.content += " = '\" . $" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ". \"'\";\n";		
			this.content += "\t}\n";							
		}		
	}

	private void createUpdateImage(Entity entity) {
		for(Attribute attribute : entity.getAttributes()) {
			if(attribute.isImage()) {
				this.content += "\n\tfunction updateImage" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($value){\n";
				this.content += "\t\treturn \"update " + entity.getName() + " set \n";
				this.content += "\t\t\t\t" + attribute.getName() + " = '\" . $value . \"'\n";																
				this.content += "\t\t\t\twhere id" + entity.getName() + " = '\" . $this -> id" + entity.getName() + " . \"'\";\n";
				this.content += "\t}\n";							
			}				
		}		
	}

	private void createUpdateFile(Entity entity) {
		for(Attribute attribute : entity.getAttributes()) {
			if(attribute.isFile()) {
				this.content += "\n\tfunction updateFile" + attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1) + "($value){\n";
				this.content += "\t\treturn \"update " + entity.getName() + " set \n";
				this.content += "\t\t\t\t" + attribute.getName() + " = '\" . $value . \"'\n";																
				this.content += "\t\t\t\twhere id" + entity.getName() + " = '\" . $this -> id" + entity.getName() + " . \"'\";\n";
				this.content += "\t}\n";							
			}				
		}		
	}
	private void createSelect(Entity entity) {
		this.content += "\n\tfunction select() {\n";
		this.content += "\t\treturn \"select ";
		for(Attribute attribute:entity.getAttributes()){
			this.content += attribute.getName() + ", ";					
		}			
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity()  + ", ";					
			}
		}			
		this.content = this.content.substring(0, this.content.length()-2) + "\n";
		this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
		this.content += "\t\t\t\twhere id" + entity.getName() + " = '\" . $this -> id" + entity.getName() + " . \"'\";\n";
		this.content += "\t}\n";		
	}

	private void createSelectAll(Entity entity) {
		this.content += "\n\tfunction selectAll() {\n";
		this.content += "\t\treturn \"select ";
		for(Attribute attribute:entity.getAttributes()){
			this.content += attribute.getName() + ", ";					
		}			
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";					
			}
		}			
		this.content = this.content.substring(0, this.content.length()-2) + "\n";
		this.content += "\t\t\t\tfrom " + entity.getName() + "\";\n";
		this.content += "\t}\n";		
	}

	private void createSelectAllByRelation(Entity entity) {
		for(Relation relationByEntity : entity.getRelations()) {
			if(relationByEntity.getCardinality().equals("1")) {
				this.content += "\n\tfunction selectAllBy" + relationByEntity.getEntity() + "() {\n";
				this.content += "\t\treturn \"select ";
				for(Attribute attribute:entity.getAttributes()){
					this.content += attribute.getName() + ", ";					
				}			
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";					
					}
				}			
				this.content = this.content.substring(0, this.content.length()-2) + "\n";
				this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
				this.content += "\t\t\t\twhere " + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + "_id" + relationByEntity.getEntity() + " = '\" . $this -> " + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + " . \"'\";\n";
				this.content += "\t}\n";								
			}
		}	
	}

	private void createSelectAllOrder(Entity entity) {
		this.content += "\n\tfunction selectAllOrder($orden, $dir){\n";
		this.content += "\t\treturn \"select ";
		for(Attribute attribute:entity.getAttributes()){
			this.content += attribute.getName() + ", ";					
		}			
		for(Relation relation : entity.getRelations()){
			if(relation.getCardinality().equals("1")){
				this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";					
			}		
		}
		this.content=this.content.substring(0, this.content.length()-2)+"\n";
		this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
		this.content += "\t\t\t\torder by \" . $orden . \" \" . $dir;\n";
		this.content += "\t}\n";				
	}

	private void createSelectAllByRelationOrder(Entity entity) {
		for(Relation relationByEntity : entity.getRelations()) {
			if(relationByEntity.getCardinality().equals("1")) {
				this.content += "\n\tfunction selectAllBy" + relationByEntity.getEntity() + "Order($orden, $dir) {\n";
				this.content += "\t\treturn \"select ";
				for(Attribute attribute:entity.getAttributes()){
					this.content += attribute.getName() + ", ";					
				}			
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";					
					}
				}			
				this.content = this.content.substring(0, this.content.length()-2) + "\n";
				this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
				this.content += "\t\t\t\twhere " + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + "_id" + relationByEntity.getEntity() + " = '\" . $this -> " + relationByEntity.getEntity().substring(0, 1).toLowerCase() + relationByEntity.getEntity().substring(1) + " . \"'\n";
				this.content += "\t\t\t\torder by \" . $orden . \" \" . $dir;\n";
				this.content += "\t}\n";								
			}
		}	
	}
	
	private void createSearch(Entity entity) {
		if(entity.isMenuDeploy()) {
			this.content += "\n\tfunction search($search) {\n";
			this.content += "\t\treturn \"select ";
			for(Attribute attribute:entity.getAttributes()){
				this.content += attribute.getName() + ", ";					
			}			
			for(Relation relation : entity.getRelations()){
				if(relation.getCardinality().equals("1")){
					this.content += relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + ", ";					
				}
			}			
			this.content = this.content.substring(0, this.content.length()-2) + "\n";
			this.content += "\t\t\t\tfrom " + entity.getName() + "\n";
			String conditional = "";
			for(Attribute attribute : entity.getAttributes()) {
				if(!attribute.isPrimaryKey() && !attribute.isUrl() && !attribute.getType().equals(Attribute.PASSWORD) && attribute.isVisible()) {
					conditional += attribute.getName() + " like '%\" . $search . \"%' or ";				
				}
			}
			conditional = conditional.substring(0, conditional.length()-4);
			this.content += "\t\t\t\twhere " + conditional;
			if(entity.isLog()) {
				this.content += "\n\t\t\t\torder by " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_date") + " desc, " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_time") + " desc\";\n";
			}else {
				this.content += "\";\n";
			}
			this.content += "\t}\n";							
		}
	}

	private String createConnection(String generationDirectory) throws IOException {
		this.content = "<?php\n";
		this.content +=  "class Connection {\n";
		this.content += "\tprivate $mysqli;\n";
		this.content += "\tprivate $result;\n";
		this.content += "\n\t/**\n";
		this.content += "\t * Open the conection \n";
		this.content += "\t */ \n";		
		this.content += "\tfunction open(){\n";
		this.content += "\t\t$this -> mysqli = new mysqli(\"localhost\", \"root\", \"\", \""+this.model.getAcronym().toLowerCase()+"\");\n";
		this.content += "\t\t$this -> mysqli -> set_charset(\"utf8\");\n";
		this.content += "\t}\n";
		this.content += "\n\tfunction lastId(){\n";
		this.content += "\t\treturn $this -> mysqli -> insert_id;\n";
		this.content += "\t}\n";
		this.content += "\n\tfunction run($query){\n";
		this.content += "\t\t$this -> result = $this -> mysqli -> query($query);\n";
		this.content += "\t}\n";
		this.content += "\n\tfunction close(){\n";
		this.content += "\t\t$this -> mysqli -> close();\n";
		this.content += "\t}\n";
		this.content += "\n\tfunction numRows(){\n";
		this.content += "\t\treturn ($this -> result != null)?$this -> result -> num_rows : 0;\n";
		this.content += "\t}\n";
		this.content += "\n\tfunction fetchRow(){\n";
		this.content += "\t\treturn $this -> result -> fetch_row();\n";
		this.content += "\t}\n";
		this.content += "\n\tfunction querySuccess(){\n";
		this.content += "\t\treturn $this -> result === TRUE;\n";
		this.content += "\t}\n";
		this.content += "}\n";
		this.content += "?>\n";
		return this.fileUtility.writePHP(this.content, generationDirectory + "/" + PHPGenerator.PERSISTANCE + "/" + "Connection.php");
	}

	private String createSQL(String generationDirectory) throws IOException {
		this.content = "";
		for(Entity entity : this.model.getEntities()){
			String primaryKey = "";
			this.content += "CREATE TABLE " + entity.getName() + " (\n";			
			for(Attribute attribute : entity.getAttributes()){
				if(attribute.isPrimaryKey()){
					this.content += "\t" + attribute.getName() + " int(11) NOT NULL AUTO_INCREMENT,\n";
					primaryKey+="\tPRIMARY KEY (" + attribute.getName() + ")," + "\n";
				}else{
					this.content += "\t" + attribute.getName();
					if(attribute.getType().equals(Attribute.STRING) 
							|| attribute.getType().equals(Attribute.PASSWORD) 
							|| attribute.getType().equals(Attribute.EMAIL)){
						this.content += " varchar(" + (attribute.getLength()==0?"45":attribute.getLength()) + ")";
					}else if(attribute.getType().equals(Attribute.STATE) || attribute.getType().equals(Attribute.BOOLEAN)){
						this.content += " tinyint";
					}else if(attribute.getType().equals(Attribute.INT) 
							|| attribute.getType().equals(Attribute.DATE)
							|| attribute.getType().equals(Attribute.TIME)
							|| attribute.getType().equals(Attribute.TEXT)
							|| attribute.getType().equals(Attribute.LONG_TEXT)){
						this.content += " " + attribute.getType();
					}
					if(attribute.isMandatory()){
						this.content += " NOT NULL,\n";
					}else{
						this.content += " DEFAULT NULL,\n";
					}
				}
			}
			for(Relation relation : entity.getRelations()){
				if(relation.getCardinality().equals("1")){
					this.content += "\t" + relation.getEntity().substring(0, 1).toLowerCase() + relation.getEntity().substring(1) + "_id" + relation.getEntity() + " int(11) NOT NULL,\n";
				}
			}			
			this.content += primaryKey;
			this.content = this.content.substring(0, this.content.length()-2);
			this.content += "\n);\n\n";

			if(entity.isAdmin()){
				this.content += "INSERT INTO " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator") + "(id" + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_name") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_lastName") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_email") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_password") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_phone") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_mobile") + ", " + Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_state") + ") VALUES \n";
				this.content += "\t('1', 'Admin', 'Admin', 'admin@udistrital.edu.co', md5('123'), '123', '123', '1'); \n\n";
			}
		}		
		for(Entity entity : this.model.getEntities()){
			for(Relation relation : entity.getRelations()){
				if(relation.getCardinality().equals("1")){
					this.content += "ALTER TABLE " + entity.getName() + "\n ";
					this.content += "\tADD FOREIGN KEY (" + relation.getEntity().toLowerCase() + "_id" + relation.getEntity() + ") REFERENCES " + relation.getEntity() + " (id" + relation.getEntity() + "); \n\n";
				}
			}
		}		
		return this.fileUtility.writePHP(this.content, generationDirectory + "/" + PHPGenerator.PERSISTANCE + "/" + "SQLScript.sql");
	}

	private void createDelete(Entity entity) {
		if(entity.isDelete()) {
			this.content += "\n\tfunction delete(){\n";
			this.content += "\t\treturn \"delete from " + entity.getName() + "\n";
			this.content += "\t\t\t\twhere id" + entity.getName() + " = '\" . $this -> id" + entity.getName() + " . \"'\";\n";		
			this.content += "\t}\n";							
		}
	}
}
