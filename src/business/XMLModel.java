package business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import org.xml.sax.SAXParseException;
import persistance.XMLReader;

public class XMLModel extends Observable{
	private String xmlContent;
	private String xmlPath;
	private String stream;

	public String getXmlContent() {
		return xmlContent;
	}

	public void setXmlContent(String xmlContent) {
		this.xmlContent = xmlContent;
		this.setChanged();
		this.notifyObservers(this);
	}

	public String getXmlPath() {
		return xmlPath;
	}

	public void setXmlPath(String xmlPath) {
		this.xmlPath = xmlPath;
	}

	public String getStream() {
		return stream;
	}

	@Override
	public String toString() {
		return this.xmlContent;
	}

	public boolean validateXmlModel() {
		if(this.stream != "") {
			this.stream = "";
		}
		//SchemaFactory schFact= SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			//((schFact.newSchema(new File("res/schema.xsd"))).newValidator()).validate(new StreamSource(new File(getXmlPath())));
			if(!validateSemanthic()) {
				return false;
			}
		} catch (IOException e) {
			this.stream += "Please, remember the order of the XML model. This is not a valid document and that is the reason: "+e.toString()+"\n";
			return false;
		} catch (JDOMException e) {
			this.stream += "Please, remember the order of the XML model. This is not a valid document and that is the reason: "+e.toString()+"\n";
			return false;
		} catch (SAXParseException e){
			this.stream += "The line number related with error is: "+e.getLineNumber()+"\n";
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean validateSemanthic() throws SAXParseException, JDOMException, IOException{
		String stream = "";
		ArrayList<String> relationsName = new ArrayList<String>();
		ArrayList<String> entitiesNames = new ArrayList<String>();
		entitiesNames.add("Administrator");
		entitiesNames.add("Administrador");
		XMLReader xmlReader = new XMLReader();
		Document document = xmlReader.readXML(getXmlPath());

		Element root = document.getRootElement();
		if(root.getAttributeValue(Model.LANGUAGE) != null && !(root.getAttributeValue(Model.LANGUAGE).equals("es") || root.getAttributeValue(Model.LANGUAGE).equals("en"))) {
			stream += "Error in the '"+Model.LANGUAGE+"' attribute in the "+root.getName()+" element.\n";
		}

		/**
		 * Validation of customize entity
		 * */		
		List<Element> entitiesXML = root.getChildren(Model.ENTITY);
		Map<Element, Integer> entityCounts = new HashMap<Element, Integer>();
		if(entitiesXML.size()<=0) {
			stream += "The XML model must have at least one entity.\n";
		}

		//If there is more than 1 repeated item
		boolean entitiesRepetitions = false;
		ArrayList<String> repeatedEntities = new ArrayList<String>();
		for(Element entityXML : entitiesXML) {
			if (entityCounts.containsKey(entityXML)) {
				repeatedEntities.add(entityXML.getAttributeValue(Model.NAME));
				entitiesRepetitions = true;
			} else {
				entityCounts.put(entityXML, 1);
			}
			if(entitiesRepetitions) {
				stream += "The entity(ies):";
				for (String entity : repeatedEntities) {
					stream += " " + entity;
				}
				stream += "are/is repeated in the XML model.\n";
			}
			if(entityXML.getAttributeValue(Model.NAME) != null) {
				entitiesNames.add(entityXML.getAttributeValue(Model.NAME));
				if(!Character.isUpperCase(entityXML.getAttributeValue(Model.NAME).charAt(0))) {
					stream += "The first character in the entity "+entityXML.getAttributeValue(Model.NAME)+" must be in uppercase.\n";
				}
			}else {
				stream += "The entity "+entityXML.getAttributeValue(Model.NAME)+" must have at least a name \n";
			}
			/*
			 * Attributes in entity
			 */
			if(entityXML.getAttributeValue(Model.ACTOR) !=null && !(entityXML.getAttributeValue(Model.ACTOR).equals(Model.TRUE) || entityXML.getAttributeValue(Model.ACTOR).equals(Model.FALSE))){
				stream += "Not valid value for actor in entity "+entityXML.getAttributeValue(Model.NAME)+"\n";
			}
			if(entityXML.getAttributeValue(Model.MENU_DEPLOY) !=null && !(entityXML.getAttributeValue(Model.MENU_DEPLOY).equals(Model.TRUE) || entityXML.getAttributeValue(Model.MENU_DEPLOY).equals(Model.FALSE))){
				stream += "Not valid value for menu deploy in entity "+entityXML.getAttributeValue(Model.NAME)+"\n";
			}
			if(entityXML.getAttributeValue(Model.DELETE) !=null && !(entityXML.getAttributeValue(Model.DELETE).equals(Model.TRUE) || entityXML.getAttributeValue(Model.DELETE).equals(Model.FALSE))){
				stream += "Not valid value for menu delete in entity " + entityXML.getAttributeValue(Model.NAME)+"\n";
			}
			/*
			 * List of (element) attributes inside entity
			 */
			List<Element> attributesXML=entityXML.getChildren(Model.ATTRIBUTE);
			Map<Element, Integer> attribCounts = new HashMap<Element, Integer>();
			boolean attribRepetitions = false;
			ArrayList<String> repeatedAttributes = new ArrayList<String>();
			for(Element attributeXML : attributesXML){
				if (attribCounts.containsKey(attributeXML)) {
					repeatedAttributes.add(attributeXML.getAttributeValue(Model.NAME));
					attribRepetitions = true;
				} else {
					attribCounts.put(attributeXML, 1);
				}
				if(attribRepetitions) {
					stream += "The attribute(s):";
					for (String attribute : repeatedAttributes) {
						stream += " " + attribute;
					}
					stream += "are/is repeated in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}
				/*
				 * Validation of the entity name length 
				 * Validation of name entities repeated
				 * Validation of pkey repeated 
				 */
				if(attributeXML.getAttributeValue(Model.NAME).isEmpty()) {
					stream += "The entity "+entityXML.getAttributeValue(Model.NAME)+" name must have at least one character.\n";
				}
				if(attributeXML.getAttributeValue(Model.MANDOTORY)!=null && !(attributeXML.getAttributeValue(Model.MANDOTORY).equals(Model.TRUE) || attributeXML.getAttributeValue(Model.MANDOTORY).equals(Model.FALSE))){
					stream += "Not valid value for '"+Model.MANDOTORY+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}				
				if(attributeXML.getAttributeValue(Model.TYPE)!=null 
						&& !(attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.STRING) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.EMAIL) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.TEXT) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.DATE) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.TIME) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.INT) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.BOOLEAN) 
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.PASSWORD)
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.STATE)
								|| attributeXML.getAttributeValue(Model.TYPE).equals(Attribute.LONG_TEXT))){
					stream += "Not valid value for '"+Model.TYPE+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}				
				if(attributeXML.getAttributeValue(Model.NOWRAP)!=null && !(attributeXML.getAttributeValue(Model.NOWRAP).equals(Model.TRUE) || attributeXML.getAttributeValue(Model.NOWRAP).equals(Model.FALSE))){
					stream += "Not valid value for '"+Model.NOWRAP+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}	
				if(attributeXML.getAttributeValue(Model.VISIBLE)!=null && !(attributeXML.getAttributeValue(Model.VISIBLE).equals(Model.TRUE) || attributeXML.getAttributeValue(Model.VISIBLE).equals(Model.FALSE))){
					stream += "Not valid value for '"+Model.VISIBLE+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}								
				if(attributeXML.getAttributeValue(Model.DEPLOY)!=null && !(attributeXML.getAttributeValue(Model.DEPLOY).equals(Model.TRUE) || attributeXML.getAttributeValue(Model.DEPLOY).equals(Model.FALSE))){
					stream += "Not valid value for '"+Model.DEPLOY+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}								
				if(attributeXML.getAttributeValue(Model.LENGTH)!=null && attributeXML.getAttributeValue(Model.LENGTH).length()>4){
					stream += "The '"+attributeXML.getAttributeValue(Model.LENGTH)+"' has exceeded the maximum length\n";
				}												
				if(attributeXML.getAttributeValue(Model.URL)!=null && !(attributeXML.getAttributeValue(Model.URL).equals(Model.TRUE) || attributeXML.getAttributeValue(Model.URL).equals(Model.FALSE))){
					stream += "Not valid value for '"+Model.URL+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}												
				if(attributeXML.getAttributeValue(Model.IMAGE)!=null && !(attributeXML.getAttributeValue(Model.IMAGE).equals(Model.TRUE) || attributeXML.getAttributeValue(Model.IMAGE).equals(Model.FALSE))){
					stream += "Not valid value for '"+Model.IMAGE+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}
				if(attributeXML.getAttributeValue(Model.ORDER)!=null && !(attributeXML.getAttributeValue(Model.ORDER).equals(Model.ASC) || attributeXML.getAttributeValue(Model.ORDER).equals(Model.DESC))){
					stream += "Not valid value for '"+Model.ORDER+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}
			}
			List<Element> relationsXML = entityXML.getChildren(Model.RELATION);
			Map<Element, Integer> relationsCounts = new HashMap<Element, Integer>();
			boolean relationRepetitions = false;
			ArrayList<String> repeatedRelations = new ArrayList<String>();
			for(Element relationXML : relationsXML){
				if (relationsCounts.containsKey(relationXML)) {
					repeatedRelations.add(relationXML.getAttributeValue(Model.ENTITY));
					relationRepetitions = true;
				} else {
					attribCounts.put(relationXML, 1);
				}
				if(relationRepetitions) {
					stream += "The relation(s):";
					for (String relation : repeatedRelations) {
						stream += " " + relation;
					}
					stream += "are/is repeated in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}
				if(relationXML.getAttributeValue(Model.ENTITY) != null) {
					if(!Character.isUpperCase(relationXML.getAttributeValue(Model.ENTITY).charAt(0))) {
						stream += "The first character in the entity "+entityXML.getAttributeValue(Model.NAME)+" relation must be in uppercase.\n";
					}else {
						relationsName.add(relationXML.getAttributeValue(Model.ENTITY));
					}
				}else {
					stream += "The relation "+relationXML.getAttributeValue(Model.ENTITY)+" in entity "+entityXML.getAttributeValue(Model.NAME)+" must have atleast entity name. \n";
				}
				if(relationXML.getAttributeValue(Model.CARDINALITY).length()>2) {
					stream += "The cardinality value must be '*' or '1' in entity: "+entityXML.getAttributeValue(Model.NAME)+". \n";
				}else {
					if(!(relationXML.getAttributeValue(Model.CARDINALITY).charAt(0) == '*' || relationXML.getAttributeValue(Model.CARDINALITY).charAt(0) == '1')) {
						stream += "The cardinality value must be '*' in entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
					}
				}
			}
			List<Element> servicesXML = entityXML.getChildren(Model.SERVICE);
			Map<Element, Integer> servicesCounts = new HashMap<Element, Integer>();
			boolean servicesRepetitions = false;
			ArrayList<String> repeatedServices = new ArrayList<String>();
			for(Element serviceXML : servicesXML){
				if (servicesCounts.containsKey(serviceXML)) {
					repeatedServices.add(serviceXML.getAttributeValue(Model.ENTITY));
					servicesRepetitions = true;
				} else {
					attribCounts.put(serviceXML, 1);
				}
				if(servicesRepetitions) {
					stream += "The service(s):";
					for (String service : repeatedServices) {
						stream += " " + service;
					}
					stream += "are/is repeated in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
				}
				if(entityXML.getAttributeValue(Model.ACTOR) ==null) {
					stream += "This entity "+entityXML.getAttributeValue(Model.NAME)+" is not an actor. \n";
				}else {
					if(serviceXML.getAttributeValue(Model.CREATE) != null && !(serviceXML.getAttributeValue(Model.CREATE).equals(Model.TRUE) || serviceXML.getAttributeValue(Model.CREATE).equals(Model.FALSE))){
						stream += "Not valid value for '"+Model.CREATE+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
					}				
					if(serviceXML.getAttributeValue(Model.GET) != null && !(serviceXML.getAttributeValue(Model.GET).equals(Model.TRUE) || serviceXML.getAttributeValue(Model.GET).equals(Model.FALSE))){
						stream += "Not valid value for '"+Model.GET+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
					}				
					if(serviceXML.getAttributeValue(Model.EDIT) != null && !(serviceXML.getAttributeValue(Model.EDIT).equals(Model.TRUE) || serviceXML.getAttributeValue(Model.EDIT).equals(Model.FALSE))){
						stream += "Not valid value for '"+Model.EDIT+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
					}				
					if(serviceXML.getAttributeValue(Model.DELETE) != null && !(serviceXML.getAttributeValue(Model.DELETE).equals(Model.TRUE) || serviceXML.getAttributeValue(Model.DELETE).equals(Model.FALSE))){
						stream += "Not valid value for '"+Model.DELETE+"' property in the entity: "+entityXML.getAttributeValue(Model.NAME)+".\n";
					}
				}							
			}
		}
		/**
		 * Valid repetitions relations
		 * get more atribs
		 * */
		for(String relation : relationsName) {
			if(!entitiesNames.contains(relation)) {
				stream += "The relation: "+relation+" does not belongs to any entity";
			}
		}
		if(stream == "") {
			stream += "No problem, this is a valid document.";
			this.stream = stream;
			return true;
		}
		this.stream = stream;
		return false;
	}
}
