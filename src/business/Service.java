package business;

public class Service {
	private String entity;
	private boolean create;
	private boolean get;
	private boolean edit;
	private boolean delete;
	
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public boolean isCreate() {
		return create;
	}
	public void setCreate(boolean create) {
		this.create = create;
	}
	public boolean isGet() {
		return get;
	}
	public void setGet(boolean get) {
		this.get = get;
	}
	public boolean isEdit() {
		return edit;
	}
	public void setEdit(boolean edit) {
		this.edit = edit;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public Service(String entity) {
		super();
		this.entity = entity;
	}
	
}
