package business;

import java.io.IOException;

import persistance.FileUtility;

public class UMLGenerator {

	private Model model;
	private String content;
	private FileUtility fileUtility = new FileUtility();
	
	public UMLGenerator(Model model) {
		this.model = model;
	}

	public String createUML(String generationDirectory) throws IOException {
		this.content = "";
		this.content += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		this.content += "<uml:Model xmi:version=\"20131001\" xmlns:xmi=\"http://www.omg.org/spec/XMI/20131001\" xmlns:uml=\"http://www.eclipse.org/uml2/5.0.0/UML\" xmi:id=\"_" + this.model.hashCode() + "\" name=\"" + this.model.getAcronym() + "\">\n";		
		this.content += this.creaetComponentDiagram();
		this.content += this.createClassDiagram();
		this.content += this.createUseCaseDiagram();
		this.content += "</uml:Model>";
		return this.fileUtility.writePHP(content, generationDirectory + "/" + PHPGenerator.UML + "/model.uml");
	}

	private String creaetComponentDiagram() {
		this.content = "";
		this.content += "\t<packagedElement xmi:type=\"uml:Component\" xmi:id=\"_bootstrap\" name=\"Bootstrap\">\n";
		this.content += "\t\t<packagedElement xmi:type=\"uml:Component\" xmi:id=\"_bootstrapeditor\" name=\"Bootstrap Editor\"/>\n";		
		this.content += "\t</packagedElement>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Component\" xmi:id=\"_jquery\" name=\"JQuery\">\n";
		this.content += "\t</packagedElement>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Component\" xmi:id=\"_" + this.model.hashCode() + "\" name=\"" + this.model.getAcronym() + "\">\n";
		this.content += "\t</packagedElement>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Component\" xmi:id=\"_odbc\" name=\"MySQL ODBC\"/>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Dependency\" xmi:id=\"_" + Math.random() + "\" client=\"_bootstrap\" supplier=\"_jquery\"/>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Dependency\" xmi:id=\"_" + Math.random() + "\" client=\"_" + this.model.hashCode() + "\" supplier=\"_jquery\"/>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Dependency\" xmi:id=\"_" + Math.random() + "\" client=\"_" + this.model.hashCode() + "\" supplier=\"_bootstrap\"/>\n";
		this.content += "\t<packagedElement xmi:type=\"uml:Dependency\" xmi:id=\"_" + Math.random() + "\" client=\"_" + this.model.hashCode() + "\" supplier=\"_odbc\"/>\n";				
		return this.content;
	}

	private String createClassDiagram() throws IOException {
		this.content = "";
		//Create Classes for Data Types
		this.content += "\t<packagedElement xmi:type=\"uml:Class\" xmi:id=\"_" + Attribute.STRING.hashCode() + "\" name=\"String\"/>\n";				
		this.content += "\t<packagedElement xmi:type=\"uml:Class\" xmi:id=\"_" + Attribute.INT.hashCode() + "\" name=\"Int\"/>\n";				
		this.content += "\t<packagedElement xmi:type=\"uml:Class\" xmi:id=\"_" + Attribute.DATE.hashCode() + "\" name=\"Date\"/>\n";				
		this.content += "\t<packagedElement xmi:type=\"uml:Class\" xmi:id=\"_" + Attribute.TIME.hashCode() + "\" name=\"Time\"/>\n";				
		//Create Classes for Entities
		for(Entity entity : this.model.getEntities()){
			this.content += "\t<packagedElement xmi:type=\"uml:Class\" xmi:id=\"_" + entity.hashCode() + "\" name=\"" + entity.getName() + "\">\n";				
			for(Attribute attribute : entity.getAttributes()){
				this.content += "\t\t<ownedAttribute xmi:id=\"" + attribute.hashCode() + "\" name=\"" + attribute.getName() + "\" visibility=\"private\" type=";
				if(attribute.getType().equals(Attribute.INT)){
					this.content += "\"_" + Attribute.INT.hashCode() + "\"";
				}else if(attribute.getType().equals(Attribute.DATE)){
					this.content += "\"_" + Attribute.DATE.hashCode() + "\"";
				}else if(attribute.getType().equals(Attribute.TIME)){
					this.content += "\"_" + Attribute.TIME.hashCode() + "\"";
				}else{
					this.content += "\"_" + Attribute.STRING.hashCode() + "\"";
				}
				this.content += ">\n";
				this.content += "\t\t\t<lowerValue xmi:type=\"uml:LiteralInteger\" xmi:id=\"_" + attribute.hashCode() + "lv\" value=";
				if(attribute.isMandatory()){
					this.content += "\"1\"";					
				}else{
					this.content += "\"0\"";
				}
				this.content += "/>\n";
				this.content += "\t\t\t<upperValue xmi:type=\"uml:LiteralUnlimitedNatural\" xmi:id=\"_" + attribute.hashCode() + "uv\" value=\"1\"/>\n";
				this.content += "\t\t</ownedAttribute>\n"; 
			}
			//Methods
			//Insert in all classes
			this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"insert\"/>\n";
			if(!entity.isLog()){
				//Other methods in all classes except log
				this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"update\"/>\n";
				this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"select\"/>\n";
				
				this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"selectAll\">\n";
				this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"" + entity.getName() +"s\" type=\"_" + entity.hashCode() + "\" direction=\"return\"/>\n";
				this.content += "\t\t</ownedOperation>\n";
				this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"selectAllOrder\">\n";
				this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"order\" type=\"_" + Attribute.STRING.hashCode() + "\" />\n";
				this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"dir\" type=\"_" + Attribute.STRING.hashCode() + "\" />\n";
				this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"" + entity.getName() + "s\" type=\"_" + entity.hashCode() + "\" direction=\"return\"/>\n";
				this.content += "\t\t</ownedOperation>\n";				
				//Methods by relations
				for(Relation relation : entity.getRelations()){
					if(relation.getCardinality().equals("1")){
						Entity entityByRelation = this.model.findEntity(relation.getEntity());
						if(!entityByRelation.isLog()){
							this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"selectAllBy" + relation.getEntity() + "\">\n";
							this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"" + entity.getName() + "s\" type=\"_" + entity.hashCode() + "\" direction=\"return\"/>\n";
							this.content += "\t\t</ownedOperation>\n";												
							this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"selectAllBy" + relation.getEntity() + "Order\">\n";
							this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"order\" type=\"_" + Attribute.STRING.hashCode() + "\" />\n";
							this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"dir\" type=\"_" + Attribute.STRING.hashCode() + "\" />\n";
							this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"" + entity.getName() + "s\" type=\"_" + entity.hashCode() + "\" direction=\"return\"/>\n";
							this.content += "\t\t</ownedOperation>\n";						
						}
					}
				}
			}			
			//Search in all classes
			this.content += "\t\t<ownedOperation xmi:id=\"_" + Math.random() + "\" name=\"search\">\n";
			this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"search\" type=\"_" + Attribute.STRING.hashCode() + "\" />\n";
			this.content += "\t\t\t<ownedParameter xmi:id=\"_" + Math.random() + "\" name=\"" + entity.getName() + "s\" type=\"_" + entity.hashCode() + "\" direction=\"return\"/>\n";
			this.content += "\t\t</ownedOperation>\n";			
			this.content += "\t</packagedElement>\n";
		}
		//Relations between classes
		for(Entity entity : this.model.getEntities()){			
			for(Relation relation : entity.getRelations()){
				if(relation.getCardinality().equals("*")){
					Entity entityByRelation = this.model.findEntity(relation.getEntity());
					this.content += "\t<packagedElement xmi:type=\"uml:Dependency\" xmi:id=\"_" + relation.hashCode() + "\" name=\"id" + relation.getEntity() + "\" visibility=\"private\" client=\"_" + entity.hashCode() + "\" supplier=\"_" + entityByRelation.hashCode() + "\"/>\n";					
				}
			}			
		}
		return this.content;
	}

	private String createUseCaseDiagram() {
		this.content = "";
		//Actors
		for(Entity entity : this.model.getEntities()){
			if(entity.isActor()){
				this.content += "\t<packagedElement xmi:type=\"uml:Actor\" xmi:id=\"_" + entity.hashCode() + "actor\" name=\"" + entity.getName() + "\">\n";
				this.content += "\t\t<eAnnotations xmi:id=\"_" + Math.random() + "\" source=\"http://www.eclipse.org/uml2/2.0.0/UML\">\n";
				this.content += "\t\t\t<details xmi:id=\"_" + Math.random() + "\" key=\"Human\"/>\n";
				this.content += "\t\t</eAnnotations>\n";
				this.content += "\t</packagedElement>\n";
			}			
		}
		//Use Cases 
		for(Entity entity : this.model.getEntities()){			
			this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseManage\" name=\"Manage " + entity.getName() + "\"/>\n";
			this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseInsert\" name=\"Insert " + entity.getName() + "\">\n";
			this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
			if(entity.isLog()){
				for(Entity entity4Log : this.model.getEntities()){
					if(entity != entity4Log){
						if(!entity4Log.isLog()){
							this.content += "\t\t<extend xmi:id=\"_" + Math.random() + "\" extendedCase=\"_" + entity4Log.hashCode() + "useCaseInsert\"/>\n";
							this.content += "\t\t<extend xmi:id=\"_" + Math.random() + "\" extendedCase=\"_" + entity4Log.hashCode() + "useCaseUpdate\"/>\n";							
						}
					}
				}
			}
			this.content += "\t</packagedElement>\n";
			if(!entity.isLog()){
				this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseSelect\" name=\"Select " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
				this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
				this.content += "\t</packagedElement>\n";
				this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseSelectAll\" name=\"Select All " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
				this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
				this.content += "\t</packagedElement>\n";
				this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseSelectAllOrder\" name=\"Select All Order " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
				this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
				this.content += "\t\t<extend xmi:id=\"_" + Math.random() + "\" extendedCase=\"_" + entity.hashCode() + "useCaseSelectAll\"/>\n";
				this.content += "\t</packagedElement>\n";				
			}
			this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseSearch\" name=\"Search " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
			this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
			this.content += "\t</packagedElement>\n";
			if(!entity.isLog()){
				this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseUpdate\" name=\"Update " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
				this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
				this.content += "\t\t<include xmi:id=\"_" + Math.random() + "\" addition=\"_" + entity.hashCode() + "useCaseSelect\"/>\n";
				this.content += "\t\t<extend xmi:id=\"_" + Math.random() + "\" extendedCase=\"_" + entity.hashCode() + "useCaseSelectAll\"/>\n";
				this.content += "\t\t<extend xmi:id=\"_" + Math.random() + "\" extendedCase=\"_" + entity.hashCode() + "useCaseSelectAllOrder\"/>\n";
				this.content += "\t\t<extend xmi:id=\"_" + Math.random() + "\" extendedCase=\"_" + entity.hashCode() + "useCaseSearch\"/>\n";
				this.content += "\t</packagedElement>\n";	
			}
			if(entity.isActor()) {
				this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseEditProfile\" name=\"Edit Profile " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
				this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
				this.content += "\t</packagedElement>\n";
				this.content += "\t<packagedElement xmi:type=\"uml:UseCase\" xmi:id=\"_" + entity.hashCode() + "useCaseUpdatePassword\" name=\"Update Password " + Utilities.replaceCapitalLetter(entity.getName()) + "\">\n";
				this.content += "\t\t<generalization xmi:id=\"_" + Math.random() + "\" general=\"_" + entity.hashCode() + "useCaseManage\"/>\n";
				this.content += "\t</packagedElement>\n";				
				this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_" + entity.hashCode() + "actor_" + entity.hashCode() + "useCaseEditProfile\" memberEnd=\"_" + entity.hashCode() + "actorSource _" + entity.hashCode() + "useCaseEditProfileTarget\">\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "actorSource\" type=\"_" + entity.hashCode() + "actor\" association=\"_" + entity.hashCode() + "actor_" + entity.hashCode() + "useCaseEditProfile\"/>\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "useCaseEditProfileTarget\" type=\"_" + entity.hashCode() + "useCaseEditProfile\" association=\"_" + entity.hashCode() + "actor_" + entity.hashCode() + "useCaseEditProfile\"/>\n";
				this.content += "\t</packagedElement>\n";		  				
				this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_" + entity.hashCode() + "actor_" + entity.hashCode() + "useCaseUpdatePassword\" memberEnd=\"_" + entity.hashCode() + "actorSource _" + entity.hashCode() + "useCaseUpdatePasswordTarget\">\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "actorSource\" type=\"_" + entity.hashCode() + "actor\" association=\"_" + entity.hashCode() + "actor_" + entity.hashCode() + "useCaseUpdatePassword\"/>\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "useCaseUpdatePasswordTarget\" type=\"_" + entity.hashCode() + "useCaseUpdatePassword\" association=\"_" + entity.hashCode() + "actor_" + entity.hashCode() + "useCaseUpdatePassword\"/>\n";
				this.content += "\t</packagedElement>\n";		  				
			}
			
			Entity administrador = this.model.findEntity(Dictionary.getInstance().getVocabulary().get(this.model.getLanguage() + "_Administrator"));
			//Association between administrator and use cases
			if(!entity.isLog()){
				this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseInsert\" memberEnd=\"_" + administrador.hashCode() + "actorSource _" + entity.hashCode() + "useCaseInsertTarget\">\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + administrador.hashCode() + "actorSource\" type=\"_" + administrador.hashCode() + "actor\" association=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseInsert\"/>\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "useCaseInsertTarget\" type=\"_" + entity.hashCode() + "useCaseInsert\" association=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseInsert\"/>\n";
				this.content += "\t</packagedElement>\n";		  
				this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseSelectAll\" memberEnd=\"_" + administrador.hashCode() + "actorSource _" + entity.hashCode() + "useCaseSelectAllTarget\">\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + administrador.hashCode() + "actorSource\" type=\"_" + administrador.hashCode() + "actor\" association=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseSelectAll\"/>\n";
				this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "useCaseSelectAllTarget\" type=\"_" + entity.hashCode() + "useCaseSelectAll\" association=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseSelectAll\"/>\n";
				this.content += "\t</packagedElement>\n";		  
			}
			this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseSearch\" memberEnd=\"_" + administrador.hashCode() + "actorSource _" + entity.hashCode() + "useCaseSearchTarget\">\n";
			this.content += "\t\t<ownedEnd xmi:id=\"_" + administrador.hashCode() + "actorSource\" type=\"_" + administrador.hashCode() + "actor\" association=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseSearch\"/>\n";
			this.content += "\t\t<ownedEnd xmi:id=\"_" + entity.hashCode() + "useCaseSearchTarget\" type=\"_" + entity.hashCode() + "useCaseSearch\" association=\"_" + administrador.hashCode() + "actor_" + entity.hashCode() + "useCaseSearch\"/>\n";
			this.content += "\t</packagedElement>\n";		  				
		}
		
		//Agrega asociaciones entre actores y servicios de entidades
//		ArrayList<Entity> actores = this.model.buscarActores();
//		for(Entity actor : actores){
//			for(Rol rol : actor.getRoles()){
//				for(Servicio servicio : rol.getServicios()){
//					Entity entidadServicio = this.model.buscarEntity(servicio.getEntity());
//					if(servicio.isIngresar()){
//						this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseInsert\" memberEnd=\"_"+actor.hashCode() + "actorSource_"+entidadServicio.hashCode() + "useCaseInsertTarget\">\n";
//						this.content += "\t\t<ownedEnd xmi:id=\"_"+actor.hashCode() + "actorSource\" type=\"_"+actor.hashCode() + "actor\" association=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseInsert\"/>\n";
//						this.content += "\t\t<ownedEnd xmi:id=\"_"+entidadServicio.hashCode() + "useCaseInsertTarget\" type=\"_"+entidadServicio.hashCode() + "useCaseInsert\" association=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseInsert\"/>\n";
//						this.content += "\t</packagedElement>\n";						
//					}
//					if(servicio.isConsultar()){
//						this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseSelectAll\" memberEnd=\"_"+actor.hashCode() + "actorSource_"+entidadServicio.hashCode() + "useCaseSelectAllTarget\">\n";
//						this.content += "\t\t<ownedEnd xmi:id=\"_"+actor.hashCode() + "actorSource\" type=\"_"+actor.hashCode() + "actor\" association=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseSelectAll\"/>\n";
//						this.content += "\t\t<ownedEnd xmi:id=\"_"+entidadServicio.hashCode() + "useCaseSelectAllTarget\" type=\"_"+entidadServicio.hashCode() + "useCaseSelectAll\" association=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseSelectAll\"/>\n";
//						this.content += "\t</packagedElement>\n";						
//						this.content += "\t<packagedElement xmi:type=\"uml:Association\" xmi:id=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseSearch\" memberEnd=\"_"+actor.hashCode() + "actorSource_"+entidadServicio.hashCode() + "useCaseSearchTarget\">\n";
//						this.content += "\t\t<ownedEnd xmi:id=\"_"+actor.hashCode() + "actorSource\" type=\"_"+actor.hashCode() + "actor\" association=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseSearch\"/>\n";
//						this.content += "\t\t<ownedEnd xmi:id=\"_"+entidadServicio.hashCode() + "useCaseSearchTarget\" type=\"_"+entidadServicio.hashCode() + "useCaseSearch\" association=\"_"+actor.hashCode() + "actor_"+entidadServicio.hashCode() + "useCaseSearch\"/>\n";
//						this.content += "\t</packagedElement>\n";						
//					}
//				}
//			}
//			
//		}
//		
		return this.content;
	}
}
