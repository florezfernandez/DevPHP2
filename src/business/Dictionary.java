package business;

import java.util.HashMap;
import java.util.Map;

public class Dictionary {
	private static final Dictionary INSTANCE = new Dictionary();
	private Map<String, String> vocabulary = new HashMap<String, String>();
	
	public void init() {
		vocabulary.put("en_action", "action");
		vocabulary.put("es_action", "accion");
		vocabulary.put("en_Administrator", "Administrator");
		vocabulary.put("es_Administrator", "Administrador");
		vocabulary.put("en_browser", "browser");		
		vocabulary.put("es_browser", "explorador");	
		vocabulary.put("en_Create", "Create");
		vocabulary.put("es_Create", "Crear");
		vocabulary.put("en_ConfirmPassword", "Confirm Password");
		vocabulary.put("es_ConfirmPassword", "Confirmación de Clave");
		vocabulary.put("en_CurrentPassword", "Current Password");
		vocabulary.put("es_CurrentPassword", "Clave Actual");
		vocabulary.put("en_date", "date");
		vocabulary.put("es_date", "fecha");
		vocabulary.put("en_Disabled", "Disabled");
		vocabulary.put("es_Disabled", "Deshabilitado");
		vocabulary.put("en_Edit", "Edit");
		vocabulary.put("es_Edit", "Editar");
		vocabulary.put("en_Delete", "Edit");
		vocabulary.put("es_Delete", "Eliminar");
		vocabulary.put("en_EditPassword", "Edit Password");
		vocabulary.put("es_EditPassword", "Editar Clave");
		vocabulary.put("en_Enabled", "Enabled");
		vocabulary.put("es_Enabled", "Habilitado");
		vocabulary.put("en_Email", "Email");
		vocabulary.put("es_Email", "Correo");
		vocabulary.put("en_email", "email");
		vocabulary.put("es_email", "correo");
		vocabulary.put("en_False", "False");
		vocabulary.put("es_False", "Falso");
		vocabulary.put("en_GetAll", "Get All");
		vocabulary.put("es_GetAll", "Consultar");
		vocabulary.put("en_information", "information");
		vocabulary.put("es_information", "informacion");
		vocabulary.put("en_ip", "ip");		
		vocabulary.put("es_ip", "ip");		
		vocabulary.put("en_lastName", "lastName");		
		vocabulary.put("es_lastName", "apellido");		
		vocabulary.put("en_location", "location");		
		vocabulary.put("es_location", "ubicacion");		
		vocabulary.put("en_LogIn", "Log In");
		vocabulary.put("es_LogIn", "Autenticar");
		vocabulary.put("en_LogOut", "Log Out");
		vocabulary.put("es_LogOut", "Salir");
		vocabulary.put("en_mobile", "mobile");
		vocabulary.put("es_mobile", "celular");
		vocabulary.put("en_NewPassword", "New Password");		
		vocabulary.put("es_NewPassword", "Nueva Clave");		
		vocabulary.put("en_name", "name");		
		vocabulary.put("es_name", "nombre");		
		vocabulary.put("en_of", "of");		
		vocabulary.put("es_of", "de");		
		vocabulary.put("en_os", "os");		
		vocabulary.put("es_os", "so");		
		vocabulary.put("en_Password", "Password");
		vocabulary.put("es_Password", "Clave");
		vocabulary.put("en_password", "password");
		vocabulary.put("es_password", "clave");
		vocabulary.put("en_phone", "phone");
		vocabulary.put("es_phone", "telefono");
		vocabulary.put("en_Picture", "Picture");
		vocabulary.put("es_Picture", "Foto");
		vocabulary.put("en_picture", "picture");
		vocabulary.put("es_picture", "foto");
		vocabulary.put("en_Profile", "Profile");
		vocabulary.put("es_Profile", "Perfil");		
		vocabulary.put("en_RecoverPassword", "Recover Password");
		vocabulary.put("es_RecoverPassword", "Recuperar Clave");
		vocabulary.put("en_True", "True");
		vocabulary.put("es_True", "Verdadero");
		
		vocabulary.put("en_Search", "Search");
		vocabulary.put("es_Search", "Buscar");
		vocabulary.put("es_Select", "Seleccione");
		vocabulary.put("en_Select", "Select");
		vocabulary.put("en_State", "State");
		vocabulary.put("es_State", "Estado");
		vocabulary.put("en_state", "state");
		vocabulary.put("es_state", "estado");
		vocabulary.put("en_time", "time");
		vocabulary.put("es_time", "hora");		
				
		//Alerts
		vocabulary.put("en_Alert1", "Wrong email or password");
		vocabulary.put("es_Alert1", "Error de correo o clave");
		vocabulary.put("en_Alert2", "User disabled");
		vocabulary.put("es_Alert2", "Usuario Deshabilitado");
		vocabulary.put("en_Alert3", "Data Entered");
		vocabulary.put("es_Alert3", "Datos Ingresados");
		vocabulary.put("en_Alert4", "Data Edited");
		vocabulary.put("es_Alert4", "Datos Editados");
		vocabulary.put("en_Alert5", "Image Edited");
		vocabulary.put("es_Alert5", "Imagen Editada");
		vocabulary.put("en_Alert6", "Password Edited");
		vocabulary.put("es_Alert6", "Clave Editada");
		vocabulary.put("en_Alert7", "Error. <em>New Password</em> and <em>Confirm Password</em> fields must be equal");
		vocabulary.put("es_Alert7", "Error. Los campos <em>Nueva Clave</em> y <em>Confirmación de Clave</em> deben ser iguales");
		vocabulary.put("en_Alert8", "Error. <em>Current Password</em> is wrong");
		vocabulary.put("es_Alert8", "Error. La <em>Clave Actual</em> es incorrecta");
		vocabulary.put("en_Alert9", "File Edited");
		vocabulary.put("es_Alert9", "Archivo Editado");
		vocabulary.put("en_Alert10", "The image must be png, jpg or jpeg");
		vocabulary.put("es_Alert10", "La imagen debe ser png, jpg o jpeg");
		vocabulary.put("en_Alert11", "The file must be pdf");
		vocabulary.put("es_Alert11", "La archivo debe ser pdf");
		
		//Tags
		vocabulary.put("en_Tag1", "Your role is");
		vocabulary.put("es_Tag1", "Su rol es");
		vocabulary.put("en_Tag2", "View more information");
		vocabulary.put("es_Tag2", "Ver mas información");
		vocabulary.put("en_Tag3", "If the email");
		vocabulary.put("es_Tag3", "Si el correo");
		vocabulary.put("en_Tag4", "was found in the system, a new password was sent");
		vocabulary.put("es_Tag4", "fue encontrado en el sistema, una nueva clave fue enviada");
		vocabulary.put("en_Tag5", "Password recovery for");
		vocabulary.put("es_Tag5", "Recuperación de clave para");
		vocabulary.put("en_Tag6", "Your new password is");
		vocabulary.put("es_Tag6", "Su nueva clave es");
		vocabulary.put("en_Tag7", "Sort Ascending");
		vocabulary.put("es_Tag7", "Ordenar Ascendente");
		vocabulary.put("en_Tag8", "Sort Descending");
		vocabulary.put("es_Tag8", "Ordenar Descendente");
		vocabulary.put("en_Tag9", "Confirm to delete");
		vocabulary.put("es_Tag9", "Confirma eliminar");
	}
	
	public static Dictionary getInstance() {
		return INSTANCE;
	}

	public Map<String, String> getVocabulary() {
		return vocabulary;
	}
	
}
