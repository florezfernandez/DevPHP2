package business;

public class Attribute {
	
	public final static String STRING="string";
	public final static String INT="int";
	public final static String BOOLEAN="boolean";
	public final static String PASSWORD="password";
	public final static String TEXT="text";
	public final static String DATE="date";
	public final static String TIME="time";
	public final static String EMAIL="email";
	public final static String STATE="state";
	public final static String LONG_TEXT="longtext";
	
	private String name;
	private String type;
	private boolean primaryKey;
	private boolean mandatory;
	private boolean nowrap;
	private boolean visible;
	private String order;
	private boolean deploy;
	private int length;
	private boolean url;
	private boolean image;
	private boolean file;
	private String info;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean isNowrap() {
		return nowrap;
	}

	public void setNowrap(boolean nowrap) {
		this.nowrap = nowrap;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public boolean isDeploy() {
		return deploy;
	}

	public void setDeploy(boolean deploy) {
		this.deploy = deploy;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isUrl() {
		return url;
	}

	public void setUrl(boolean url) {
		this.url = url;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}

	public boolean isFile() {
		return file;
	}

	public void setFile(boolean file) {
		this.file = file;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Attribute(String name) {
		this.name = name;
	}
}
