package business;

import java.io.IOException;
import java.util.Observable;

import persistance.FileUtility;


public class DevPHP extends Observable {

	private String generationDirectory;
	private String consoleStream;
	private XMLModel xmlModel = new XMLModel();


	public String getGenerationDirectory() {
		return generationDirectory;
	}

	public void setGenerationDirectory(String generationDirectory) {
		this.generationDirectory = generationDirectory;
		this.consoleStream = "Generation Directory: " + this.generationDirectory+"\n";
		this.setChanged();
		this.notifyObservers(this);	
	}

	public XMLModel getXMLModel() {
		return xmlModel;
	}

	/**
	 * Open the XML model
	 * @param path: Path of the XML file
	 * @throws IOException: The file of the path does not exist 
	 */
	public void openXMLModel(String path) throws IOException {
		this.xmlModel.setXmlPath(path);
		FileUtility fileUtility = new FileUtility();
		this.xmlModel.setXmlContent(fileUtility.openFile(path));
		this.consoleStream = "XML File: " + path + " loaded\n";
		this.setChanged();
		this.notifyObservers(this);		
	}

	/**
	 * Generate the PHP project
	 * @throws Exception 
	 */
	public void generateProject() throws Exception {
		if(this.xmlModel.validateXmlModel()) {
			PHPGenerator phpGenerator = new PHPGenerator();
			this.consoleStream = phpGenerator.generatePHP(this.generationDirectory, this.xmlModel.getXmlPath());
		}else {
			this.consoleStream = this.xmlModel.getStream();
		}
		this.setChanged();
		this.notifyObservers(this);	
	}
	
	/**
	 * Validate the XML model
	 */
	public void validateXmlModel() {
		/*
		 * Execute method validate model
		 * Then get the stream resulted by last method
		 * */
		this.xmlModel.validateXmlModel();
		this.consoleStream = this.xmlModel.getStream();
		this.setChanged();
		this.notifyObservers(this);	
	}
	
	@Override
	public String toString() {		
		return this.consoleStream;
	}
}
