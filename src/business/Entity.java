package business;

import java.util.ArrayList;

public class Entity {
	private String name;
	private boolean actor;
	private boolean admin;
	private boolean log;
	private boolean menuDeploy;
	private boolean delete;
	private boolean extensible;
	
	private ArrayList<Attribute> attributes = new ArrayList<Attribute>();
	private ArrayList<Relation> relations = new ArrayList<Relation>();
	private ArrayList<Service> services = new ArrayList<Service>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActor() {
		return actor;
	}

	public void setActor(boolean actor) {
		this.actor = actor;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isLog() {
		return log;
	}

	public void setLog(boolean log) {
		this.log = log;
	}

	public boolean isMenuDeploy() {
		return menuDeploy;
	}

	public void setMenuDeploy(boolean menuDeploy) {
		this.menuDeploy = menuDeploy;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
	public boolean isExtensible() {
		return extensible;
	}

	public void setExtensible(boolean extensible) {
		this.extensible = extensible;
	}

	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

	public ArrayList<Relation> getRelations() {
		return relations;
	}
	
	public ArrayList<Service> getServices() {
		return services;
	}

	public Entity(String name) {
		this.name = name;
	}

	public void addAttribute(Attribute attribute) {
		this.attributes.add(attribute);
	}

	public void addRelation(Relation relation) {
		this.relations.add(relation);		
	}
	
	public void addService(Service service) {
		this.services.add(service);		
	}
	
	public boolean hasCreateService() {
		for(Service service : this.services) {
			if(service.isCreate()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasGetService() {
		for(Service service : this.services) {
			if(service.isGet()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasService(String entityName, String serviceName) {
		for(Service service : this.services) {
			if(service.getEntity().equals(entityName)) {
				if(serviceName.equals(Model.CREATE) && service.isCreate()
				|| serviceName.equals(Model.GET) && service.isGet()
				|| serviceName.equals(Model.EDIT) && service.isEdit()
				|| serviceName.equals(Model.DELETE) && service.isDelete()) {
					return true;
				}
			}
		}
		return false;
	}
	
}
