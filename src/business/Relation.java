package business;

public class Relation {
	private String cardinality;
	private String entity;
	
	public String getCardinality() {
		return cardinality;
	}
	
	public void setCardinality(String cardinality) {
		this.cardinality = cardinality;
	}
	
	public String getEntity() {
		return entity;
	}
	
	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Relation(String cardinality, String entity) {
		this.cardinality = cardinality;
		this.entity = entity;
	}
}
