package persistance;

import java.io.IOException;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class XMLReader {

	public Document readXML(String xmlPath) throws JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder(false); 				
		return builder.build(xmlPath);
	}
}
