package persistance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtility {
	
	/**
	 * Open a file and read its content
	 * @param path: Path of the file to read
	 * @return Content of the file
	 * @throws IOException: The file of the path does not exist 
	 */
	public String openFile(String path) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String line;
		String stream="";
		while ((line=br.readLine())!=null){
			stream+=line+"\n";
		}
		br.close();
		return stream;
	}
	
	/**
	 * Create a directory
	 * @param directory: Name of the directory to be created
	 * @return Success message if the directory was created or failure message if the directory was not created because it already exists 
	 */
	public String createDirectory(String directory) {
		String path = directory.replace("\\", "/");
		File file = new File(path);
        if (!file.exists()) {
            if (file.mkdir()) {
                return "Directory created: " + path + "\n";
            }
        }	
        return "Directory not created: " + path + "\n";
	}

	/**
	 * Copy files from DevPHP to the generated project
	 * @param sourcePath: Path in DevPHP
	 * @param targetPath: Path of the generation directory 
	 * @throws IOException: The targetPath does not exists
	 */
	public void copyFiles(String sourcePath, String targetPath) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
        is = new FileInputStream(new File(sourcePath));
        os = new FileOutputStream(new File(targetPath));
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }
        is.close();
        os.close();
	}	
	
	public String writePHP(String content, String path) throws IOException{
		path = path.replace("\\", "/");
		BufferedWriter bw = new BufferedWriter(new FileWriter(path));
		bw.write(content);
		bw.close();
        return "Created File: " + path + "\n";
	}
}
