package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

public class PanelEditor extends JPanel implements Observer {

	private static final long serialVersionUID = 995750957452178570L;
	private JTextArea textAreaEditor;

	/**
	 * Create the panel.
	 */
	public PanelEditor() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(new BorderLayout(0, 0));
		textAreaEditor = new JTextArea();
		textAreaEditor.setEditable(false);
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		scrollPane.setViewportView(textAreaEditor);
	}

	@Override
	public void update(Observable o, Object obj) {
		textAreaEditor.setText(obj.toString());
	}

}
