package ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import business.DevPHP;
import business.Dictionary;

public class Main extends JFrame {
	
	private static final long serialVersionUID = -6470220817562714125L;
	private JPanel contentPane;
	private PanelEditor panelEditor;
	private PanelConsole panelConsole;
	private DevPHP devPHP;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("DevPHP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmOpenXmlModel = new JMenuItem("Open XML Model");
		mntmOpenXmlModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mntmOpenXmlModelActionPerformed();
			}
		});
		mnFile.add(mntmOpenXmlModel);
		
		JMenu mnTools = new JMenu("Tools");
		menuBar.add(mnTools);
		
		JMenuItem mntmSetGenerationDirectory = new JMenuItem("Set Generation Directory");
		mntmSetGenerationDirectory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mntmSetGenerationDirectoryActionPerformed();
			}
		});
		mnTools.add(mntmSetGenerationDirectory);
		
		JMenuItem mntmGeneratePhp = new JMenuItem("Generate PHP");
		mntmGeneratePhp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mntmGeneratePhpActionPerformed();
			}
		});
		mnTools.add(mntmGeneratePhp);
		
		JMenuItem mntmVerifyXmlModel = new JMenuItem("Verify PHP");
		mntmVerifyXmlModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mntmValidateXmlModelActionPerformed();
			}
		});
		mnTools.add(mntmVerifyXmlModel);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		panelEditor = new PanelEditor();
		contentPane.add(panelEditor, BorderLayout.CENTER);
		panelConsole = new PanelConsole();
		contentPane.add(panelConsole, BorderLayout.SOUTH);
		this.devPHP = new DevPHP();
		this.devPHP.addObserver(this.panelConsole);
		this.devPHP.getXMLModel().addObserver(this.panelEditor);
	}

	/**
	 * Set the directory where the PHP project is going to be generated
	 */
	protected void mntmSetGenerationDirectoryActionPerformed() {
		JFileChooser fileChooser = new JFileChooser();
	    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fileChooser.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
			File file = fileChooser.getSelectedFile();
			this.devPHP.setGenerationDirectory(file.getAbsolutePath());
		}							
	}

	/**
	 * Generate the PHP project in the desired generation directory, based on the XML model already opened
	 */
	protected void mntmGeneratePhpActionPerformed() {
		try {
			Dictionary.getInstance().init();
			if(this.devPHP.getXMLModel().getXmlPath()!=null) {
				if(this.devPHP.getGenerationDirectory()==null){
					this.mntmSetGenerationDirectoryActionPerformed();
				}
				this.devPHP.generateProject();				
			}else {
				JOptionPane.showMessageDialog(this, "Please, open a XML model", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {			
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}				
	}

	/**
	 * Open the XML model
	 */
	protected void mntmOpenXmlModelActionPerformed() {
		try {
			JFileChooser fileChooser = new JFileChooser();
			Filter filter = new Filter(".xml", "XML Files (*.xml)");
			fileChooser.addChoosableFileFilter(filter);
			if (fileChooser.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
				File file = fileChooser.getSelectedFile();
				this.devPHP.openXMLModel(file.getAbsolutePath());
			}			
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

	}
	
	/**
	 * Verify the XML model, only if it has already been opened
	 */
	protected void mntmValidateXmlModelActionPerformed() {
		if(this.devPHP.getXMLModel().getXmlPath()!=null && this.devPHP.getXMLModel().getXmlContent() != "") {
			this.devPHP.validateXmlModel();
		}else {
			JOptionPane.showMessageDialog(this, "Please, open a XML model", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
