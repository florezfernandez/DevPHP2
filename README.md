<h1>DevPHP</h1>
<p>DevPHP is a project that allows creating a PHP project based on a conceptual model written in XML.</p>

<p>The models can include language that can be English (en) or Spanish (es) and a template from <a href="https://bootswatch.com/" target="_blank">Bootswatch</a></p> 

<p>The folder "models" include two example models. The former generates a project in English, while the latter a project in Spanish</p>


 